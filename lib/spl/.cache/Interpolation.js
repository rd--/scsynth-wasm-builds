sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "bilinearInterpolation",
  ["q11", "q21", "q12", "q22", "mu1", "mu2"],
  sl.annotateFunction(function (_q11, _q21, _q12, _q22, _mu1, _mu2) {
    /* ArityCheck */
    if (arguments.length !== 6) {
      const errorMessage =
        "Arity: expected 6, _q11, _q21, _q12, _q22, _mu1, _mu2";
      throw new Error(errorMessage);
    } /* Statements */
    return _linearInterpolation_3(
      _linearInterpolation_3(_q11, _q21, _mu1),
      _linearInterpolation_3(_q12, _q22, _mu1),
      _mu2,
    );
  }, ["q11", "q21", "q12", "q22", "mu1", "mu2"]),
  "{ :q11 :q21 :q12 :q22 :mu1 :mu2 | linearInterpolation(linearInterpolation(q11, q21, mu1), linearInterpolation(q12, q22, mu1), mu2) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "bilinearInterpolation",
  ["q11", "q21", "q12", "q22", "mu1", "mu2"],
  sl.annotateFunction(function (_q11, _q21, _q12, _q22, _mu1, _mu2) {
    /* ArityCheck */
    if (arguments.length !== 6) {
      const errorMessage =
        "Arity: expected 6, _q11, _q21, _q12, _q22, _mu1, _mu2";
      throw new Error(errorMessage);
    } /* Statements */
    return _linearInterpolation_3(
      _linearInterpolation_3(_q11, _q21, _mu1),
      _linearInterpolation_3(_q12, _q22, _mu1),
      _mu2,
    );
  }, ["q11", "q21", "q12", "q22", "mu1", "mu2"]),
  "{ :q11 :q21 :q12 :q22 :mu1 :mu2 | linearInterpolation(linearInterpolation(q11, q21, mu1), linearInterpolation(q12, q22, mu1), mu2) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "trilinearInterpolation",
  [
    "c000",
    "c100",
    "c010",
    "c110",
    "c001",
    "c101",
    "c011",
    "c111",
    "mu1",
    "mu2",
    "mu3",
  ],
  sl.annotateFunction(
    function (
      _c000,
      _c100,
      _c010,
      _c110,
      _c001,
      _c101,
      _c011,
      _c111,
      _mu1,
      _mu2,
      _mu3,
    ) {
      /* ArityCheck */
      if (arguments.length !== 11) {
        const errorMessage =
          "Arity: expected 11, _c000, _c100, _c010, _c110, _c001, _c101, _c011, _c111, _mu1, _mu2, _mu3";
        throw new Error(errorMessage);
      } /* Statements */
      return _linearInterpolation_3(
        _bilinearInterpolation_6(_c000, _c100, _c010, _c110, _mu1, _mu2),
        _bilinearInterpolation_6(_c001, _c101, _c011, _c111, _mu1, _mu2),
        _mu3,
      );
    },
    [
      "c000",
      "c100",
      "c010",
      "c110",
      "c001",
      "c101",
      "c011",
      "c111",
      "mu1",
      "mu2",
      "mu3",
    ],
  ),
  "{ :c000 :c100 :c010 :c110 :c001 :c101 :c011 :c111 :mu1 :mu2 :mu3 | linearInterpolation(bilinearInterpolation(c000, c100, c010, c110, mu1, mu2), bilinearInterpolation(c001, c101, c011, c111, mu1, mu2), mu3) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "trilinearInterpolation",
  [
    "c000",
    "c100",
    "c010",
    "c110",
    "c001",
    "c101",
    "c011",
    "c111",
    "mu1",
    "mu2",
    "mu3",
  ],
  sl.annotateFunction(
    function (
      _c000,
      _c100,
      _c010,
      _c110,
      _c001,
      _c101,
      _c011,
      _c111,
      _mu1,
      _mu2,
      _mu3,
    ) {
      /* ArityCheck */
      if (arguments.length !== 11) {
        const errorMessage =
          "Arity: expected 11, _c000, _c100, _c010, _c110, _c001, _c101, _c011, _c111, _mu1, _mu2, _mu3";
        throw new Error(errorMessage);
      } /* Statements */
      return _linearInterpolation_3(
        _bilinearInterpolation_6(_c000, _c100, _c010, _c110, _mu1, _mu2),
        _bilinearInterpolation_6(_c001, _c101, _c011, _c111, _mu1, _mu2),
        _mu3,
      );
    },
    [
      "c000",
      "c100",
      "c010",
      "c110",
      "c001",
      "c101",
      "c011",
      "c111",
      "mu1",
      "mu2",
      "mu3",
    ],
  ),
  "{ :c000 :c100 :c010 :c110 :c001 :c101 :c011 :c111 :mu1 :mu2 :mu3 | linearInterpolation(bilinearInterpolation(c000, c100, c010, c110, mu1, mu2), bilinearInterpolation(c001, c101, c011, c111, mu1, mu2), mu3) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "blend",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Statements */
    return _blend_4(
      _y1,
      _y2,
      _mu,
      sl.annotateFunction(function (_y1, _y2, _mu) {
        /* ArityCheck */
        if (arguments.length !== 3) {
          const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSign_2(_y1, _asterisk_2(_mu, _hyphenMinus_2(_y2, _y1)));
      }, ["y1", "y2", "mu"]),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | blend(y1, y2, mu, { :y1 :y2 :mu | +(y1, (*(mu, (-(y2, y1))))) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "blend",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Statements */
    return _blend_4(
      _y1,
      _y2,
      _mu,
      sl.annotateFunction(function (_y1, _y2, _mu) {
        /* ArityCheck */
        if (arguments.length !== 3) {
          const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSign_2(_y1, _asterisk_2(_mu, _hyphenMinus_2(_y2, _y1)));
      }, ["y1", "y2", "mu"]),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | blend(y1, y2, mu, { :y1 :y2 :mu | +(y1, (*(mu, (-(y2, y1))))) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "blend",
  ["y1", "y2", "mu", "aBlock:/3"],
  sl.annotateFunction(function (_y1, _y2, _mu, _aBlock_3) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _y1, _y2, _mu, _aBlock_3";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isList_1(_mu),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _mu,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _blend_4(_y1, _y2, _each, _aBlock_3);
          }, ["each"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_3(_y1, _y2, _mu);
      }, []),
    );
  }, ["y1", "y2", "mu", "aBlock:/3"]),
  "{ :y1 :y2 :mu :aBlock:/3 | if(isList(mu), { collect(mu, { :each | blend(y1,y2, each, aBlock:/3) }) }, { aBlock(y1, y2, mu) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "blend",
  ["y1", "y2", "mu", "aBlock:/3"],
  sl.annotateFunction(function (_y1, _y2, _mu, _aBlock_3) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _y1, _y2, _mu, _aBlock_3";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isList_1(_mu),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _mu,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _blend_4(_y1, _y2, _each, _aBlock_3);
          }, ["each"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_3(_y1, _y2, _mu);
      }, []),
    );
  }, ["y1", "y2", "mu", "aBlock:/3"]),
  "{ :y1 :y2 :mu :aBlock:/3 | if(isList(mu), { collect(mu, { :each | blend(y1,y2, each, aBlock:/3) }) }, { aBlock(y1, y2, mu) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "catmullRomInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a0 = _plusSign_2(
      _hyphenMinus_2(
        _plusSign_2(_asterisk_2(-0.5, _y0), _asterisk_2(1.5, _y1)),
        _asterisk_2(1.5, _y2),
      ),
      _asterisk_2(0.5, _y3),
    );
    let _a1 = _hyphenMinus_2(
      _plusSign_2(
        _hyphenMinus_2(_y0, _asterisk_2(2.5, _y1)),
        _asterisk_2(2, _y2),
      ),
      _asterisk_2(0.5, _y3),
    );
    let _a2 = _plusSign_2(_asterisk_2(-0.5, _y0), _asterisk_2(0.5, _y2));
    let _a3 = _y1;
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _mu3), _asterisk_2(_a1, _mu2)),
        _asterisk_2(_a2, _mu),
      ),
      _a3,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let a0 = +(-(+((*(-0.5, y0)), (*(1.5, y1))), (*(1.5, y2))), (*(0.5, y3))); let a1 = -(+(-(y0, (*(2.5, y1))), (*(2, y2))), (*(0.5, y3))); let a2 = +((*(-0.5, y0)), (*(0.5, y2))); let a3 = y1; let mu2 = *(mu, mu); let mu3 = *(mu2, mu); +(+(+((*(a0, mu3)), (*(a1, mu2))), (*(a2, mu))), a3) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "catmullRomInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a0 = _plusSign_2(
      _hyphenMinus_2(
        _plusSign_2(_asterisk_2(-0.5, _y0), _asterisk_2(1.5, _y1)),
        _asterisk_2(1.5, _y2),
      ),
      _asterisk_2(0.5, _y3),
    );
    let _a1 = _hyphenMinus_2(
      _plusSign_2(
        _hyphenMinus_2(_y0, _asterisk_2(2.5, _y1)),
        _asterisk_2(2, _y2),
      ),
      _asterisk_2(0.5, _y3),
    );
    let _a2 = _plusSign_2(_asterisk_2(-0.5, _y0), _asterisk_2(0.5, _y2));
    let _a3 = _y1;
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _mu3), _asterisk_2(_a1, _mu2)),
        _asterisk_2(_a2, _mu),
      ),
      _a3,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let a0 = +(-(+((*(-0.5, y0)), (*(1.5, y1))), (*(1.5, y2))), (*(0.5, y3))); let a1 = -(+(-(y0, (*(2.5, y1))), (*(2, y2))), (*(0.5, y3))); let a2 = +((*(-0.5, y0)), (*(0.5, y2))); let a3 = y1; let mu2 = *(mu, mu); let mu3 = *(mu2, mu); +(+(+((*(a0, mu3)), (*(a1, mu2))), (*(a2, mu))), a3) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "cosineInterpolation",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _solidus_2(_hyphenMinus_2(1, _cos_1(_pi_1(_mu))), 2);
    /* Statements */
    return _plusSign_2(
      _asterisk_2(_y1, _hyphenMinus_2(1, _x)),
      _asterisk_2(_y2, _x),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | let x = /((-(1, cos(pi(mu)))), 2); +((*(y1, (-(1, x)))), (*(y2, x))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "cosineInterpolation",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _solidus_2(_hyphenMinus_2(1, _cos_1(_pi_1(_mu))), 2);
    /* Statements */
    return _plusSign_2(
      _asterisk_2(_y1, _hyphenMinus_2(1, _x)),
      _asterisk_2(_y2, _x),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | let x = /((-(1, cos(pi(mu)))), 2); +((*(y1, (-(1, x)))), (*(y2, x))) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "cubicInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a0 = _plusSign_2(_hyphenMinus_2(_hyphenMinus_2(_y3, _y2), _y0), _y1);
    let _a1 = _hyphenMinus_2(_hyphenMinus_2(_y0, _y1), _a0);
    let _a2 = _hyphenMinus_2(_y2, _y0);
    let _a3 = _y1;
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _mu3), _asterisk_2(_a1, _mu2)),
        _asterisk_2(_a2, _mu),
      ),
      _a3,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let a0 = +(-(-(y3, y2), y0), y1); let a1 = -(-(y0, y1), a0); let a2 = -(y2, y0); let a3 = y1; let mu2 = *(mu, mu); let mu3 = *(mu2, mu); +(+(+((*(a0, mu3)), (*(a1, mu2))), (*(a2, mu))), a3) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "cubicInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a0 = _plusSign_2(_hyphenMinus_2(_hyphenMinus_2(_y3, _y2), _y0), _y1);
    let _a1 = _hyphenMinus_2(_hyphenMinus_2(_y0, _y1), _a0);
    let _a2 = _hyphenMinus_2(_y2, _y0);
    let _a3 = _y1;
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _mu3), _asterisk_2(_a1, _mu2)),
        _asterisk_2(_a2, _mu),
      ),
      _a3,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let a0 = +(-(-(y3, y2), y0), y1); let a1 = -(-(y0, y1), a0); let a2 = -(y2, y0); let a3 = y1; let mu2 = *(mu, mu); let mu3 = *(mu2, mu); +(+(+((*(a0, mu3)), (*(a1, mu2))), (*(a2, mu))), a3) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "hermiteInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c0 = _y1;
    let _c1 = _asterisk_2(0.5, _hyphenMinus_2(_y2, _y0));
    let _c3 = _plusSign_2(
      _asterisk_2(1.5, _hyphenMinus_2(_y1, _y2)),
      _asterisk_2(0.5, _hyphenMinus_2(_y3, _y0)),
    );
    let _c2 = _hyphenMinus_2(_plusSign_2(_hyphenMinus_2(_y0, _y1), _c1), _c3);
    /* Statements */
    return _plusSign_2(
      _asterisk_2(
        _plusSign_2(
          _asterisk_2(_plusSign_2(_asterisk_2(_c3, _mu), _c2), _mu),
          _c1,
        ),
        _mu,
      ),
      _c0,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let c0 = y1; let c1 = *(0.5, (-(y2, y0))); let c3 = +((*(1.5, (-(y1, y2)))), (*(0.5, (-(y3, y0))))); let c2 = -(+(-(y0, y1), c1), c3); +(*((+(*((+(*(c3, mu), c2)), mu), c1)), mu), c0) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "hermiteInterpolation",
  ["y0", "y1", "y2", "y3", "mu"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c0 = _y1;
    let _c1 = _asterisk_2(0.5, _hyphenMinus_2(_y2, _y0));
    let _c3 = _plusSign_2(
      _asterisk_2(1.5, _hyphenMinus_2(_y1, _y2)),
      _asterisk_2(0.5, _hyphenMinus_2(_y3, _y0)),
    );
    let _c2 = _hyphenMinus_2(_plusSign_2(_hyphenMinus_2(_y0, _y1), _c1), _c3);
    /* Statements */
    return _plusSign_2(
      _asterisk_2(
        _plusSign_2(
          _asterisk_2(_plusSign_2(_asterisk_2(_c3, _mu), _c2), _mu),
          _c1,
        ),
        _mu,
      ),
      _c0,
    );
  }, ["y0", "y1", "y2", "y3", "mu"]),
  "{ :y0 :y1 :y2 :y3 :mu | let c0 = y1; let c1 = *(0.5, (-(y2, y0))); let c3 = +((*(1.5, (-(y1, y2)))), (*(0.5, (-(y3, y0))))); let c2 = -(+(-(y0, y1), c1), c3); +(*((+(*((+(*(c3, mu), c2)), mu), c1)), mu), c0) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "hermiteInterpolation",
  ["tension", "bias"],
  sl.annotateFunction(function (_tension, _bias) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _tension, _bias";
      throw new Error(errorMessage);
    } /* Statements */
    return sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
      /* ArityCheck */
      if (arguments.length !== 5) {
        const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _hermiteInterpolation_7(_y0, _y1, _y2, _y3, _mu, _tension, _bias);
    }, ["y0", "y1", "y2", "y3", "mu"]);
  }, ["tension", "bias"]),
  "{ :tension :bias | { :y0 :y1 :y2 :y3 :mu | hermiteInterpolation(y0, y1, y2, y3, mu, tension, bias) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "hermiteInterpolation",
  ["tension", "bias"],
  sl.annotateFunction(function (_tension, _bias) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _tension, _bias";
      throw new Error(errorMessage);
    } /* Statements */
    return sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu) {
      /* ArityCheck */
      if (arguments.length !== 5) {
        const errorMessage = "Arity: expected 5, _y0, _y1, _y2, _y3, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _hermiteInterpolation_7(_y0, _y1, _y2, _y3, _mu, _tension, _bias);
    }, ["y0", "y1", "y2", "y3", "mu"]);
  }, ["tension", "bias"]),
  "{ :tension :bias | { :y0 :y1 :y2 :y3 :mu | hermiteInterpolation(y0, y1, y2, y3, mu, tension, bias) } }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "hermiteInterpolation",
  ["y0", "y1", "y2", "y3", "mu", "tension", "bias"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu, _tension, _bias) {
    /* ArityCheck */
    if (arguments.length !== 7) {
      const errorMessage =
        "Arity: expected 7, _y0, _y1, _y2, _y3, _mu, _tension, _bias";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    let _m0 = _plusSign_2(
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y1, _y0), _plusSign_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y2, _y1), _hyphenMinus_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
    );
    let _m1 = _plusSign_2(
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y2, _y1), _plusSign_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y3, _y2), _hyphenMinus_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
    );
    let _a0 = _plusSign_2(
      _hyphenMinus_2(_asterisk_2(2, _mu3), _asterisk_2(3, _mu2)),
      1,
    );
    let _a1 = _plusSign_2(_hyphenMinus_2(_mu3, _asterisk_2(2, _mu2)), _mu);
    let _a2 = _hyphenMinus_2(_mu3, _mu2);
    let _a3 = _plusSign_2(_asterisk_2(-2, _mu3), _asterisk_2(3, _mu2));
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _y1), _asterisk_2(_a1, _m0)),
        _asterisk_2(_a2, _m1),
      ),
      _asterisk_2(_a3, _y2),
    );
  }, ["y0", "y1", "y2", "y3", "mu", "tension", "bias"]),
  "{ :y0 :y1 :y2 :y3 :mu :tension :bias | let mu2 = *(mu, mu); let mu3 = *(mu2, mu); let m0 = +((/(*(*((-(y1, y0)), (+(1, bias))), (-(1, tension))), 2)), (/(*(*((-(y2, y1)), (-(1, bias))), (-(1, tension))), 2))); let m1 = +((/(*(*((-(y2, y1)), (+(1, bias))), (-(1, tension))), 2)), (/(*(*((-(y3, y2)), (-(1, bias))), (-(1, tension))), 2))); let a0 = +(-((*(2, mu3)), (*(3, mu2))), 1); let a1 = +(-(mu3, (*(2, mu2))), mu); let a2 = -(mu3, mu2); let a3 = +((*(-2, mu3)), (*(3, mu2))); +(+(+((*(a0, y1)), (*(a1, m0))), (*(a2, m1))), (*(a3, y2))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "hermiteInterpolation",
  ["y0", "y1", "y2", "y3", "mu", "tension", "bias"],
  sl.annotateFunction(function (_y0, _y1, _y2, _y3, _mu, _tension, _bias) {
    /* ArityCheck */
    if (arguments.length !== 7) {
      const errorMessage =
        "Arity: expected 7, _y0, _y1, _y2, _y3, _mu, _tension, _bias";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _mu2 = _asterisk_2(_mu, _mu);
    let _mu3 = _asterisk_2(_mu2, _mu);
    let _m0 = _plusSign_2(
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y1, _y0), _plusSign_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y2, _y1), _hyphenMinus_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
    );
    let _m1 = _plusSign_2(
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y2, _y1), _plusSign_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
      _solidus_2(
        _asterisk_2(
          _asterisk_2(_hyphenMinus_2(_y3, _y2), _hyphenMinus_2(1, _bias)),
          _hyphenMinus_2(1, _tension),
        ),
        2,
      ),
    );
    let _a0 = _plusSign_2(
      _hyphenMinus_2(_asterisk_2(2, _mu3), _asterisk_2(3, _mu2)),
      1,
    );
    let _a1 = _plusSign_2(_hyphenMinus_2(_mu3, _asterisk_2(2, _mu2)), _mu);
    let _a2 = _hyphenMinus_2(_mu3, _mu2);
    let _a3 = _plusSign_2(_asterisk_2(-2, _mu3), _asterisk_2(3, _mu2));
    /* Statements */
    return _plusSign_2(
      _plusSign_2(
        _plusSign_2(_asterisk_2(_a0, _y1), _asterisk_2(_a1, _m0)),
        _asterisk_2(_a2, _m1),
      ),
      _asterisk_2(_a3, _y2),
    );
  }, ["y0", "y1", "y2", "y3", "mu", "tension", "bias"]),
  "{ :y0 :y1 :y2 :y3 :mu :tension :bias | let mu2 = *(mu, mu); let mu3 = *(mu2, mu); let m0 = +((/(*(*((-(y1, y0)), (+(1, bias))), (-(1, tension))), 2)), (/(*(*((-(y2, y1)), (-(1, bias))), (-(1, tension))), 2))); let m1 = +((/(*(*((-(y2, y1)), (+(1, bias))), (-(1, tension))), 2)), (/(*(*((-(y3, y2)), (-(1, bias))), (-(1, tension))), 2))); let a0 = +(-((*(2, mu3)), (*(3, mu2))), 1); let a1 = +(-(mu3, (*(2, mu2))), mu); let a2 = -(mu3, mu2); let a3 = +((*(-2, mu3)), (*(3, mu2))); +(+(+((*(a0, y1)), (*(a1, m0))), (*(a2, m1))), (*(a3, y2))) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "linearInterpolation",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Statements */
    return _plusSign_2(
      _asterisk_2(_y1, _hyphenMinus_2(1, _mu)),
      _asterisk_2(_y2, _mu),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | +((*(y1, (-(1, mu)))), (*(y2, mu))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "linearInterpolation",
  ["y1", "y2", "mu"],
  sl.annotateFunction(function (_y1, _y2, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _y1, _y2, _mu";
      throw new Error(errorMessage);
    } /* Statements */
    return _plusSign_2(
      _asterisk_2(_y1, _hyphenMinus_2(1, _mu)),
      _asterisk_2(_y2, _mu),
    );
  }, ["y1", "y2", "mu"]),
  "{ :y1 :y2 :mu | +((*(y1, (-(1, mu)))), (*(y2, mu))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "akimaInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    let _differences = _List_1(_n);
    let _weights = _List_1(_n);
    let _firstDerivative = _List_1(_plusSign_2(_n, 1));
    let _epsilon = _smallFloatEpsilon_1(1);
    let _differentiateThreePoint_4 = sl.annotateFunction(
      function (_i, _j1, _j2, _j3) {
        /* ArityCheck */
        if (arguments.length !== 4) {
          const errorMessage = "Arity: expected 4, _i, _j1, _j2, _j3";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _x0 = _at_2(_y, _j1);
        let _x1 = _at_2(_y, _j2);
        let _x2 = _at_2(_y, _j3);
        let _t = _hyphenMinus_2(_at_2(_x, _i), _at_2(_x, _j1));
        let _t1 = _hyphenMinus_2(_at_2(_x, _j2), _at_2(_x, _j1));
        let _t2 = _hyphenMinus_2(_at_2(_x, _j3), _at_2(_x, _j1));
        let _a = _solidus_2(
          _hyphenMinus_2(
            _hyphenMinus_2(_x2, _x0),
            _asterisk_2(_solidus_2(_t2, _t1), _hyphenMinus_2(_x1, _x0)),
          ),
          _hyphenMinus_2(_asterisk_2(_t2, _t2), _asterisk_2(_t1, _t2)),
        );
        let _b = _solidus_2(
          _hyphenMinus_2(
            _hyphenMinus_2(_x1, _x0),
            _asterisk_2(_asterisk_2(_a, _t1), _t1),
          ),
          _t1,
        );
        /* Statements */
        return _plusSign_2(_asterisk_2(_asterisk_2(2, _a), _t), _b);
      },
      ["i", "j1", "j2", "j3"],
    );
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 5);
    _toDo_3(
      1,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _differences,
          _i,
          _solidus_2(
            _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i)),
            _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i)),
          ),
        );
      }, ["i"]),
    );
    _toDo_3(
      2,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _weights,
          _i,
          _abs_1(
            _hyphenMinus_2(
              _at_2(_differences, _i),
              _at_2(_differences, _hyphenMinus_2(_i, 1)),
            ),
          ),
        );
      }, ["i"]),
    );
    _toDo_3(
      3,
      _hyphenMinus_2(_n, 1),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _wP = _at_2(_weights, _plusSign_2(_i, 1));
        let _wM = _at_2(_weights, _hyphenMinus_2(_i, 1));
        /* Statements */
        return _if_3(
          _ampersand_2(
            _lessThanSign_2(_abs_1(_wP), _epsilon),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _lessThanSign_2(_abs_1(_wM), _epsilon);
            }, []),
          ),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _xv = _at_2(_x, _i);
            let _xvP = _at_2(_x, _plusSign_2(_i, 1));
            let _xvM = _at_2(_x, _hyphenMinus_2(_i, 1));
            /* Statements */
            return _atPut_3(
              _firstDerivative,
              _i,
              _solidus_2(
                _plusSign_2(
                  _asterisk_2(
                    _hyphenMinus_2(_xvP, _xv),
                    _at_2(_differences, _hyphenMinus_2(_i, 1)),
                  ),
                  _asterisk_2(
                    _hyphenMinus_2(_xv, _xvM),
                    _at_2(_differences, _i),
                  ),
                ),
                _hyphenMinus_2(_xvP, _xvM),
              ),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _atPut_3(
              _firstDerivative,
              _i,
              _solidus_2(
                _plusSign_2(
                  _asterisk_2(_wP, _at_2(_differences, _hyphenMinus_2(_i, 1))),
                  _asterisk_2(_wM, _at_2(_differences, _i)),
                ),
                _plusSign_2(_wP, _wM),
              ),
            );
          }, []),
        );
      }, ["i"]),
    );
    _atPut_3(_firstDerivative, 1, _differentiateThreePoint_4(1, 1, 2, 3));
    _atPut_3(_firstDerivative, 2, _differentiateThreePoint_4(2, 1, 2, 3));
    _atPut_3(
      _firstDerivative,
      _n,
      _differentiateThreePoint_4(
        _n,
        _hyphenMinus_2(_n, 1),
        _n,
        _plusSign_2(_n, 1),
      ),
    );
    _atPut_3(
      _firstDerivative,
      _plusSign_2(_n, 1),
      _differentiateThreePoint_4(
        _plusSign_2(_n, 1),
        _hyphenMinus_2(_n, 1),
        _n,
        _plusSign_2(_n, 1),
      ),
    );
    return _hermiteInterpolatorCoefficientList_3(_x, _y, _firstDerivative);
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); let differences = List(n); let weights = List(n); let firstDerivative = List(+(n, 1)); let epsilon = smallFloatEpsilon(1); let differentiateThreePoint = { :i :j1 :j2 :j3 | let x0 = at(y, j1); let x1 = at(y, j2); let x2 = at(y, j3); let t = -(at(x, i), at(x, j1)); let t1 = -(at(x, j2), at(x, j1)); let t2 = -(at(x, j3), at(x, j1)); let a = /((-(-(x2, x0), (*(/(t2, t1), (-(x1, x0)))))), (-((*(t2, t2)), (*(t1, t2))))); let b = /((-(-(x1, x0), (*(*(a, t1), t1)))), t1); +((*(*(2, a), t)), b) }; assertIsValidInterpolatorData(x, y, 5); toDo(1, n, { :i | atPut(differences, i, /((-(at(y, +(i, 1)), at(y, i))), (-(at(x, +(i, 1)), at(x, i))))) }); toDo(2, n, { :i | atPut(weights, i, abs(-(at(differences, i), at(differences, -(i, 1))))) }); toDo(3, -(n, 1), { :i | let wP = at(weights, +(i, 1)); let wM = at(weights, -(i, 1)); if((&(<(abs(wP), epsilon), { <(abs(wM), epsilon) })), { let xv = at(x, i); let xvP = at(x, +(i, 1)); let xvM = at(x, -(i, 1)); atPut(firstDerivative, i, (/(+((*((-(xvP, xv)), at(differences, -(i, 1)))), ((*((-(xv, xvM)), at(differences, i))))), (-(xvP, xvM))))) }, { atPut(firstDerivative, i, /((+((*(wP, at(differences, -(i, 1)))), (*(wM, at(differences, i))))), (+(wP, wM)))) }) }); atPut(firstDerivative, 1, differentiateThreePoint(1, 1, 2, 3)); atPut(firstDerivative, 2, differentiateThreePoint(2, 1, 2, 3)); atPut(firstDerivative, n, differentiateThreePoint(n, -(n, 1), n, +(n, 1))); atPut(firstDerivative, +(n, 1), differentiateThreePoint(+(n, 1), -(n, 1), n, +(n, 1))); hermiteInterpolatorCoefficientList(x, y, firstDerivative) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "akimaInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    let _differences = _List_1(_n);
    let _weights = _List_1(_n);
    let _firstDerivative = _List_1(_plusSign_2(_n, 1));
    let _epsilon = _smallFloatEpsilon_1(1);
    let _differentiateThreePoint_4 = sl.annotateFunction(
      function (_i, _j1, _j2, _j3) {
        /* ArityCheck */
        if (arguments.length !== 4) {
          const errorMessage = "Arity: expected 4, _i, _j1, _j2, _j3";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _x0 = _at_2(_y, _j1);
        let _x1 = _at_2(_y, _j2);
        let _x2 = _at_2(_y, _j3);
        let _t = _hyphenMinus_2(_at_2(_x, _i), _at_2(_x, _j1));
        let _t1 = _hyphenMinus_2(_at_2(_x, _j2), _at_2(_x, _j1));
        let _t2 = _hyphenMinus_2(_at_2(_x, _j3), _at_2(_x, _j1));
        let _a = _solidus_2(
          _hyphenMinus_2(
            _hyphenMinus_2(_x2, _x0),
            _asterisk_2(_solidus_2(_t2, _t1), _hyphenMinus_2(_x1, _x0)),
          ),
          _hyphenMinus_2(_asterisk_2(_t2, _t2), _asterisk_2(_t1, _t2)),
        );
        let _b = _solidus_2(
          _hyphenMinus_2(
            _hyphenMinus_2(_x1, _x0),
            _asterisk_2(_asterisk_2(_a, _t1), _t1),
          ),
          _t1,
        );
        /* Statements */
        return _plusSign_2(_asterisk_2(_asterisk_2(2, _a), _t), _b);
      },
      ["i", "j1", "j2", "j3"],
    );
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 5);
    _toDo_3(
      1,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _differences,
          _i,
          _solidus_2(
            _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i)),
            _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i)),
          ),
        );
      }, ["i"]),
    );
    _toDo_3(
      2,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _weights,
          _i,
          _abs_1(
            _hyphenMinus_2(
              _at_2(_differences, _i),
              _at_2(_differences, _hyphenMinus_2(_i, 1)),
            ),
          ),
        );
      }, ["i"]),
    );
    _toDo_3(
      3,
      _hyphenMinus_2(_n, 1),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _wP = _at_2(_weights, _plusSign_2(_i, 1));
        let _wM = _at_2(_weights, _hyphenMinus_2(_i, 1));
        /* Statements */
        return _if_3(
          _ampersand_2(
            _lessThanSign_2(_abs_1(_wP), _epsilon),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _lessThanSign_2(_abs_1(_wM), _epsilon);
            }, []),
          ),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _xv = _at_2(_x, _i);
            let _xvP = _at_2(_x, _plusSign_2(_i, 1));
            let _xvM = _at_2(_x, _hyphenMinus_2(_i, 1));
            /* Statements */
            return _atPut_3(
              _firstDerivative,
              _i,
              _solidus_2(
                _plusSign_2(
                  _asterisk_2(
                    _hyphenMinus_2(_xvP, _xv),
                    _at_2(_differences, _hyphenMinus_2(_i, 1)),
                  ),
                  _asterisk_2(
                    _hyphenMinus_2(_xv, _xvM),
                    _at_2(_differences, _i),
                  ),
                ),
                _hyphenMinus_2(_xvP, _xvM),
              ),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _atPut_3(
              _firstDerivative,
              _i,
              _solidus_2(
                _plusSign_2(
                  _asterisk_2(_wP, _at_2(_differences, _hyphenMinus_2(_i, 1))),
                  _asterisk_2(_wM, _at_2(_differences, _i)),
                ),
                _plusSign_2(_wP, _wM),
              ),
            );
          }, []),
        );
      }, ["i"]),
    );
    _atPut_3(_firstDerivative, 1, _differentiateThreePoint_4(1, 1, 2, 3));
    _atPut_3(_firstDerivative, 2, _differentiateThreePoint_4(2, 1, 2, 3));
    _atPut_3(
      _firstDerivative,
      _n,
      _differentiateThreePoint_4(
        _n,
        _hyphenMinus_2(_n, 1),
        _n,
        _plusSign_2(_n, 1),
      ),
    );
    _atPut_3(
      _firstDerivative,
      _plusSign_2(_n, 1),
      _differentiateThreePoint_4(
        _plusSign_2(_n, 1),
        _hyphenMinus_2(_n, 1),
        _n,
        _plusSign_2(_n, 1),
      ),
    );
    return _hermiteInterpolatorCoefficientList_3(_x, _y, _firstDerivative);
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); let differences = List(n); let weights = List(n); let firstDerivative = List(+(n, 1)); let epsilon = smallFloatEpsilon(1); let differentiateThreePoint = { :i :j1 :j2 :j3 | let x0 = at(y, j1); let x1 = at(y, j2); let x2 = at(y, j3); let t = -(at(x, i), at(x, j1)); let t1 = -(at(x, j2), at(x, j1)); let t2 = -(at(x, j3), at(x, j1)); let a = /((-(-(x2, x0), (*(/(t2, t1), (-(x1, x0)))))), (-((*(t2, t2)), (*(t1, t2))))); let b = /((-(-(x1, x0), (*(*(a, t1), t1)))), t1); +((*(*(2, a), t)), b) }; assertIsValidInterpolatorData(x, y, 5); toDo(1, n, { :i | atPut(differences, i, /((-(at(y, +(i, 1)), at(y, i))), (-(at(x, +(i, 1)), at(x, i))))) }); toDo(2, n, { :i | atPut(weights, i, abs(-(at(differences, i), at(differences, -(i, 1))))) }); toDo(3, -(n, 1), { :i | let wP = at(weights, +(i, 1)); let wM = at(weights, -(i, 1)); if((&(<(abs(wP), epsilon), { <(abs(wM), epsilon) })), { let xv = at(x, i); let xvP = at(x, +(i, 1)); let xvM = at(x, -(i, 1)); atPut(firstDerivative, i, (/(+((*((-(xvP, xv)), at(differences, -(i, 1)))), ((*((-(xv, xvM)), at(differences, i))))), (-(xvP, xvM))))) }, { atPut(firstDerivative, i, /((+((*(wP, at(differences, -(i, 1)))), (*(wM, at(differences, i))))), (+(wP, wM)))) }) }); atPut(firstDerivative, 1, differentiateThreePoint(1, 1, 2, 3)); atPut(firstDerivative, 2, differentiateThreePoint(2, 1, 2, 3)); atPut(firstDerivative, n, differentiateThreePoint(n, -(n, 1), n, +(n, 1))); atPut(firstDerivative, +(n, 1), differentiateThreePoint(+(n, 1), -(n, 1), n, +(n, 1))); hermiteInterpolatorCoefficientList(x, y, firstDerivative) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "akimaInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _akimaInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = akimaInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "akimaInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _akimaInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = akimaInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "assertIsValidInterpolatorData",
  ["x", "y", "n"],
  sl.annotateFunction(function (_x, _y, _n) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _y, _n";
      throw new Error(errorMessage);
    } /* Statements */
    _ifTrue_2(
      _tildeEqualsSign_2(_size_1(_x), _size_1(_y)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: dimension mismatch");
      }, []),
    );
    _ifTrue_2(
      _lessThanSign_2(_size_1(_x), _n),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: number of points is too small");
      }, []),
    );
    return _ifFalse_2(
      _isStrictlyIncreasing_1(_x),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: x not strictly increasing");
      }, []),
    );
  }, ["x", "y", "n"]),
  "{ :x :y :n | ifTrue((~=(size(x), size(y))), { error(x,'Interpolator: dimension mismatch') }); ifTrue((<(size(x), n)), { error(x,'Interpolator: number of points is too small') }); ifFalse(isStrictlyIncreasing(x), { error(x,'Interpolator: x not strictly increasing') }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "assertIsValidInterpolatorData",
  ["x", "y", "n"],
  sl.annotateFunction(function (_x, _y, _n) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _y, _n";
      throw new Error(errorMessage);
    } /* Statements */
    _ifTrue_2(
      _tildeEqualsSign_2(_size_1(_x), _size_1(_y)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: dimension mismatch");
      }, []),
    );
    _ifTrue_2(
      _lessThanSign_2(_size_1(_x), _n),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: number of points is too small");
      }, []),
    );
    return _ifFalse_2(
      _isStrictlyIncreasing_1(_x),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_x, "Interpolator: x not strictly increasing");
      }, []),
    );
  }, ["x", "y", "n"]),
  "{ :x :y :n | ifTrue((~=(size(x), size(y))), { error(x,'Interpolator: dimension mismatch') }); ifTrue((<(size(x), n)), { error(x,'Interpolator: number of points is too small') }); ifFalse(isStrictlyIncreasing(x), { error(x,'Interpolator: x not strictly increasing') }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "cubicSplineInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    let _h = _List_1(_n);
    let _mu = _List_1(_n);
    let _z = _List_1(_plusSign_2(_n, 1));
    let _b = _List_1(_n);
    let _c = _List_1(_plusSign_2(_n, 1));
    let _d = _List_1(_n);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 3);
    _toDo_3(
      1,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _h,
          _i,
          _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i)),
        );
      }, ["i"]),
    );
    _atPut_3(_mu, 1, 0);
    _atPut_3(_z, 1, 0);
    _toDo_3(
      2,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _g = _hyphenMinus_2(
          _asterisk_2(
            2,
            _hyphenMinus_2(
              _at_2(_x, _plusSign_2(_i, 1)),
              _at_2(_x, _hyphenMinus_2(_i, 1)),
            ),
          ),
          _asterisk_2(
            _at_2(_h, _hyphenMinus_2(_i, 1)),
            _at_2(_mu, _hyphenMinus_2(_i, 1)),
          ),
        );
        /* Statements */
        _atPut_3(_mu, _i, _solidus_2(_at_2(_h, _i), _g));
        return _atPut_3(
          _z,
          _i,
          _solidus_2(
            _hyphenMinus_2(
              _solidus_2(
                _asterisk_2(
                  3,
                  _plusSign_2(
                    _hyphenMinus_2(
                      _asterisk_2(
                        _at_2(_y, _plusSign_2(_i, 1)),
                        _at_2(_h, _hyphenMinus_2(_i, 1)),
                      ),
                      _asterisk_2(
                        _at_2(_y, _i),
                        _hyphenMinus_2(
                          _at_2(_x, _plusSign_2(_i, 1)),
                          _at_2(_x, _hyphenMinus_2(_i, 1)),
                        ),
                      ),
                    ),
                    _asterisk_2(
                      _at_2(_y, _hyphenMinus_2(_i, 1)),
                      _at_2(_h, _i),
                    ),
                  ),
                ),
                _asterisk_2(_at_2(_h, _hyphenMinus_2(_i, 1)), _at_2(_h, _i)),
              ),
              _asterisk_2(
                _at_2(_h, _hyphenMinus_2(_i, 1)),
                _at_2(_z, _hyphenMinus_2(_i, 1)),
              ),
            ),
            _g,
          ),
        );
      }, ["i"]),
    );
    _atPut_3(_z, _plusSign_2(_n, 1), 0);
    _atPut_3(_c, _plusSign_2(_n, 1), 0);
    _downToDo_3(
      _n,
      1,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _dx = _at_2(_h, _i);
        let _dy = _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i));
        /* Statements */
        _atPut_3(
          _c,
          _i,
          _hyphenMinus_2(
            _at_2(_z, _i),
            _asterisk_2(_at_2(_mu, _i), _at_2(_c, _plusSign_2(_i, 1))),
          ),
        );
        _atPut_3(
          _b,
          _i,
          _hyphenMinus_2(
            _solidus_2(_dy, _dx),
            _solidus_2(
              _asterisk_2(
                _dx,
                _plusSign_2(
                  _at_2(_c, _plusSign_2(_i, 1)),
                  _asterisk_2(2, _at_2(_c, _i)),
                ),
              ),
              3,
            ),
          ),
        );
        return _atPut_3(
          _d,
          _i,
          _solidus_2(
            _hyphenMinus_2(_at_2(_c, _plusSign_2(_i, 1)), _at_2(_c, _i)),
            _asterisk_2(3, _dx),
          ),
        );
      }, ["i"]),
    );
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _withoutTrailingZeros_1([
          _at_2(_y, _i),
          _at_2(_b, _i),
          _at_2(_c, _i),
          _at_2(_d, _i),
        ]);
      }, ["i"]),
    );
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); let h = List(n); let mu = List(n); let z = List(+(n, 1)); let b = List(n); let c = List(+(n, 1)); let d = List(n); assertIsValidInterpolatorData(x, y, 3); toDo(1, n, { :i | atPut(h, i, -(at(x, +(i, 1)), at(x, i))) }); atPut(mu, 1, 0); atPut(z, 1, 0); toDo(2, n, { :i | let g = -((*(2, (-(at(x, +(i, 1)), at(x, -(i, 1)))))), (*(at(h, -(i, 1)), at(mu, -(i, 1))))); atPut(mu, i, /(at(h, i), g)); atPut(z, i, /((-(/(*(3, (+(-((*(at(y, +(i, 1)), at(h, -(i, 1)))), (*(at(y, i), (-(at(x, +(i, 1)), at(x, -(i, 1))))))), (*(at(y, -(i, 1)), at(h, i)))))), (*(at(h, -(i, 1)), at(h, i)))), (*(at(h, -(i, 1)), at(z, -(i, 1)))))), g)) }); atPut(z, +(n, 1), 0); atPut(c, +(n, 1), 0); downToDo(n, 1, { :i | let dx = at(h, i); let dy = -(at(y, +(i, 1)), at(y, i)); atPut(c, i, -(at(z, i), (*(at(mu, i), at(c, +(i, 1)))))); atPut(b, i, -((/(dy, dx)), (/(*(dx, (+(at(c, +(i, 1)), (*(2, at(c, i)))))), 3)))); atPut(d, i, /((-(at(c, +(i, 1)), at(c, i))), (*(3, dx)))) }); collect(upOrDownTo(1, n), { :i | withoutTrailingZeros([at(y, i), at(b, i), at(c, i), at(d, i)]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "cubicSplineInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    let _h = _List_1(_n);
    let _mu = _List_1(_n);
    let _z = _List_1(_plusSign_2(_n, 1));
    let _b = _List_1(_n);
    let _c = _List_1(_plusSign_2(_n, 1));
    let _d = _List_1(_n);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 3);
    _toDo_3(
      1,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _h,
          _i,
          _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i)),
        );
      }, ["i"]),
    );
    _atPut_3(_mu, 1, 0);
    _atPut_3(_z, 1, 0);
    _toDo_3(
      2,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _g = _hyphenMinus_2(
          _asterisk_2(
            2,
            _hyphenMinus_2(
              _at_2(_x, _plusSign_2(_i, 1)),
              _at_2(_x, _hyphenMinus_2(_i, 1)),
            ),
          ),
          _asterisk_2(
            _at_2(_h, _hyphenMinus_2(_i, 1)),
            _at_2(_mu, _hyphenMinus_2(_i, 1)),
          ),
        );
        /* Statements */
        _atPut_3(_mu, _i, _solidus_2(_at_2(_h, _i), _g));
        return _atPut_3(
          _z,
          _i,
          _solidus_2(
            _hyphenMinus_2(
              _solidus_2(
                _asterisk_2(
                  3,
                  _plusSign_2(
                    _hyphenMinus_2(
                      _asterisk_2(
                        _at_2(_y, _plusSign_2(_i, 1)),
                        _at_2(_h, _hyphenMinus_2(_i, 1)),
                      ),
                      _asterisk_2(
                        _at_2(_y, _i),
                        _hyphenMinus_2(
                          _at_2(_x, _plusSign_2(_i, 1)),
                          _at_2(_x, _hyphenMinus_2(_i, 1)),
                        ),
                      ),
                    ),
                    _asterisk_2(
                      _at_2(_y, _hyphenMinus_2(_i, 1)),
                      _at_2(_h, _i),
                    ),
                  ),
                ),
                _asterisk_2(_at_2(_h, _hyphenMinus_2(_i, 1)), _at_2(_h, _i)),
              ),
              _asterisk_2(
                _at_2(_h, _hyphenMinus_2(_i, 1)),
                _at_2(_z, _hyphenMinus_2(_i, 1)),
              ),
            ),
            _g,
          ),
        );
      }, ["i"]),
    );
    _atPut_3(_z, _plusSign_2(_n, 1), 0);
    _atPut_3(_c, _plusSign_2(_n, 1), 0);
    _downToDo_3(
      _n,
      1,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _dx = _at_2(_h, _i);
        let _dy = _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i));
        /* Statements */
        _atPut_3(
          _c,
          _i,
          _hyphenMinus_2(
            _at_2(_z, _i),
            _asterisk_2(_at_2(_mu, _i), _at_2(_c, _plusSign_2(_i, 1))),
          ),
        );
        _atPut_3(
          _b,
          _i,
          _hyphenMinus_2(
            _solidus_2(_dy, _dx),
            _solidus_2(
              _asterisk_2(
                _dx,
                _plusSign_2(
                  _at_2(_c, _plusSign_2(_i, 1)),
                  _asterisk_2(2, _at_2(_c, _i)),
                ),
              ),
              3,
            ),
          ),
        );
        return _atPut_3(
          _d,
          _i,
          _solidus_2(
            _hyphenMinus_2(_at_2(_c, _plusSign_2(_i, 1)), _at_2(_c, _i)),
            _asterisk_2(3, _dx),
          ),
        );
      }, ["i"]),
    );
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _withoutTrailingZeros_1([
          _at_2(_y, _i),
          _at_2(_b, _i),
          _at_2(_c, _i),
          _at_2(_d, _i),
        ]);
      }, ["i"]),
    );
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); let h = List(n); let mu = List(n); let z = List(+(n, 1)); let b = List(n); let c = List(+(n, 1)); let d = List(n); assertIsValidInterpolatorData(x, y, 3); toDo(1, n, { :i | atPut(h, i, -(at(x, +(i, 1)), at(x, i))) }); atPut(mu, 1, 0); atPut(z, 1, 0); toDo(2, n, { :i | let g = -((*(2, (-(at(x, +(i, 1)), at(x, -(i, 1)))))), (*(at(h, -(i, 1)), at(mu, -(i, 1))))); atPut(mu, i, /(at(h, i), g)); atPut(z, i, /((-(/(*(3, (+(-((*(at(y, +(i, 1)), at(h, -(i, 1)))), (*(at(y, i), (-(at(x, +(i, 1)), at(x, -(i, 1))))))), (*(at(y, -(i, 1)), at(h, i)))))), (*(at(h, -(i, 1)), at(h, i)))), (*(at(h, -(i, 1)), at(z, -(i, 1)))))), g)) }); atPut(z, +(n, 1), 0); atPut(c, +(n, 1), 0); downToDo(n, 1, { :i | let dx = at(h, i); let dy = -(at(y, +(i, 1)), at(y, i)); atPut(c, i, -(at(z, i), (*(at(mu, i), at(c, +(i, 1)))))); atPut(b, i, -((/(dy, dx)), (/(*(dx, (+(at(c, +(i, 1)), (*(2, at(c, i)))))), 3)))); atPut(d, i, /((-(at(c, +(i, 1)), at(c, i))), (*(3, dx)))) }); collect(upOrDownTo(1, n), { :i | withoutTrailingZeros([at(y, i), at(b, i), at(c, i), at(d, i)]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "cubicSplineInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _cubicSplineInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = cubicSplineInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "cubicSplineInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _cubicSplineInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = cubicSplineInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "evaluateInterpolatorSegment",
  ["x", "c", "mu"],
  sl.annotateFunction(function (_x, _c, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _c, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_x);
    let _i = _hyphenMinus_2(
      _binaryDetectIndex_2(
        _k,
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          return _greaterThanSignEqualsSign_2(_at_2(_x, _each), _mu);
        }, ["each"]),
      ),
      1,
    );
    /* Statements */
    _i = _max_2(1, _min_2(_i, _size_1(_c)));
    return _evaluateUnivariatePolynomial_2(
      _at_2(_c, _i),
      _hyphenMinus_2(_mu, _at_2(_x, _i)),
    );
  }, ["x", "c", "mu"]),
  "{ :x :c :mu | let k = size(x); let i = -(binaryDetectIndex(k, { :each | >=(at(x, each), mu) }), 1); i := max(1,min(i,size(c))); evaluateUnivariatePolynomial(at(c, i),-(mu, at(x, i))) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "evaluateInterpolatorSegment",
  ["x", "c", "mu"],
  sl.annotateFunction(function (_x, _c, _mu) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _c, _mu";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_x);
    let _i = _hyphenMinus_2(
      _binaryDetectIndex_2(
        _k,
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          return _greaterThanSignEqualsSign_2(_at_2(_x, _each), _mu);
        }, ["each"]),
      ),
      1,
    );
    /* Statements */
    _i = _max_2(1, _min_2(_i, _size_1(_c)));
    return _evaluateUnivariatePolynomial_2(
      _at_2(_c, _i),
      _hyphenMinus_2(_mu, _at_2(_x, _i)),
    );
  }, ["x", "c", "mu"]),
  "{ :x :c :mu | let k = size(x); let i = -(binaryDetectIndex(k, { :each | >=(at(x, each), mu) }), 1); i := max(1,min(i,size(c))); evaluateUnivariatePolynomial(at(c, i),-(mu, at(x, i))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "hermiteInterpolatorCoefficientList",
  ["x", "y", "firstDerivative"],
  sl.annotateFunction(function (_x, _y, _firstDerivative) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _y, _firstDerivative";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 2);
    _ifTrue_2(
      _tildeEqualsSign_2(_size_1(_x), _size_1(_firstDerivative)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _x,
          "hermiteInterpolatorCoefficientList: firstDerivative list invalid",
        );
      }, []),
    );
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _w = _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i));
        let _w2 = _asterisk_2(_w, _w);
        let _yv = _at_2(_y, _i);
        let _yvP = _at_2(_y, _plusSign_2(_i, 1));
        let _fd = _at_2(_firstDerivative, _i);
        let _fdP = _at_2(_firstDerivative, _plusSign_2(_i, 1));
        /* Statements */
        return _withoutTrailingZeros_1([
          _yv,
          _at_2(_firstDerivative, _i),
          _solidus_2(
            _hyphenMinus_2(
              _hyphenMinus_2(
                _solidus_2(_asterisk_2(3, _hyphenMinus_2(_yvP, _yv)), _w),
                _asterisk_2(2, _fd),
              ),
              _fdP,
            ),
            _w,
          ),
          _solidus_2(
            _plusSign_2(
              _plusSign_2(
                _solidus_2(_asterisk_2(2, _hyphenMinus_2(_yv, _yvP)), _w),
                _fd,
              ),
              _fdP,
            ),
            _w2,
          ),
        ]);
      }, ["i"]),
    );
  }, ["x", "y", "firstDerivative"]),
  "{ :x :y :firstDerivative | let n = -(size(x), 1); assertIsValidInterpolatorData(x, y, 2); ifTrue((~=(size(x), size(firstDerivative))), { error(x,'hermiteInterpolatorCoefficientList: firstDerivative list invalid') }); collect(upOrDownTo(1, n), { :i | let w = -(at(x, +(i, 1)), at(x, i)); let w2 = *(w, w); let yv = at(y, i); let yvP = at(y, +(i, 1)); let fd = at(firstDerivative, i); let fdP = at(firstDerivative, +(i, 1)); withoutTrailingZeros([yv, at(firstDerivative, i), /((-(-(/(*(3, (-(yvP, yv))), w), (*(2, fd))), fdP)), w), /((+(+(/(*(2, (-(yv, yvP))), w), fd), fdP)), w2)]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "hermiteInterpolatorCoefficientList",
  ["x", "y", "firstDerivative"],
  sl.annotateFunction(function (_x, _y, _firstDerivative) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _x, _y, _firstDerivative";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 2);
    _ifTrue_2(
      _tildeEqualsSign_2(_size_1(_x), _size_1(_firstDerivative)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _x,
          "hermiteInterpolatorCoefficientList: firstDerivative list invalid",
        );
      }, []),
    );
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _w = _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i));
        let _w2 = _asterisk_2(_w, _w);
        let _yv = _at_2(_y, _i);
        let _yvP = _at_2(_y, _plusSign_2(_i, 1));
        let _fd = _at_2(_firstDerivative, _i);
        let _fdP = _at_2(_firstDerivative, _plusSign_2(_i, 1));
        /* Statements */
        return _withoutTrailingZeros_1([
          _yv,
          _at_2(_firstDerivative, _i),
          _solidus_2(
            _hyphenMinus_2(
              _hyphenMinus_2(
                _solidus_2(_asterisk_2(3, _hyphenMinus_2(_yvP, _yv)), _w),
                _asterisk_2(2, _fd),
              ),
              _fdP,
            ),
            _w,
          ),
          _solidus_2(
            _plusSign_2(
              _plusSign_2(
                _solidus_2(_asterisk_2(2, _hyphenMinus_2(_yv, _yvP)), _w),
                _fd,
              ),
              _fdP,
            ),
            _w2,
          ),
        ]);
      }, ["i"]),
    );
  }, ["x", "y", "firstDerivative"]),
  "{ :x :y :firstDerivative | let n = -(size(x), 1); assertIsValidInterpolatorData(x, y, 2); ifTrue((~=(size(x), size(firstDerivative))), { error(x,'hermiteInterpolatorCoefficientList: firstDerivative list invalid') }); collect(upOrDownTo(1, n), { :i | let w = -(at(x, +(i, 1)), at(x, i)); let w2 = *(w, w); let yv = at(y, i); let yvP = at(y, +(i, 1)); let fd = at(firstDerivative, i); let fdP = at(firstDerivative, +(i, 1)); withoutTrailingZeros([yv, at(firstDerivative, i), /((-(-(/(*(3, (-(yvP, yv))), w), (*(2, fd))), fdP)), w), /((+(+(/(*(2, (-(yv, yvP))), w), fd), fdP)), w2)]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "interpolation",
  ["data", "method"],
  sl.annotateFunction(function (_data, _method) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _data, _method";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _f_2 = _caseOfOtherwise_3(
      _method,
      [
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Akima";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _akimaInterpolator_2;
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Linear";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _linearInterpolator_2;
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Spline";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _cubicSplineInterpolator_2;
          }, []),
        ),
      ],
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_data, "interpolation: unknown method");
      }, []),
    );
    /* Statements */
    return _if_3(
      _isVector_1(_data),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _y = _data;
        let _x = _asList_1(_upOrDownTo_2(1, _size_1(_y)));
        /* Statements */
        return _f_2(_x, _y);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let __SPL3 = _assertIsOfSize_2(_transposed_1(_data), 2);
        let _x = _at_2(__SPL3, 1);
        let _y = _at_2(__SPL3, 2);
        /* Statements */
        return _f_2(_x, _y);
      }, []),
    );
  }, ["data", "method"]),
  "{ :data :method | let f:/2 = caseOfOtherwise(method, [->({ 'Akima' }, { akimaInterpolator:/2 }), ->({ 'Linear' }, { linearInterpolator:/2 }), ->({ 'Spline' }, { cubicSplineInterpolator:/2 })], { error(data,'interpolation: unknown method') }); if(isVector(data), { let y = data; let x = asList(upOrDownTo(1, size(y))); f(x, y) }, { let __SPL3 = assertIsOfSize(transposed(data), 2); let x = at(__SPL3, 1); let y = at(__SPL3, 2); f(x, y) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "interpolation",
  ["data", "method"],
  sl.annotateFunction(function (_data, _method) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _data, _method";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _f_2 = _caseOfOtherwise_3(
      _method,
      [
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Akima";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _akimaInterpolator_2;
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Linear";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _linearInterpolator_2;
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return "Spline";
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _cubicSplineInterpolator_2;
          }, []),
        ),
      ],
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_data, "interpolation: unknown method");
      }, []),
    );
    /* Statements */
    return _if_3(
      _isVector_1(_data),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _y = _data;
        let _x = _asList_1(_upOrDownTo_2(1, _size_1(_y)));
        /* Statements */
        return _f_2(_x, _y);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let __SPL3 = _assertIsOfSize_2(_transposed_1(_data), 2);
        let _x = _at_2(__SPL3, 1);
        let _y = _at_2(__SPL3, 2);
        /* Statements */
        return _f_2(_x, _y);
      }, []),
    );
  }, ["data", "method"]),
  "{ :data :method | let f:/2 = caseOfOtherwise(method, [->({ 'Akima' }, { akimaInterpolator:/2 }), ->({ 'Linear' }, { linearInterpolator:/2 }), ->({ 'Spline' }, { cubicSplineInterpolator:/2 })], { error(data,'interpolation: unknown method') }); if(isVector(data), { let y = data; let x = asList(upOrDownTo(1, size(y))); f(x, y) }, { let __SPL3 = assertIsOfSize(transposed(data), 2); let x = at(__SPL3, 1); let y = at(__SPL3, 2); f(x, y) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "interpolation",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _interpolation_2(_self, "Spline");
  }, ["self"]),
  "{ :self | interpolation(self,'Spline') }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "interpolation",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _interpolation_2(_self, "Spline");
  }, ["self"]),
  "{ :self | interpolation(self,'Spline') }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "linearInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 2);
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _dx = _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i));
        let _dy = _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i));
        let _m = _solidus_2(_dy, _dx);
        /* Statements */
        return _withoutTrailingZeros_1([_at_2(_y, _i), _m]);
      }, ["i"]),
    );
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); assertIsValidInterpolatorData(x, y, 2); collect(upOrDownTo(1, n), { :i | let dx = -(at(x, +(i, 1)), at(x, i)); let dy = -(at(y, +(i, 1)), at(y, i)); let m = /(dy, dx); withoutTrailingZeros([at(y, i), m]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "linearInterpolatorCoefficientList",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _hyphenMinus_2(_size_1(_x), 1);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 2);
    return _collect_2(
      _upOrDownTo_2(1, _n),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _dx = _hyphenMinus_2(_at_2(_x, _plusSign_2(_i, 1)), _at_2(_x, _i));
        let _dy = _hyphenMinus_2(_at_2(_y, _plusSign_2(_i, 1)), _at_2(_y, _i));
        let _m = _solidus_2(_dy, _dx);
        /* Statements */
        return _withoutTrailingZeros_1([_at_2(_y, _i), _m]);
      }, ["i"]),
    );
  }, ["x", "y"]),
  "{ :x :y | let n = -(size(x), 1); assertIsValidInterpolatorData(x, y, 2); collect(upOrDownTo(1, n), { :i | let dx = -(at(x, +(i, 1)), at(x, i)); let dy = -(at(y, +(i, 1)), at(y, i)); let m = /(dy, dx); withoutTrailingZeros([at(y, i), m]) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "linearInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _linearInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = linearInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "linearInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _c = _linearInterpolatorCoefficientList_2(_x, _y);
    let _xCopy = _copy_1(_x);
    /* Statements */
    return sl.annotateFunction(function (_mu) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _mu";
        throw new Error(errorMessage);
      } /* Statements */
      return _evaluateInterpolatorSegment_3(_xCopy, _c, _mu);
    }, ["mu"]);
  }, ["x", "y"]),
  "{ :x :y | let c = linearInterpolatorCoefficientList(x,y); let xCopy = copy(x); { :mu | evaluateInterpolatorSegment(xCopy,c, mu) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "listInterpolation",
  ["self", "aBlock"],
  sl.annotateFunction(function (_self, _aBlock) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_self);
    /* Statements */
    return _caseOfOtherwise_3(
      _numArgs_1(_aBlock),
      [
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 3;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return sl.annotateFunction(function (_x) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _x";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _i = _integerPart_1(_x);
              /* Statements */
              return _if_3(
                _equalsSign_2(_i, _k),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _last_1(_self);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _aBlock(
                    _at_2(_self, _i),
                    _at_2(_self, _plusSign_2(_i, 1)),
                    _fractionPart_1(_x),
                  );
                }, []),
              );
            }, ["x"]);
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 5;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return sl.annotateFunction(function (_x) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _x";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _i = _integerPart_1(_x);
              /* Statements */
              return _if_3(
                _equalsSign_2(_i, _k),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _last_1(_self);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _aBlock(
                    _at_2(_self, _max_2(_hyphenMinus_2(_i, 1), 1)),
                    _at_2(_self, _i),
                    _at_2(_self, _plusSign_2(_i, 1)),
                    _at_2(_self, _min_2(_plusSign_2(_i, 2), _k)),
                    _fractionPart_1(_x),
                  );
                }, []),
              );
            }, ["x"]);
          }, []),
        ),
      ],
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "listInterpolation: not 3- or 5- argument block",
        );
      }, []),
    );
  }, ["self", "aBlock"]),
  "{ :self :aBlock | let k = size(self); caseOfOtherwise(numArgs(aBlock), [->({ 3 }, { { :x | let i = integerPart(x); if((=(i, k)), { last(self) }, { aBlock . (at(self, i), at(self, +(i, 1)), fractionPart(x)) }) } }), ->({ 5 }, { { :x | let i = integerPart(x); if((=(i, k)), { last(self) }, { aBlock . (at(self, max((-(i, 1)),1)), at(self, i), at(self, +(i, 1)), at(self, min((+(i, 2)),k)), fractionPart(x)) }) } })], { error(self,'listInterpolation: not 3- or 5- argument block') }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "listInterpolation",
  ["self", "aBlock"],
  sl.annotateFunction(function (_self, _aBlock) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_self);
    /* Statements */
    return _caseOfOtherwise_3(
      _numArgs_1(_aBlock),
      [
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 3;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return sl.annotateFunction(function (_x) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _x";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _i = _integerPart_1(_x);
              /* Statements */
              return _if_3(
                _equalsSign_2(_i, _k),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _last_1(_self);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _aBlock(
                    _at_2(_self, _i),
                    _at_2(_self, _plusSign_2(_i, 1)),
                    _fractionPart_1(_x),
                  );
                }, []),
              );
            }, ["x"]);
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 5;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return sl.annotateFunction(function (_x) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _x";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _i = _integerPart_1(_x);
              /* Statements */
              return _if_3(
                _equalsSign_2(_i, _k),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _last_1(_self);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _aBlock(
                    _at_2(_self, _max_2(_hyphenMinus_2(_i, 1), 1)),
                    _at_2(_self, _i),
                    _at_2(_self, _plusSign_2(_i, 1)),
                    _at_2(_self, _min_2(_plusSign_2(_i, 2), _k)),
                    _fractionPart_1(_x),
                  );
                }, []),
              );
            }, ["x"]);
          }, []),
        ),
      ],
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "listInterpolation: not 3- or 5- argument block",
        );
      }, []),
    );
  }, ["self", "aBlock"]),
  "{ :self :aBlock | let k = size(self); caseOfOtherwise(numArgs(aBlock), [->({ 3 }, { { :x | let i = integerPart(x); if((=(i, k)), { last(self) }, { aBlock . (at(self, i), at(self, +(i, 1)), fractionPart(x)) }) } }), ->({ 5 }, { { :x | let i = integerPart(x); if((=(i, k)), { last(self) }, { aBlock . (at(self, max((-(i, 1)),1)), at(self, i), at(self, +(i, 1)), at(self, min((+(i, 2)),k)), fractionPart(x)) }) } })], { error(self,'listInterpolation: not 3- or 5- argument block') }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "matrixInterpolation",
  ["self", "aBlock:/6"],
  sl.annotateFunction(function (_self, _aBlock_6) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_6";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL4 = _assertIsOfSize_2([
      _numberOfRows_1(_self),
      _numberOfColumns_1(_self),
    ], 2);
    let _m = _at_2(__SPL4, 1);
    let _n = _at_2(__SPL4, 2);
    /* Statements */
    return sl.annotateFunction(function (_x, _y) {
      /* ArityCheck */
      if (arguments.length !== 2) {
        const errorMessage = "Arity: expected 2, _x, _y";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i1 = _integerPart_1(_x);
      let _j1 = _integerPart_1(_y);
      let _i2 = _min_2(_m, _plusSign_2(_i1, 1));
      let _j2 = _min_2(_n, _plusSign_2(_j1, 1));
      /* Statements */
      return _aBlock_6(
        _at_2(_at_2(_self, _i1), _j1),
        _at_2(_at_2(_self, _i2), _j1),
        _at_2(_at_2(_self, _i1), _j2),
        _at_2(_at_2(_self, _i2), _j2),
        _fractionPart_1(_x),
        _fractionPart_1(_y),
      );
    }, ["x", "y"]);
  }, ["self", "aBlock:/6"]),
  "{ :self :aBlock:/6 | let __SPL4 = assertIsOfSize([numberOfRows(self), numberOfColumns(self)], 2); let m = at(__SPL4, 1); let n = at(__SPL4, 2); { :x :y | let i1 = integerPart(x); let j1 = integerPart(y); let i2 = min(m,+(i1, 1)); let j2 = min(n,+(j1, 1)); aBlock(at(at(self, i1), j1), at(at(self, i2), j1), at(at(self, i1), j2), at(at(self, i2), j2), fractionPart(x), fractionPart(y)) } }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "matrixInterpolation",
  ["self", "aBlock:/6"],
  sl.annotateFunction(function (_self, _aBlock_6) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_6";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL4 = _assertIsOfSize_2([
      _numberOfRows_1(_self),
      _numberOfColumns_1(_self),
    ], 2);
    let _m = _at_2(__SPL4, 1);
    let _n = _at_2(__SPL4, 2);
    /* Statements */
    return sl.annotateFunction(function (_x, _y) {
      /* ArityCheck */
      if (arguments.length !== 2) {
        const errorMessage = "Arity: expected 2, _x, _y";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i1 = _integerPart_1(_x);
      let _j1 = _integerPart_1(_y);
      let _i2 = _min_2(_m, _plusSign_2(_i1, 1));
      let _j2 = _min_2(_n, _plusSign_2(_j1, 1));
      /* Statements */
      return _aBlock_6(
        _at_2(_at_2(_self, _i1), _j1),
        _at_2(_at_2(_self, _i2), _j1),
        _at_2(_at_2(_self, _i1), _j2),
        _at_2(_at_2(_self, _i2), _j2),
        _fractionPart_1(_x),
        _fractionPart_1(_y),
      );
    }, ["x", "y"]);
  }, ["self", "aBlock:/6"]),
  "{ :self :aBlock:/6 | let __SPL4 = assertIsOfSize([numberOfRows(self), numberOfColumns(self)], 2); let m = at(__SPL4, 1); let n = at(__SPL4, 2); { :x :y | let i1 = integerPart(x); let j1 = integerPart(y); let i2 = min(m,+(i1, 1)); let j2 = min(n,+(j1, 1)); aBlock(at(at(self, i1), j1), at(at(self, i2), j1), at(at(self, i1), j2), at(at(self, i2), j2), fractionPart(x), fractionPart(y)) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "nearestNeighborInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _xCopy = _copy_1(_x);
    let _yCopy = _copy_1(_y);
    let _n = _size_1(_x);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 1);
    return _if_3(
      _equalsSign_2(_n, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return sl.annotateFunction(function (_mu) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _mu";
            throw new Error(errorMessage);
          } /* Statements */
          return _at_2(_yCopy, 1);
        }, ["mu"]);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return sl.annotateFunction(function (_mu) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _mu";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _i = _binaryDetectIndex_2(
            _n,
            sl.annotateFunction(function (_each) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _each";
                throw new Error(errorMessage);
              } /* Statements */
              return _greaterThanSignEqualsSign_2(_at_2(_xCopy, _each), _mu);
            }, ["each"]),
          );
          /* Statements */
          return _if_3(
            _ampersand_2(
              _lessThanSignEqualsSign_2(_i, _n),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _equalsSign_2(_at_2(_xCopy, _i), _mu);
              }, []),
            ),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _at_2(_yCopy, _i);
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _if_3(
                _equalsSign_2(_i, 1),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _at_2(_yCopy, 1);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _if_3(
                    _greaterThanSignEqualsSign_2(_i, _n),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _at_2(_yCopy, _n);
                    }, []),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Temporaries */
                      let _d = _hyphenMinus_2(
                        _mu,
                        _at_2(_xCopy, _hyphenMinus_2(_i, 1)),
                      );
                      let _w = _hyphenMinus_2(
                        _at_2(_xCopy, _i),
                        _at_2(_xCopy, _hyphenMinus_2(_i, 1)),
                      );
                      /* Statements */
                      return _if_3(
                        _lessThanSign_2(_plusSign_2(_d, _d), _w),
                        sl.annotateFunction(function () {
                          /* ArityCheck */
                          if (arguments.length !== 0) {
                            const errorMessage = "Arity: expected 0, ";
                            throw new Error(errorMessage);
                          } /* Statements */
                          return _at_2(_yCopy, _hyphenMinus_2(_i, 1));
                        }, []),
                        sl.annotateFunction(function () {
                          /* ArityCheck */
                          if (arguments.length !== 0) {
                            const errorMessage = "Arity: expected 0, ";
                            throw new Error(errorMessage);
                          } /* Statements */
                          return _at_2(_yCopy, _i);
                        }, []),
                      );
                    }, []),
                  );
                }, []),
              );
            }, []),
          );
        }, ["mu"]);
      }, []),
    );
  }, ["x", "y"]),
  "{ :x :y | let xCopy = copy(x); let yCopy = copy(y); let n = size(x); assertIsValidInterpolatorData(x, y, 1); if((=(n, 1)), { { :mu | at(yCopy, 1) } }, { { :mu | let i = binaryDetectIndex(n, { :each | >=(at(xCopy, each), mu) }); if((&(<=(i, n), { =(at(xCopy, i), mu) })), { at(yCopy, i) }, { if((=(i, 1)), { at(yCopy, 1) }, { if((>=(i, n)), { at(yCopy, n) }, { let d = -(mu, at(xCopy, -(i, 1))); let w = -(at(xCopy, i), at(xCopy, -(i, 1))); if((<(+(d, d), w)), { at(yCopy, -(i, 1)) }, { at(yCopy, i) }) }) }) }) } }) }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "nearestNeighborInterpolator",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _xCopy = _copy_1(_x);
    let _yCopy = _copy_1(_y);
    let _n = _size_1(_x);
    /* Statements */
    _assertIsValidInterpolatorData_3(_x, _y, 1);
    return _if_3(
      _equalsSign_2(_n, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return sl.annotateFunction(function (_mu) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _mu";
            throw new Error(errorMessage);
          } /* Statements */
          return _at_2(_yCopy, 1);
        }, ["mu"]);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return sl.annotateFunction(function (_mu) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _mu";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _i = _binaryDetectIndex_2(
            _n,
            sl.annotateFunction(function (_each) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _each";
                throw new Error(errorMessage);
              } /* Statements */
              return _greaterThanSignEqualsSign_2(_at_2(_xCopy, _each), _mu);
            }, ["each"]),
          );
          /* Statements */
          return _if_3(
            _ampersand_2(
              _lessThanSignEqualsSign_2(_i, _n),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _equalsSign_2(_at_2(_xCopy, _i), _mu);
              }, []),
            ),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _at_2(_yCopy, _i);
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _if_3(
                _equalsSign_2(_i, 1),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _at_2(_yCopy, 1);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _if_3(
                    _greaterThanSignEqualsSign_2(_i, _n),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _at_2(_yCopy, _n);
                    }, []),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Temporaries */
                      let _d = _hyphenMinus_2(
                        _mu,
                        _at_2(_xCopy, _hyphenMinus_2(_i, 1)),
                      );
                      let _w = _hyphenMinus_2(
                        _at_2(_xCopy, _i),
                        _at_2(_xCopy, _hyphenMinus_2(_i, 1)),
                      );
                      /* Statements */
                      return _if_3(
                        _lessThanSign_2(_plusSign_2(_d, _d), _w),
                        sl.annotateFunction(function () {
                          /* ArityCheck */
                          if (arguments.length !== 0) {
                            const errorMessage = "Arity: expected 0, ";
                            throw new Error(errorMessage);
                          } /* Statements */
                          return _at_2(_yCopy, _hyphenMinus_2(_i, 1));
                        }, []),
                        sl.annotateFunction(function () {
                          /* ArityCheck */
                          if (arguments.length !== 0) {
                            const errorMessage = "Arity: expected 0, ";
                            throw new Error(errorMessage);
                          } /* Statements */
                          return _at_2(_yCopy, _i);
                        }, []),
                      );
                    }, []),
                  );
                }, []),
              );
            }, []),
          );
        }, ["mu"]);
      }, []),
    );
  }, ["x", "y"]),
  "{ :x :y | let xCopy = copy(x); let yCopy = copy(y); let n = size(x); assertIsValidInterpolatorData(x, y, 1); if((=(n, 1)), { { :mu | at(yCopy, 1) } }, { { :mu | let i = binaryDetectIndex(n, { :each | >=(at(xCopy, each), mu) }); if((&(<=(i, n), { =(at(xCopy, i), mu) })), { at(yCopy, i) }, { if((=(i, 1)), { at(yCopy, 1) }, { if((>=(i, n)), { at(yCopy, n) }, { let d = -(mu, at(xCopy, -(i, 1))); let w = -(at(xCopy, i), at(xCopy, -(i, 1))); if((<(+(d, d), w)), { at(yCopy, -(i, 1)) }, { at(yCopy, i) }) }) }) }) } }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "volumeInterpolation",
  ["self", "aBlock:/11"],
  sl.annotateFunction(function (_self, _aBlock_11) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_11";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL5 = _assertIsOfSize_2(_shape_1(_self), 3);
    let _m = _at_2(__SPL5, 1);
    let _n = _at_2(__SPL5, 2);
    let _o = _at_2(__SPL5, 3);
    /* Statements */
    return sl.annotateFunction(function (_x, _y, _z) {
      /* ArityCheck */
      if (arguments.length !== 3) {
        const errorMessage = "Arity: expected 3, _x, _y, _z";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i1 = _integerPart_1(_x);
      let _j1 = _integerPart_1(_y);
      let _k1 = _integerPart_1(_z);
      let _i2 = _min_2(_m, _plusSign_2(_i1, 1));
      let _j2 = _min_2(_n, _plusSign_2(_j1, 1));
      let _k2 = _min_2(_o, _plusSign_2(_k1, 1));
      /* Statements */
      return _aBlock_11(
        _at_2(_at_2(_at_2(_self, _i1), _j1), _k1),
        _at_2(_at_2(_at_2(_self, _i2), _j1), _k1),
        _at_2(_at_2(_at_2(_self, _i1), _j2), _k1),
        _at_2(_at_2(_at_2(_self, _i2), _j2), _k1),
        _at_2(_at_2(_at_2(_self, _i1), _j1), _k2),
        _at_2(_at_2(_at_2(_self, _i2), _j1), _k2),
        _at_2(_at_2(_at_2(_self, _i1), _j2), _k2),
        _at_2(_at_2(_at_2(_self, _i2), _j2), _k2),
        _fractionPart_1(_x),
        _fractionPart_1(_y),
        _fractionPart_1(_z),
      );
    }, ["x", "y", "z"]);
  }, ["self", "aBlock:/11"]),
  "{ :self :aBlock:/11 | let __SPL5 = assertIsOfSize(shape(self), 3); let m = at(__SPL5, 1); let n = at(__SPL5, 2); let o = at(__SPL5, 3); { :x :y :z | let i1 = integerPart(x); let j1 = integerPart(y); let k1 = integerPart(z); let i2 = min(m,+(i1, 1)); let j2 = min(n,+(j1, 1)); let k2 = min(o,+(k1, 1)); aBlock(at(at(at(self, i1), j1), k1), at(at(at(self, i2), j1), k1), at(at(at(self, i1), j2), k1), at(at(at(self, i2), j2), k1), at(at(at(self, i1), j1), k2), at(at(at(self, i2), j1), k2), at(at(at(self, i1), j2), k2), at(at(at(self, i2), j2), k2), fractionPart(x), fractionPart(y), fractionPart(z)) } }",
);

sl.extendTypeOrTraitWithMethod(
  "Range",
  "Interpolation",
  "volumeInterpolation",
  ["self", "aBlock:/11"],
  sl.annotateFunction(function (_self, _aBlock_11) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_11";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL5 = _assertIsOfSize_2(_shape_1(_self), 3);
    let _m = _at_2(__SPL5, 1);
    let _n = _at_2(__SPL5, 2);
    let _o = _at_2(__SPL5, 3);
    /* Statements */
    return sl.annotateFunction(function (_x, _y, _z) {
      /* ArityCheck */
      if (arguments.length !== 3) {
        const errorMessage = "Arity: expected 3, _x, _y, _z";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i1 = _integerPart_1(_x);
      let _j1 = _integerPart_1(_y);
      let _k1 = _integerPart_1(_z);
      let _i2 = _min_2(_m, _plusSign_2(_i1, 1));
      let _j2 = _min_2(_n, _plusSign_2(_j1, 1));
      let _k2 = _min_2(_o, _plusSign_2(_k1, 1));
      /* Statements */
      return _aBlock_11(
        _at_2(_at_2(_at_2(_self, _i1), _j1), _k1),
        _at_2(_at_2(_at_2(_self, _i2), _j1), _k1),
        _at_2(_at_2(_at_2(_self, _i1), _j2), _k1),
        _at_2(_at_2(_at_2(_self, _i2), _j2), _k1),
        _at_2(_at_2(_at_2(_self, _i1), _j1), _k2),
        _at_2(_at_2(_at_2(_self, _i2), _j1), _k2),
        _at_2(_at_2(_at_2(_self, _i1), _j2), _k2),
        _at_2(_at_2(_at_2(_self, _i2), _j2), _k2),
        _fractionPart_1(_x),
        _fractionPart_1(_y),
        _fractionPart_1(_z),
      );
    }, ["x", "y", "z"]);
  }, ["self", "aBlock:/11"]),
  "{ :self :aBlock:/11 | let __SPL5 = assertIsOfSize(shape(self), 3); let m = at(__SPL5, 1); let n = at(__SPL5, 2); let o = at(__SPL5, 3); { :x :y :z | let i1 = integerPart(x); let j1 = integerPart(y); let k1 = integerPart(z); let i2 = min(m,+(i1, 1)); let j2 = min(n,+(j1, 1)); let k2 = min(o,+(k1, 1)); aBlock(at(at(at(self, i1), j1), k1), at(at(at(self, i2), j1), k1), at(at(at(self, i1), j2), k1), at(at(at(self, i2), j2), k1), at(at(at(self, i1), j1), k2), at(at(at(self, i2), j1), k2), at(at(at(self, i1), j2), k2), at(at(at(self, i2), j2), k2), fractionPart(x), fractionPart(y), fractionPart(z)) } }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "basicDownsampleSteinarsson",
  ["self", "threshold"],
  sl.annotateFunction(function (_self, _threshold) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _threshold";
      throw new Error(errorMessage);
    } /* Primitive */
    return sc.downsampleSteinarsson(_self, _threshold);
  }, ["self", "threshold"]),
  "{ :self :threshold | <primitive: return sc.downsampleSteinarsson(_self, _threshold);>\n }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "downsample",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _thenTo_3(1, _plusSign_2(1, _anInteger), _size_1(_self)),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _at_2(_self, _each);
      }, ["each"]),
    );
  }, ["self", "anInteger"]),
  "{ :self :anInteger | collect(thenTo(1, +(1, anInteger), size(self)), { :each | at(self, each) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "downsampleSteinarsson",
  ["self", "threshold"],
  sl.annotateFunction(function (_self, _threshold) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _threshold";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isVector_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _basicDownsampleSteinarsson_2(
          _transposed_1([_indices_1(_self), _self]),
          _threshold,
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _basicDownsampleSteinarsson_2(_self, _threshold);
      }, []),
    );
  }, ["self", "threshold"]),
  "{ :self :threshold | if(isVector(self), { basicDownsampleSteinarsson(transposed([indices(self), self]),threshold) }, { basicDownsampleSteinarsson(self,threshold) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "matrixResample",
  ["self", "shape"],
  sl.annotateFunction(function (_self, _shape) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _shape";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL6 = _assertIsOfSize_2([
      _numberOfRows_1(_self),
      _numberOfColumns_1(_self),
    ], 2);
    let _m = _at_2(__SPL6, 1);
    let _n = _at_2(__SPL6, 2);
    let __SPL7 = _assertIsOfSize_2(_shape, 2);
    let _p = _at_2(__SPL7, 1);
    let _q = _at_2(__SPL7, 2);
    let _i = _discretize_2(_hyphenMinusHyphenMinus_2(1, _m), _p);
    let _j = _discretize_2(_hyphenMinusHyphenMinus_2(1, _n), _q);
    /* Statements */
    return _table_3(
      _matrixInterpolation_2(_self, _bilinearInterpolation_6),
      _i,
      _j,
    );
  }, ["self", "shape"]),
  "{ :self :shape | let __SPL6 = assertIsOfSize([numberOfRows(self), numberOfColumns(self)], 2); let m = at(__SPL6, 1); let n = at(__SPL6, 2); let __SPL7 = assertIsOfSize(shape, 2); let p = at(__SPL7, 1); let q = at(__SPL7, 2); let i = discretize((--(1, m)),p); let j = discretize((--(1, n)),q); table(matrixInterpolation(self,bilinearInterpolation:/6),i, j) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "resample",
  ["self", "newSize"],
  sl.annotateFunction(function (_self, _newSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _newSize";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _factor = _solidus_2(
      _hyphenMinus_2(_size_1(_self), 1),
      _max_2(_hyphenMinus_2(_newSize, 1), 1),
    );
    /* Statements */
    return _collect_2(
      _to_2(0, _hyphenMinus_2(_newSize, 1)),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _atBlend_2(_self, _plusSign_2(1, _asterisk_2(_each, _factor)));
      }, ["each"]),
    );
  }, ["self", "newSize"]),
  "{ :self :newSize | let factor = /((-(size(self), 1)), max((-(newSize, 1)),1)); collect(to(0,-(newSize, 1)), { :each | atBlend(self,+(1, (*(each, factor)))) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "upsample",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _List_2(_asterisk_2(_size_1(_self), _anInteger), 0);
    /* Statements */
    _do_2(
      _to_2(0, _hyphenMinus_2(_size_1(_self), 1)),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _answer,
          _plusSign_2(_asterisk_2(_each, _anInteger), 1),
          _at_2(_self, _plusSign_2(_each, 1)),
        );
      }, ["each"]),
    );
    return _answer;
  }, ["self", "anInteger"]),
  "{ :self :anInteger | let answer = List(*(size(self), anInteger), 0); do(to(0,-(size(self), 1)), { :each | atPut(answer, +((*(each, anInteger)), 1), at(self, +(each, 1))) }); answer }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "volumeResample",
  ["self", "shape"],
  sl.annotateFunction(function (_self, _shape) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _shape";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL8 = _assertIsOfSize_2(_shape_1(_self), 3);
    let _m = _at_2(__SPL8, 1);
    let _n = _at_2(__SPL8, 2);
    let _o = _at_2(__SPL8, 3);
    let __SPL9 = _assertIsOfSize_2(_shape, 3);
    let _p = _at_2(__SPL9, 1);
    let _q = _at_2(__SPL9, 2);
    let _r = _at_2(__SPL9, 3);
    let _i = _discretize_2(_hyphenMinusHyphenMinus_2(1, _m), _p);
    let _j = _discretize_2(_hyphenMinusHyphenMinus_2(1, _n), _q);
    let _k = _discretize_2(_hyphenMinusHyphenMinus_2(1, _o), _r);
    /* Statements */
    return _table_4(
      _volumeInterpolation_2(_self, _trilinearInterpolation_11),
      _i,
      _j,
      _k,
    );
  }, ["self", "shape"]),
  "{ :self :shape | let __SPL8 = assertIsOfSize(shape(self), 3); let m = at(__SPL8, 1); let n = at(__SPL8, 2); let o = at(__SPL8, 3); let __SPL9 = assertIsOfSize(shape, 3); let p = at(__SPL9, 1); let q = at(__SPL9, 2); let r = at(__SPL9, 3); let i = discretize((--(1, m)),p); let j = discretize((--(1, n)),q); let k = discretize((--(1, o)),r); table(volumeInterpolation(self,trilinearInterpolation:/11),i, j, k) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "inverseSmoothstep",
  ["x"],
  sl.annotateFunction(function (_x) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _x";
      throw new Error(errorMessage);
    } /* Statements */
    return _hyphenMinus_2(
      0.5,
      _sin_1(_solidus_2(_arcSin_1(_hyphenMinus_2(1, _asterisk_2(2, _x))), 3)),
    );
  }, ["x"]),
  "{ :x | -(0.5, sin((/(arcSin((-(1, (*(2, x))))), 3)))) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "smoothstepFunction",
  ["n"],
  sl.annotateFunction(function (_n) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return sl.annotateFunction(function (_x) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _x";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _answer = 0;
      /* Statements */
      _toDo_3(
        0,
        _n,
        sl.annotateFunction(function (_i) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _i";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _p = _binomialPascal_2(_hyphenMinus_2(_negated_1(_n), 1), _i);
          let _q = _binomialPascal_2(
            _plusSign_2(_asterisk_2(2, _n), 1),
            _hyphenMinus_2(_n, _i),
          );
          let _r = _circumflexAccent_2(_x, _plusSign_2(_plusSign_2(_n, _i), 1));
          /* Statements */
          return _answer = _plusSign_2(
            _answer,
            _asterisk_2(_asterisk_2(_p, _q), _r),
          );
        }, ["i"]),
      );
      return _answer;
    }, ["x"]);
  }, ["n"]),
  "{ :n | { :x | let answer = 0; toDo(0, n, { :i | let p = binomialPascal(-(negated(n), 1), i); let q = binomialPascal(+(*(2, n), 1), -(n, i)); let r = ^(x, (+(+(n, i), 1))); answer := +(answer, (*(*(p, q), r))) }); answer } }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "smoothstep",
  ["x"],
  sl.annotateFunction(function (_x) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _x";
      throw new Error(errorMessage);
    } /* Statements */
    return _asterisk_2(
      _asterisk_2(_x, _x),
      _hyphenMinus_2(3, _asterisk_2(2, _x)),
    );
  }, ["x"]),
  "{ :x | *(*(x, x), (-(3, (*(2, x))))) }",
);

sl.extendTypeOrTraitWithMethod(
  "SmallFloat",
  "Interpolation",
  "smootherstep",
  ["x"],
  sl.annotateFunction(function (_x) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _x";
      throw new Error(errorMessage);
    } /* Statements */
    return _asterisk_2(
      _asterisk_2(_asterisk_2(_x, _x), _x),
      _plusSign_2(_asterisk_2(_x, _hyphenMinus_2(_asterisk_2(6, _x), 15)), 10),
    );
  }, ["x"]),
  "{ :x | *(*(*(x, x), x), (+(*(x, (-(*(6, x), 15))), 10))) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Interpolation",
  "simpleLinearRegression",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let __SPL10 = _assertIsOfSize_2(_transposed_1(_self), 2);
    let _x = _at_2(__SPL10, 1);
    let _y = _at_2(__SPL10, 2);
    let _sx = _sum_1(_x);
    let _sy = _sum_1(_y);
    let _sxx = _sum_1(_asterisk_2(_x, _x));
    let _syy = _sum_1(_asterisk_2(_y, _y));
    let _sxy = _sum_1(_asterisk_2(_x, _y));
    let _xx = _hyphenMinus_2(_sxx, _solidus_2(_asterisk_2(_sx, _sx), _n));
    let _yy = _hyphenMinus_2(_syy, _solidus_2(_asterisk_2(_sy, _sy), _n));
    let _xy = _hyphenMinus_2(_sxy, _solidus_2(_asterisk_2(_sx, _sy), _n));
    /* Statements */
    return _if_3(
      _verticalLine_2(
        _lessThanSign_2(_n, 2),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_abs_1(_xx), 0);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "List>>simpleLinearRegression: too few points or infinite slope",
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _b = _solidus_2(_xy, _xx);
        let _a = _hyphenMinus_2(
          _solidus_2(_sy, _n),
          _solidus_2(_asterisk_2(_b, _sx), _n),
        );
        let _r = _if_3(
          _equalsSign_2(_abs_1(_yy), 0),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 1;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _solidus_2(_xy, _sqrt_1(_asterisk_2(_xx, _yy)));
          }, []),
        );
        /* Statements */
        return [_a, _b, _r];
      }, []),
    );
  }, ["self"]),
  "{ :self | let n = size(self); let __SPL10 = assertIsOfSize(transposed(self), 2); let x = at(__SPL10, 1); let y = at(__SPL10, 2); let sx = sum(x); let sy = sum(y); let sxx = sum((*(x, x))); let syy = sum((*(y, y))); let sxy = sum((*(x, y))); let xx = -(sxx, (/(*(sx, sx), n))); let yy = -(syy, (/(*(sy, sy), n))); let xy = -(sxy, (/(*(sx, sy), n))); if((|(<(n, 2), { =(abs(xx), 0) })), { error(self,'List>>simpleLinearRegression: too few points or infinite slope') }, { let b = /(xy, xx); let a = -((/(sy, n)), (/(*(b, sx), n))); let r = if((=(abs(yy), 0)), { 1 }, { /(xy, sqrt((*(xx, yy)))) }); [a, b, r] }) }",
);
