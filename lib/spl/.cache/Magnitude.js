sl.addTrait("Magnitude", "Magnitude");

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "lessThanSign",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _typeResponsibility_2(_self, "@Magnitude>><");
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | typeResponsibility(self,'@Magnitude>><') }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "lessThanSignEqualsSign",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _verticalLine_2(
      _lessThanSign_2(_self, _aMagnitude),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _equalsSign_2(_self, _aMagnitude);
      }, []),
    );
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | |(<(self, aMagnitude), { =(self, aMagnitude) }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "greaterThanSign",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _lessThanSign_2(_aMagnitude, _self);
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | <(aMagnitude, self) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "greaterThanSignEqualsSign",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _lessThanSignEqualsSign_2(_aMagnitude, _self);
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | <=(aMagnitude, self) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "lessThanSignEqualsSignGreaterThanSign",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _equalsSign_2(_self, _aMagnitude),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 0;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _lessThanSign_2(_self, _aMagnitude),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return -1;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 1;
          }, []),
        );
      }, []),
    );
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | if((=(self, aMagnitude)), { 0 }, { if((<(self, aMagnitude)), { -1 }, { 1 }) }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "betweenAnd",
  ["self", "min", "max"],
  sl.annotateFunction(function (_self, _min, _max) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _min, _max";
      throw new Error(errorMessage);
    } /* Statements */
    return _ampersand_2(
      _lessThanSignEqualsSign_2(_min, _self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSignEqualsSign_2(_self, _max);
      }, []),
    );
  }, ["self", "min", "max"]),
  "{ :self :min :max | &(<=(min, self), { <=(self, max) }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clamp",
  ["self", "lowMagnitude", "highMagnitude"],
  sl.annotateFunction(function (_self, _lowMagnitude, _highMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _lowMagnitude, _highMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _max_2(_min_2(_self, _highMagnitude), _lowMagnitude);
  }, ["self", "lowMagnitude", "highMagnitude"]),
  "{ :self :lowMagnitude :highMagnitude | max(min(self,highMagnitude),lowMagnitude) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clampLow",
  ["self", "lowMagnitude"],
  sl.annotateFunction(function (_self, _lowMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _lowMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _max_2(_self, _lowMagnitude);
  }, ["self", "lowMagnitude"]),
  "{ :self :lowMagnitude | max(self,lowMagnitude) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clampHigh",
  ["self", "highMagnitude"],
  sl.annotateFunction(function (_self, _highMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _highMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _min_2(_self, _highMagnitude);
  }, ["self", "highMagnitude"]),
  "{ :self :highMagnitude | min(self,highMagnitude) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clip",
  ["self", "min", "max", "vMin", "vMax"],
  sl.annotateFunction(function (_self, _min, _max, _vMin, _vMax) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage = "Arity: expected 5, _self, _min, _max, _vMin, _vMax";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_self, _min),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _vMin;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _greaterThanSign_2(_self, _max),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _vMax;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _self;
          }, []),
        );
      }, []),
    );
  }, ["self", "min", "max", "vMin", "vMax"]),
  "{ :self :min :max :vMin :vMax | if((<(self, min)), { vMin }, { if((>(self, max)), { vMax }, { self }) }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clip",
  ["self", "min", "max"],
  sl.annotateFunction(function (_self, _min, _max) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _min, _max";
      throw new Error(errorMessage);
    } /* Statements */
    return _clip_5(_self, _min, _max, _min, _max);
  }, ["self", "min", "max"]),
  "{ :self :min :max | clip(self,min, max, min, max) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "clip",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _clip_5(_self, -1, 1, -1, 1);
  }, ["self"]),
  "{ :self | clip(self,-1, 1, -1, 1) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "inRangeOfAnd",
  ["self", "first", "second"],
  sl.annotateFunction(function (_self, _first, _second) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _first, _second";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_first, _second),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _betweenAnd_3(_self, _first, _second);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _betweenAnd_3(_self, _second, _first);
      }, []),
    );
  }, ["self", "first", "second"]),
  "{ :self :first :second | if((<(first, second)), { betweenAnd(self,first, second) }, { betweenAnd(self,second, first) }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "max",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_self, _aMagnitude),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aMagnitude;
      }, []),
    );
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | if((>(self, aMagnitude)), { self }, { aMagnitude }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "min",
  ["self", "aMagnitude"],
  sl.annotateFunction(function (_self, _aMagnitude) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aMagnitude";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_self, _aMagnitude),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aMagnitude;
      }, []),
    );
  }, ["self", "aMagnitude"]),
  "{ :self :aMagnitude | if((<(self, aMagnitude)), { self }, { aMagnitude }) }",
);

sl.addMethodToExistingTrait(
  "Magnitude",
  "Magnitude",
  "minMax",
  ["self", "aMin", "aMax"],
  sl.annotateFunction(function (_self, _aMin, _aMax) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aMin, _aMax";
      throw new Error(errorMessage);
    } /* Statements */
    return _max_2(_min_2(_self, _aMin), _aMax);
  }, ["self", "aMin", "aMax"]),
  "{ :self :aMin :aMax | max(min(self,aMin),aMax) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Object",
  "Magnitude",
  "maxBy",
  ["self", "aMagnitude", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aMagnitude, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aMagnitude, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_aBlock_1(_self), _aBlock_1(_aMagnitude)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aMagnitude;
      }, []),
    );
  }, ["self", "aMagnitude", "aBlock:/1"]),
  "{ :self :aMagnitude :aBlock:/1 | if((>(aBlock(self), aBlock(aMagnitude))), { self }, { aMagnitude }) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Object",
  "Magnitude",
  "minBy",
  ["self", "aMagnitude", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aMagnitude, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aMagnitude, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_aBlock_1(_self), _aBlock_1(_aMagnitude)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aMagnitude;
      }, []),
    );
  }, ["self", "aMagnitude", "aBlock:/1"]),
  "{ :self :aMagnitude :aBlock:/1 | if((<(aBlock(self), aBlock(aMagnitude))), { self }, { aMagnitude }) }",
);
