/*  Require: SmallFloat  */

sl.addTrait("Sequenceable", "Sequenceable");

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "equalsSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _equalBy_3(_self, _anObject, _equalsSign_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | equalBy(self,anObject, =) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "tilde",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _equalBy_3(_self, _anObject, _tilde_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | equalBy(self,anObject, ~) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "plusSignPlusSign",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyReplaceFromToWith_4(
      _self,
      _plusSign_2(_size_1(_self), 1),
      _size_1(_self),
      _aList,
    );
  }, ["self", "aList"]),
  "{ :self :aList | copyReplaceFromToWith(self,+(size(self), 1), size(self), aList) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "plusSignPlusSign",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _catenate_1(_self);
  }, ["self"]),
  "{ :self | catenate(self) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "plusSignPlusSignPlusSign",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return (_each_1(_plusSignPlusSign_2)(_self, _aList));
  }, ["self", "aList"]),
  "{ :self :aList | (each(++) . (self, aList)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "numberSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _replicateEach_2(_self, _anObject);
  }, ["self", "anObject"]),
  "{ :self :anObject | replicateEach(self,anObject) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "exclamationMark",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _duplicateShape_2(_self, _anObject);
  }, ["self", "anObject"]),
  "{ :self :anObject | duplicateShape(self,anObject) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "ampersandAmpersand",
  ["self", "other"],
  sl.annotateFunction(function (_self, _other) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _other";
      throw new Error(errorMessage);
    } /* Statements */
    return _withCollect_3(_self, _other, _ampersandAmpersand_2);
  }, ["self", "other"]),
  "{ :self :other | withCollect(self,other, &&) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "verticalLineVerticalLine",
  ["self", "other"],
  sl.annotateFunction(function (_self, _other) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _other";
      throw new Error(errorMessage);
    } /* Statements */
    return _withCollect_3(_self, _other, _verticalLineVerticalLine_2);
  }, ["self", "other"]),
  "{ :self :other | withCollect(self,other, ||) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "accumulate",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _scan_2(_self, _plusSign_2);
  }, ["self"]),
  "{ :self | scan(self,+) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "adaptToCollectionAndApply",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isSequenceable_1(_anObject),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withCollect_3(_anObject, _self, _aBlock_2);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "@Sequenceable: only sequenceable collections may be processed elementwise",
        );
      }, []),
    );
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | if(isSequenceable(anObject), { withCollect(anObject,self, aBlock:/2) }, { error(self,'@Sequenceable: only sequenceable collections may be processed elementwise') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "adjacentPairs",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _adjacentPairsCollect_2(
      _self,
      sl.annotateFunction(function (_i, _j) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _i, _j";
          throw new Error(errorMessage);
        } /* Statements */
        return _newFrom_2(_species_1(_self), [_i, _j]);
      }, ["i", "j"]),
    );
  }, ["self"]),
  "{ :self | adjacentPairsCollect(self, { :i :j | newFrom(species(self),[i, j]) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "adjacentPairsDo",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      1,
      _hyphenMinus_2(_size_1(_self), 1),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_at_2(_self, _i), _at_2(_self, _plusSign_2(_i, 1)));
      }, ["i"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | toDo(1, -(size(self), 1), { :i | aBlock(at(self, i), at(self, +(i, 1))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "adjacentPairsCollect",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _new_2(_species_1(_self), _hyphenMinus_2(_size_1(_self), 1));
    let _index = 1;
    /* Statements */
    _adjacentPairsDo_2(
      _self,
      sl.annotateFunction(function (_p, _q) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _p, _q";
          throw new Error(errorMessage);
        } /* Statements */
        _atPut_3(_answer, _index, _aBlock_2(_p, _q));
        return _index = _plusSign_2(_index, 1);
      }, ["p", "q"]),
    );
    return _answer;
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | let answer = new(species(self),-(size(self), 1)); let index = 1; adjacentPairsDo(self, { :p :q | atPut(answer, index, aBlock(p, q)); index := +(index, 1) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "after",
  ["self", "target"],
  sl.annotateFunction(function (_self, _target) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _target";
      throw new Error(errorMessage);
    } /* Statements */
    return _afterIfAbsent_3(
      _self,
      _target,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _errorNotFound_2(_self, _target);
      }, []),
    );
  }, ["self", "target"]),
  "{ :self :target | afterIfAbsent(self, target, { errorNotFound(self,target) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "afterIfAbsent",
  ["self", "target", "exceptionBlock:/0"],
  sl.annotateFunction(function (_self, _target, _exceptionBlock_0) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _target, _exceptionBlock_0";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _indexOf_2(_self, _target);
    /* Statements */
    return _if_3(
      _verticalLine_2(
        _equalsSign_2(_index, 0),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_index, _size_1(_self));
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _exceptionBlock_0();
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _at_2(_self, _plusSign_2(_index, 1));
      }, []),
    );
  }, ["self", "target", "exceptionBlock:/0"]),
  "{ :self :target :exceptionBlock:/0 | let index = indexOf(self,target); if((|(=(index, 0), { =(index, size(self)) })), { exceptionBlock() }, { at(self, +(index, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButFirst",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _allButFirst_2(_self, 1);
  }, ["self"]),
  "{ :self | allButFirst(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButFirst",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, _plusSign_2(1, _n), _size_1(_self));
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,+(1, n), size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButFirstAndLast",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _allButFirstAndLast_2(_self, 1);
  }, ["self"]),
  "{ :self | allButFirstAndLast(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButFirstAndLast",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(
      _self,
      _plusSign_2(1, _n),
      _hyphenMinus_2(_size_1(_self), _n),
    );
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,+(1, n), -(size(self), n)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButFirstDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      _plusSign_2(1, 1),
      _size_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toDo((+(1, 1)), size(self), { :index | aBlock(at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButLast",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _allButLast_2(_self, 1);
  }, ["self"]),
  "{ :self | allButLast(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButLast",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, 1, _hyphenMinus_2(_size_1(_self), _n));
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,1, -(size(self), n)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "allButLastDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      1,
      _hyphenMinus_2(_size_1(_self), 1),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toDo(1, -(size(self), 1), { :index | aBlock(at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "asDigitsAtInDo",
  ["self", "anInteger", "aCollection", "aBlock:/1"],
  sl.annotateFunction(function (_self, _anInteger, _aCollection, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _anInteger, _aCollection, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _do_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        _atPut_3(_aCollection, _anInteger, _each);
        return _if_3(
          _equalsSign_2(_anInteger, _size_1(_aCollection)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_1(_aCollection);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _asDigitsAtInDo_4(
              _self,
              _plusSign_2(_anInteger, 1),
              _aCollection,
              _aBlock_1,
            );
          }, []),
        );
      }, ["each"]),
    );
  }, ["self", "anInteger", "aCollection", "aBlock:/1"]),
  "{ :self :anInteger :aCollection :aBlock:/1 | do(self, { :each | atPut(aCollection, anInteger, each); if((=(anInteger, size(aCollection))), { aBlock(aCollection) }, { asDigitsAtInDo(self,+(anInteger, 1), aCollection, aBlock:/1) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "asDigitsToPowerDo",
  ["self", "anInteger", "aBlock:/1"],
  sl.annotateFunction(function (_self, _anInteger, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anInteger, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _aCollection = _List_1(_anInteger);
    /* Statements */
    return _asDigitsAtInDo_4(_self, 1, _aCollection, _aBlock_1);
  }, ["self", "anInteger", "aBlock:/1"]),
  "{ :self :anInteger :aBlock:/1 | let aCollection = List(anInteger); asDigitsAtInDo(self,1, aCollection, aBlock:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "asRange",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _Range_3(1, 0, 1);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _equalsSign_2(_size_1(_self), 1),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _Range_3(_first_1(_self), _first_1(_self), 1);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _isArithmeticSeries_1(_self),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _Range_3(
                  _first_1(_self),
                  _last_1(_self),
                  _hyphenMinus_2(_second_1(_self), _first_1(_self)),
                );
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _error_2(
                  _self,
                  "@Sequenceable>>asRange: not an arithmetic series",
                );
              }, []),
            );
          }, []),
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | if(isEmpty(self), { Range(1, 0, 1) }, { if((=(size(self), 1)), { Range(first(self), first(self), 1) }, { if(isArithmeticSeries(self), { Range(first(self), last(self), -(second(self), first(self))) }, { error(self,'@Sequenceable>>asRange: not an arithmetic series') }) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "asRangeList",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _equalsSign_2(_size_1(_self), 1),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return [_Range_3(_first_1(_self), _first_1(_self), 1)];
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _answer = [];
            let _start = _first_1(_self);
            let _step = _hyphenMinus_2(_second_1(_self), _first_1(_self));
            /* Statements */
            _toDo_3(
              _plusSign_2(1, 1),
              _size_1(_self),
              sl.annotateFunction(function (_i) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _i";
                  throw new Error(errorMessage);
                } /* Statements */
                return _ifFalse_2(
                  _equalsSign_2(
                    _hyphenMinus_2(
                      _at_2(_self, _i),
                      _at_2(_self, _hyphenMinus_2(_i, 1)),
                    ),
                    _step,
                  ),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    _add_2(
                      _answer,
                      _Range_3(
                        _start,
                        _at_2(_self, _hyphenMinus_2(_i, 1)),
                        _step,
                      ),
                    );
                    _start = _at_2(_self, _i);
                    return _if_3(
                      _equalsSign_2(_i, _size_1(_self)),
                      sl.annotateFunction(function () {
                        /* ArityCheck */
                        if (arguments.length !== 0) {
                          const errorMessage = "Arity: expected 0, ";
                          throw new Error(errorMessage);
                        } /* Statements */
                        _add_2(
                          _answer,
                          _Range_3(_last_1(_self), _last_1(_self), 1),
                        );
                        return _start = null;
                      }, []),
                      sl.annotateFunction(function () {
                        /* ArityCheck */
                        if (arguments.length !== 0) {
                          const errorMessage = "Arity: expected 0, ";
                          throw new Error(errorMessage);
                        } /* Statements */
                        return _step = _hyphenMinus_2(
                          _at_2(_self, _plusSign_2(_i, 1)),
                          _at_2(_self, _i),
                        );
                      }, []),
                    );
                  }, []),
                );
              }, ["i"]),
            );
            _ifNotNil_2(
              _start,
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _add_2(_answer, _Range_3(_start, _last_1(_self), _step));
              }, []),
            );
            return _answer;
          }, []),
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | if(isEmpty(self), { [] }, { if((=(size(self), 1)), { [Range(first(self), first(self), 1)] }, { let answer = []; let start = first(self); let step = -(second(self), first(self)); toDo((+(1, 1)), size(self), { :i | ifFalse((=(-(at(self, i), at(self, -(i, 1))), step)), { add(answer,Range(start, at(self, -(i, 1)), step)); start := at(self, i); if((=(i, size(self))), { add(answer,Range(last(self), last(self), 1)); start := nil }, { step := -(at(self, +(i, 1)), at(self, i)) }) }) }); ifNotNil(start, { add(answer,Range(start, last(self), step)) }); answer }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "assertShape",
  ["self", "shape"],
  sl.annotateFunction(function (_self, _shape) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _shape";
      throw new Error(errorMessage);
    } /* Statements */
    return _assert_2(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _equalsSign_2(_shape_1(_self), _shape);
      }, []),
    );
  }, ["self", "shape"]),
  "{ :self :shape | assert(self, { =(shape(self), shape) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllUsing",
  ["self", "indexList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _indexList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _indexList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _ofSize_2(_species_1(_self), _size_1(_indexList));
    /* Statements */
    _indicesDo_2(
      _indexList,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _answer,
          _index,
          _aBlock_2(_self, _at_2(_indexList, _index)),
        );
      }, ["index"]),
    );
    return _answer;
  }, ["self", "indexList", "aBlock:/2"]),
  "{ :self :indexList :aBlock:/2 | let answer = ofSize(species(self),size(indexList)); indicesDo(indexList, { :index | atPut(answer, index, aBlock(self, at(indexList, index))) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAll",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _at_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, at:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAll",
  ["self", "primaryIndices", "secondaryIndices"],
  sl.annotateFunction(function (_self, _primaryIndices, _secondaryIndices) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _primaryIndices, _secondaryIndices";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _atAll_2(_self, _primaryIndices),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _atAll_2(_each, _secondaryIndices);
      }, ["each"]),
    );
  }, ["self", "primaryIndices", "secondaryIndices"]),
  "{ :self :primaryIndices :secondaryIndices | collect(atAll(self,primaryIndices), { :each | atAll(each,secondaryIndices) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAll",
  ["self", "primaryIndices", "secondaryIndices", "tertiaryIndices"],
  sl.annotateFunction(
    function (_self, _primaryIndices, _secondaryIndices, _tertiaryIndices) {
      /* ArityCheck */
      if (arguments.length !== 4) {
        const errorMessage =
          "Arity: expected 4, _self, _primaryIndices, _secondaryIndices, _tertiaryIndices";
        throw new Error(errorMessage);
      } /* Statements */
      return _collect_2(
        _atAll_2(_self, _primaryIndices),
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          return _atAll_3(_each, _secondaryIndices, _tertiaryIndices);
        }, ["each"]),
      );
    },
    ["self", "primaryIndices", "secondaryIndices", "tertiaryIndices"],
  ),
  "{ :self :primaryIndices :secondaryIndices :tertiaryIndices | collect(atAll(self,primaryIndices), { :each | atAll(each,secondaryIndices, tertiaryIndices) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllFold",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _atFold_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, atFold:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllMissing",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _atMissing_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, atMissing:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllPin",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _atPin_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, atPin:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllPath",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _atPath_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, atPath:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllPut",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _size = _size_1(_self);
    /* Statements */
    _if_3(
      _greaterThanSign_2(_size, 50),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _fromToPut_4(_self, 1, _size_1(_self), _anObject);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _indicesDo_2(
          _self,
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            return _atPut_3(_self, _index, _anObject);
          }, ["index"]),
        );
      }, []),
    );
    return _anObject;
  }, ["self", "anObject"]),
  "{ :self :anObject | let size = size(self); if((>(size, 50)), { fromToPut(self,1, size(self), anObject) }, { indicesDo(self, { :index | atPut(self, index, anObject) }) }); anObject }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllValid",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _deleteMissing_1(_atAllMissing_2(_self, _indexList));
  }, ["self", "indexList"]),
  "{ :self :indexList | deleteMissing(atAllMissing(self,indexList)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atAllWrap",
  ["self", "indexList"],
  sl.annotateFunction(function (_self, _indexList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indexList";
      throw new Error(errorMessage);
    } /* Statements */
    return _atAllUsing_3(_self, _indexList, _atWrap_2);
  }, ["self", "indexList"]),
  "{ :self :indexList | atAllUsing(self,indexList, atWrap:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atFold",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _foldedIndex_2(_self, _index));
  }, ["self", "index"]),
  "{ :self :index | at(self, foldedIndex(self,index)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atLastPut",
  ["self", "indexFromEnd", "anObject"],
  sl.annotateFunction(function (_self, _indexFromEnd, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _indexFromEnd, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _atPut_3(
      _self,
      _hyphenMinus_2(_plusSign_2(_size_1(_self), 1), _indexFromEnd),
      _anObject,
    );
  }, ["self", "indexFromEnd", "anObject"]),
  "{ :self :indexFromEnd :anObject | atPut(self, -(+(size(self), 1), indexFromEnd), anObject) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atMod",
  ["self", "index", "n"],
  sl.annotateFunction(function (_self, _index, _n) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _index, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(
      _self,
      _plusSign_2(_percentSign_2(_hyphenMinus_2(_index, 1), _n), 1),
    );
  }, ["self", "index", "n"]),
  "{ :self :index :n | at(self, +(%(-(index, 1), n), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atMod",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    /* Statements */
    return _at_2(
      _self,
      _plusSign_2(_percentSign_2(_hyphenMinus_2(_index, 1), _n), 1),
    );
  }, ["self", "index"]),
  "{ :self :index | let n = size(self); at(self, +(%(-(index, 1), n), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atPin",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _pinnedIndex_2(_self, _index));
  }, ["self", "index"]),
  "{ :self :index | at(self, pinnedIndex(self,index)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atPutPin",
  ["self", "index", "value"],
  sl.annotateFunction(function (_self, _index, _value) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _index, _value";
      throw new Error(errorMessage);
    } /* Statements */
    return _atPut_3(_self, _pinnedIndex_2(_self, _index), _value);
  }, ["self", "index", "value"]),
  "{ :self :index :value | atPut(self,pinnedIndex(self,index), value) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atPutWrap",
  ["self", "index", "anObject"],
  sl.annotateFunction(function (_self, _index, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _index, _anObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _wrapBetweenAnd_3(_index, 1, _plusSign_2(_size_1(_self), 1));
    /* Statements */
    return _atPut_3(_self, _k, _anObject);
  }, ["self", "index", "anObject"]),
  "{ :self :index :anObject | let k = wrapBetweenAnd(index,1, +(size(self), 1)); atPut(self, k, anObject) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atWrap",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _wrapBetweenAnd_3(_index, 1, _plusSign_2(_size_1(_self), 1));
    /* Statements */
    return _at_2(_self, _k);
  }, ["self", "index"]),
  "{ :self :index | let k = wrapBetweenAnd(index,1, +(size(self), 1)); at(self, k) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "atRandom",
  ["self", "shape", "rng"],
  sl.annotateFunction(function (_self, _shape, _rng) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _shape, _rng";
      throw new Error(errorMessage);
    } /* Statements */
    return _exclamationMark_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _index = _nextRandomInteger_3(_rng, 1, _size_1(_self));
        /* Statements */
        return _at_2(_self, _index);
      }, []),
      _shape,
    );
  }, ["self", "shape", "rng"]),
  "{ :self :shape :rng | !({ let index = nextRandomInteger(rng,1, size(self)); at(self, index) }, shape) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "before",
  ["self", "target"],
  sl.annotateFunction(function (_self, _target) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _target";
      throw new Error(errorMessage);
    } /* Statements */
    return _beforeIfAbsent_3(
      _self,
      _target,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _errorNotFound_2(_self, _target);
      }, []),
    );
  }, ["self", "target"]),
  "{ :self :target | beforeIfAbsent(self, target, { errorNotFound(self,target) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "beforeIfAbsent",
  ["self", "target", "exceptionBlock:/0"],
  sl.annotateFunction(function (_self, _target, _exceptionBlock_0) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _target, _exceptionBlock_0";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _indexOf_2(_self, _target);
    /* Statements */
    return _if_3(
      _lessThanSign_2(_index, 2),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _exceptionBlock_0();
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _at_2(_self, _hyphenMinus_2(_index, 1));
      }, []),
    );
  }, ["self", "target", "exceptionBlock:/0"]),
  "{ :self :target :exceptionBlock:/0 | let index = indexOf(self,target); if((<(index, 2)), { exceptionBlock() }, { at(self, -(index, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "beginsWith",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isSequenceable_1(_aList),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _ifTrue_2(
            _lessThanSign_2(_size_1(_self), _size_1(_aList)),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(false);
            }, []),
          );
          _indicesDo_2(
            _aList,
            sl.annotateFunction(function (_index) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _index";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifFalse_2(
                _equalsSign_2(_at_2(_aList, _index), _at_2(_self, _index)),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
            }, ["index"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>beginsWith: not a sequence");
      }, []),
    );
  }, ["self", "aList"]),
  "{ :self :aList | if(isSequenceable(aList), { valueWithReturn({ :return:/1 | ifTrue((<(size(self), size(aList))), { return(false) }); indicesDo(aList, { :index | ifFalse((=(at(aList, index), at(self, index))), { return(false) }) }); true }) }, { error(self,'@Sequenceable>>beginsWith: not a sequence') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "beginsWithAnyOf",
  ["self", "aCollection"],
  sl.annotateFunction(function (_self, _aCollection) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aCollection";
      throw new Error(errorMessage);
    } /* Statements */
    return _anySatisfy_2(
      _aCollection,
      sl.annotateFunction(function (_prefix) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _prefix";
          throw new Error(errorMessage);
        } /* Statements */
        return _beginsWith_2(_self, _prefix);
      }, ["prefix"]),
    );
  }, ["self", "aCollection"]),
  "{ :self :aCollection | anySatisfy(aCollection, { :prefix | beginsWith(self,prefix) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "binaryDetectIndex",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _binaryDetectIndex_2(
      _size_1(_self),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _i));
      }, ["i"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | binaryDetectIndex(size(self), { :i | aBlock(at(self, i)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "binaryDistance",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Statements */
    return _boole_1(_equalsSign_2(_u, _v));
  }, ["u", "v"]),
  "{ :u :v | boole((=(u, v))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "binarySearch",
  ["self", "item"],
  sl.annotateFunction(function (_self, _item) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _item";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _i = _binaryDetectIndex_2(
      _n,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _greaterThanSignEqualsSign_2(_at_2(_self, _each), _item);
      }, ["each"]),
    );
    /* Statements */
    return _if_3(
      _ampersand_2(
        _lessThanSignEqualsSign_2(_i, _n),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_at_2(_self, _i), _item);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _i;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 0;
      }, []),
    );
  }, ["self", "item"]),
  "{ :self :item | let n = size(self); let i = binaryDetectIndex(n, { :each | >=(at(self, each), item) }); if((&((<=(i, n)), { =(at(self, i), item) })), { i }, { 0 }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "binarySearchLeftmost",
  ["self", "item"],
  sl.annotateFunction(function (_self, _item) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _item";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _l = 0;
    let _r = _n;
    /* Statements */
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSign_2(_l, _r);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _m = _floor_1(_solidus_2(_plusSign_2(_l, _r), 2));
        /* Statements */
        return _if_3(
          _lessThanSign_2(_at_2(_self, _plusSign_2(_m, 1)), _item),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _l = _plusSign_2(_m, 1);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _r = _m;
          }, []),
        );
      }, []),
    );
    return _if_3(
      _ampersand_2(
        _lessThanSign_2(_l, _n),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_at_2(_self, _plusSign_2(_l, 1)), _item);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSign_2(_l, 1);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _l;
      }, []),
    );
  }, ["self", "item"]),
  "{ :self :item | let n = size(self); let l = 0; let r = n; whileTrue({ <(l, r) }, { let m = floor(/((+(l, r)), 2)); if((<(at(self, +(m, 1)), item)), { l := +(m, 1) }, { r := m }) }); if((&(<(l, n), { =(at(self, +(l, 1)), item) })), { +(l, 1) }, { l }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "binarySearchRightmost",
  ["self", "item"],
  sl.annotateFunction(function (_self, _item) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _item";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _l = 0;
    let _r = _n;
    /* Statements */
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSign_2(_l, _r);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _m = _floor_1(_solidus_2(_plusSign_2(_l, _r), 2));
        /* Statements */
        return _if_3(
          _greaterThanSign_2(_at_2(_self, _plusSign_2(_m, 1)), _item),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _r = _m;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _l = _plusSign_2(_m, 1);
          }, []),
        );
      }, []),
    );
    return _if_3(
      _equalsSign_2(_r, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 1;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _equalsSign_2(_r, _n),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _plusSign_2(_n, 1);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _equalsSign_2(_at_2(_self, _r), _item),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _r;
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _plusSign_2(_r, 1);
              }, []),
            );
          }, []),
        );
      }, []),
    );
  }, ["self", "item"]),
  "{ :self :item | let n = size(self); let l = 0; let r = n; whileTrue({ <(l, r) }, { let m = floor(/((+(l, r)), 2)); if((>(at(self, +(m, 1)), item)), { r := m }, { l := +(m, 1) }) }); if((=(r, 0)), { 1 }, { if((=(r, n)), { +(n, 1) }, { if((=(at(self, r), item)), { r }, { +(r, 1) }) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "bisect",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _i = 1;
    /* Statements */
    _whileFalse_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_anObject, _at_2(_self, _i));
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _i = _plusSign_2(_i, 1);
      }, []),
    );
    return _i;
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | let i = 1; whileFalse({ aBlock(anObject, at(self, i)) }, { i := +(i, 1) }); i }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "bisect",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _bisect_3(_self, _anObject, _lessThanSign_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | bisect(self,anObject, <) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "blomqvistBeta",
  ["v", "w"],
  sl.annotateFunction(function (_v, _w) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _v, _w";
      throw new Error(errorMessage);
    } /* Statements */
    return _correlation_2(
      _sign_1(_hyphenMinus_2(_v, _median_1(_v))),
      _sign_1(_hyphenMinus_2(_w, _median_1(_w))),
    );
  }, ["v", "w"]),
  "{ :v :w | correlation(sign((-(v, median(v)))),sign((-(w, median(w))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "brayCurtisDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _solidus_2(
      _sum_1(_abs_1(_hyphenMinus_2(_self, _aList))),
      _sum_1(_abs_1(_plusSign_2(_self, _aList))),
    );
  }, ["self", "aList"]),
  "{ :self :aList | /(sum(abs((-(self, aList)))), sum(abs((+(self, aList))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "canberraDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _sum_1(
      _solidus_2(
        _abs_1(_hyphenMinus_2(_self, _aList)),
        _plusSign_2(_abs_1(_self), _abs_1(_aList)),
      ),
    );
  }, ["self", "aList"]),
  "{ :self :aList | sum((/(abs((-(self, aList))), (+(abs(self), abs(aList)))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "cartesianIndex",
  ["shape", "anInteger"],
  sl.annotateFunction(function (_shape, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _shape, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _plusSign_2(
      _mixedRadixEncode_2(_hyphenMinus_2(_anInteger, 1), _shape),
      1,
    );
  }, ["shape", "anInteger"]),
  "{ :shape :anInteger | +(mixedRadixEncode((-(anInteger, 1)),shape), 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "centerArray",
  ["aList", "anInteger", "anObject"],
  sl.annotateFunction(function (_aList, _anInteger, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _aList, _anInteger, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_size_1(_aList), _anInteger),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_aList, "centerArray");
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _prefixSize = _max_2(
          _ceiling_1(
            _solidus_2(_hyphenMinus_2(_anInteger, _size_1(_aList)), 2),
          ),
          0,
        );
        let _suffixSize = _max_2(
          _hyphenMinus_2(
            _hyphenMinus_2(_anInteger, _size_1(_aList)),
            _prefixSize,
          ),
          0,
        );
        /* Statements */
        return _plusSignPlusSign_2(
          _plusSignPlusSign_2(_numberSign_2(_anObject, _prefixSize), _aList),
          _numberSign_2(_anObject, _suffixSize),
        );
      }, []),
    );
  }, ["aList", "anInteger", "anObject"]),
  "{ :aList :anInteger :anObject | if((>(size(aList), anInteger)), { error(aList,'centerArray') }, { let prefixSize = max(ceiling((/(-(anInteger, size(aList)), 2))),0); let suffixSize = max((-(-(anInteger, size(aList)), prefixSize)),0); ++(++((#(anObject, prefixSize)), aList), (#(anObject, suffixSize))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "chessboardDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _max_1(_abs_1(_hyphenMinus_2(_self, _aList)));
  }, ["self", "aList"]),
  "{ :self :aList | max(abs((-(self, aList)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "chineseRemainder",
  ["r", "m"],
  sl.annotateFunction(function (_r, _m) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _r, _m";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _p = _product_1(_m);
    let _q = _sum_1(
      _withCollect_3(
        _m,
        _r,
        sl.annotateFunction(function (_i, _j) {
          /* ArityCheck */
          if (arguments.length !== 2) {
            const errorMessage = "Arity: expected 2, _i, _j";
            throw new Error(errorMessage);
          } /* Statements */
          return _asterisk_2(
            _asterisk_2(_j, _modularInverse_2(_solidus_2(_p, _i), _i)),
            _solidus_2(_p, _i),
          );
        }, ["i", "j"]),
      ),
    );
    /* Statements */
    return _percentSign_2(_q, _p);
  }, ["r", "m"]),
  "{ :r :m | let p = product(m); let q = sum(withCollect(m, r, { :i :j | *(*(j, modularInverse((/(p, i)),i)), (/(p, i))) })); %(q, p) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "collect",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _ofSize_2(_species_1(_self), _size_1(_self));
    /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(_answer, _index, _aBlock_1(_at_2(_self, _index)));
      }, ["index"]),
    );
    return _answer;
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let answer = ofSize(species(self),size(self)); indicesDo(self, { :index | atPut(answer, index, aBlock(at(self, index))) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "combinationsAtATimeDo",
  ["self", "kk", "aBlock:/1"],
  sl.annotateFunction(function (_self, _kk, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _kk, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _aCollection = _List_1(_kk);
    /* Statements */
    return _combinationsAtInAfterDo_5(_self, 1, _aCollection, 0, _aBlock_1);
  }, ["self", "kk", "aBlock:/1"]),
  "{ :self :kk :aBlock:/1 | let aCollection = List(kk); combinationsAtInAfterDo(self,1, aCollection, 0, aBlock:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "combinationsAtInAfterDo",
  ["self", "j", "aCollection", "n", "aBlock:/1"],
  sl.annotateFunction(function (_self, _j, _aCollection, _n, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 5) {
      const errorMessage =
        "Arity: expected 5, _self, _j, _aCollection, _n, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      _plusSign_2(_n, 1),
      _size_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        _atPut_3(_aCollection, _j, _at_2(_self, _index));
        return _if_3(
          _equalsSign_2(_j, _size_1(_aCollection)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_1(_aCollection);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _combinationsAtInAfterDo_5(
              _self,
              _plusSign_2(_j, 1),
              _aCollection,
              _index,
              _aBlock_1,
            );
          }, []),
        );
      }, ["index"]),
    );
  }, ["self", "j", "aCollection", "n", "aBlock:/1"]),
  "{ :self :j :aCollection :n :aBlock:/1 | toDo((+(n, 1)), size(self), { :index | atPut(aCollection, j, at(self, index)); if((=(j, size(aCollection))), { aBlock(aCollection) }, { combinationsAtInAfterDo(self,+(j, 1), aCollection, index, aBlock:/1) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "combinations",
  ["self", "m"],
  sl.annotateFunction(function (_self, _m) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _m";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _combinationsAtATimeDo_3(
      _self,
      _m,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _copy_1(_each));
      }, ["each"]),
    );
    return _answer;
  }, ["self", "m"]),
  "{ :self :m | let answer = []; combinationsAtATimeDo(self, m, { :each | add(answer,copy(each)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "compare",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _m = _size_1(_aList);
    /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _toDo_3(
        1,
        _min_2(_n, _m),
        sl.annotateFunction(function (_i) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _i";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _c = _lessThanSignEqualsSignGreaterThanSign_2(
            _at_2(_self, _i),
            _at_2(_aList, _i),
          );
          /* Statements */
          return _ifTrue_2(
            _tildeEqualsSign_2(_c, 0),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_c);
            }, []),
          );
        }, ["i"]),
      );
      return _lessThanSignEqualsSignGreaterThanSign_2(_n, _m);
    }, ["return:/1"]));
  }, ["self", "aList"]),
  "{ :self :aList | let n = size(self); let m = size(aList); valueWithReturn({ :return:/1 | toDo(1, min(n,m), { :i | let c = <=>(at(self, i), at(aList, i)); ifTrue((~=(c, 0)), { return(c) }) }); <=>(n, m) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "catenateSeparatedBy",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answerSize = _plusSign_2(
          _injectInto_3(
            _self,
            0,
            sl.annotateFunction(function (_sum, _each) {
              /* ArityCheck */
              if (arguments.length !== 2) {
                const errorMessage = "Arity: expected 2, _sum, _each";
                throw new Error(errorMessage);
              } /* Statements */
              return _plusSign_2(_sum, _size_1(_each));
            }, ["sum", "each"]),
          ),
          _asterisk_2(_hyphenMinus_2(_size_1(_self), 1), _size_1(_aList)),
        );
        let _answer = _ofSize_2(_species_1(_self), _answerSize);
        let _index = 1;
        let _put_1 = sl.annotateFunction(function (_items) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _items";
            throw new Error(errorMessage);
          } /* Statements */
          return _do_2(
            _items,
            sl.annotateFunction(function (_item) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _item";
                throw new Error(errorMessage);
              } /* Statements */
              _atPut_3(_answer, _index, _item);
              return _index = _plusSign_2(_index, 1);
            }, ["item"]),
          );
        }, ["items"]);
        /* Statements */
        _allButLastDo_2(
          _self,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            _put_1(_each);
            return _put_1(_aList);
          }, ["each"]),
        );
        _put_1(_last_1(_self));
        return _answer;
      }, []),
    );
  }, ["self", "aList"]),
  "{ :self :aList | ifEmpty(self, { copy(self) }, { let answerSize = +(injectInto(self, 0, { :sum :each | +(sum, size(each)) }), (*(-(size(self), 1), size(aList)))); let answer = ofSize(species(self),answerSize); let index = 1; let put = { :items | do(items, { :item | atPut(answer, index, item); index := +(index, 1) }) }; allButLastDo(self, { :each | put(each); put(aList) }); put(last(self)); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "catenate",
  ["self", "isChecked"],
  sl.annotateFunction(function (_self, _isChecked) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _isChecked";
      throw new Error(errorMessage);
    } /* Statements */
    _ifTrue_2(
      _isChecked,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifFalse_2(
          _equalsSign_2(_elementType_1(_self), _typeOf_1(_self)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _error_2(
              _self,
              "@Sequenceable>>catenate: invalid element type",
            );
          }, []),
        );
      }, []),
    );
    return _catenateSeparatedBy_2(_self, []);
  }, ["self", "isChecked"]),
  "{ :self :isChecked | ifTrue(isChecked, { ifFalse((=(elementType(self), typeOf(self))), { error(self,'@Sequenceable>>catenate: invalid element type') }) }); catenateSeparatedBy(self,[]) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "catenate",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _catenate_2(_self, false);
  }, ["self"]),
  "{ :self | catenate(self,false) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "constantArray",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _reshape_2([_anObject], _self);
  }, ["self", "anObject"]),
  "{ :self :anObject | reshape([anObject],self) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "convergents",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(_prefixes_1(_self), _fromContinuedFraction_1);
  }, ["self"]),
  "{ :self | collect(prefixes(self),fromContinuedFraction:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyFromTo",
  ["self", "start", "stop"],
  sl.annotateFunction(function (_self, _start, _stop) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _start, _stop";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      1,
      _plusSign_2(_hyphenMinus_2(_stop, _start), 1),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _at_2(_self, _hyphenMinus_2(_plusSign_2(_index, _start), 1));
      }, ["index"]),
    );
  }, ["self", "start", "stop"]),
  "{ :self :start :stop | toAsCollect(1, +(-(stop, start), 1), species(self), { :index | at(self, -(+(index, start), 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyFromToInto",
  ["self", "start", "stop", "aList"],
  sl.annotateFunction(function (_self, _start, _stop, _aList) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _self, _start, _stop, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      1,
      _plusSign_2(_hyphenMinus_2(_stop, _start), 1),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _aList,
          _index,
          _at_2(_self, _hyphenMinus_2(_plusSign_2(_index, _start), 1)),
        );
      }, ["index"]),
    );
  }, ["self", "start", "stop", "aList"]),
  "{ :self :start :stop :aList | toDo(1, +(-(stop, start), 1), { :index | atPut(aList, index, at(self, -(+(index, start), 1))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyFromToPin",
  ["self", "start", "stop"],
  sl.annotateFunction(function (_self, _start, _stop) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _start, _stop";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      1,
      _plusSign_2(_hyphenMinus_2(_stop, _start), 1),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPin_2(_self, _hyphenMinus_2(_plusSign_2(_index, _start), 1));
      }, ["index"]),
    );
  }, ["self", "start", "stop"]),
  "{ :self :start :stop | toAsCollect(1, +(-(stop, start), 1), species(self), { :index | atPin(self,-(+(index, start), 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyFromToWrap",
  ["self", "start", "stop"],
  sl.annotateFunction(function (_self, _start, _stop) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _start, _stop";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      1,
      _plusSign_2(_hyphenMinus_2(_stop, _start), 1),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atWrap_2(_self, _hyphenMinus_2(_plusSign_2(_index, _start), 1));
      }, ["index"]),
    );
  }, ["self", "start", "stop"]),
  "{ :self :start :stop | toAsCollect(1, +(-(stop, start), 1), species(self), { :index | atWrap(self,-(+(index, start), 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyReplaceAllWith",
  ["self", "old", "new"],
  sl.annotateFunction(function (_self, _old, _new) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _old, _new";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _indexList = _indicesOfSubstring_2(_self, _old);
    /* Statements */
    return _if_3(
      _isEmpty_1(_indexList),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _n = _size_1(_new);
        /* Statements */
        return _if_3(
          _equalsSign_2(_size_1(_old), _n),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _answer = _copy_1(_self);
            /* Statements */
            _do_2(
              _indexList,
              sl.annotateFunction(function (_i) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _i";
                  throw new Error(errorMessage);
                } /* Statements */
                return _replaceFromToWith_4(
                  _answer,
                  _i,
                  _hyphenMinus_2(_plusSign_2(_i, _n), 1),
                  _new,
                );
              }, ["i"]),
            );
            return _answer;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _answer = [];
            let _i = 1;
            let _k = _size_1(_old);
            /* Statements */
            _do_2(
              _indexList,
              sl.annotateFunction(function (_j) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _j";
                  throw new Error(errorMessage);
                } /* Statements */
                _add_2(
                  _answer,
                  _copyFromTo_3(_self, _i, _hyphenMinus_2(_j, 1)),
                );
                _add_2(_answer, _new);
                return _i = _plusSign_2(_j, _k);
              }, ["j"]),
            );
            _add_2(_answer, _copyFromTo_3(_self, _i, _size_1(_self)));
            return _catenate_1(_answer);
          }, []),
        );
      }, []),
    );
  }, ["self", "old", "new"]),
  "{ :self :old :new | let indexList = indicesOfSubstring(self,old); if(isEmpty(indexList), { copy(self) }, { let n = size(new); if((=(size(old), n)), { let answer = copy(self); do(indexList, { :i | replaceFromToWith(answer,i, -(+(i, n), 1), new) }); answer }, { let answer = []; let i = 1; let k = size(old); do(indexList, { :j | add(answer,copyFromTo(self,i, -(j, 1))); add(answer,new); i := +(j, k) }); add(answer,copyFromTo(self,i, size(self))); catenate(answer) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyReplaceFromToWith",
  ["self", "start", "stop", "aCollection"],
  sl.annotateFunction(function (_self, _start, _stop, _aCollection) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _start, _stop, _aCollection";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _end = _plusSign_2(_hyphenMinus_2(_start, 1), _size_1(_aCollection));
    let _newSize = _hyphenMinus_2(_plusSign_2(_size_1(_self), _end), _stop);
    let _answer = _ofSize_2(_species_1(_self), _newSize);
    /* Statements */
    _ifTrue_2(
      _greaterThanSign_2(_start, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _replaceFromToWithStartingAt_5(
          _answer,
          1,
          _hyphenMinus_2(_start, 1),
          _self,
          1,
        );
      }, []),
    );
    _ifTrue_2(
      _lessThanSignEqualsSign_2(_start, _end),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _replaceFromToWithStartingAt_5(
          _answer,
          _start,
          _end,
          _aCollection,
          1,
        );
      }, []),
    );
    _ifTrue_2(
      _lessThanSign_2(_end, _newSize),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _replaceFromToWithStartingAt_5(
          _answer,
          _plusSign_2(_end, 1),
          _newSize,
          _self,
          _plusSign_2(_stop, 1),
        );
      }, []),
    );
    return _answer;
  }, ["self", "start", "stop", "aCollection"]),
  "{ :self :start :stop :aCollection | let end = +(-(start, 1), size(aCollection)); let newSize = -(+(size(self), end), stop); let answer = ofSize(species(self),newSize); ifTrue((>(start, 1)), { replaceFromToWithStartingAt(answer,1, -(start, 1), self, 1) }); ifTrue((<=(start, end)), { replaceFromToWithStartingAt(answer,start, end, aCollection, 1) }); ifTrue((<(end, newSize)), { replaceFromToWithStartingAt(answer,+(end, 1), newSize, self, +(stop, 1)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyReplaceFromToWithObject",
  ["self", "start", "stop", "anObject"],
  sl.annotateFunction(function (_self, _start, _stop, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _self, _start, _stop, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyReplaceFromToWith_4(
      _self,
      _start,
      _stop,
      _numberSign_2(
        _anObject,
        _max_2(_plusSign_2(_hyphenMinus_2(_stop, _start), 1), 1),
      ),
    );
  }, ["self", "start", "stop", "anObject"]),
  "{ :self :start :stop :anObject | copyReplaceFromToWith(self,start, stop, #(anObject, max((+(-(stop, start), 1)),1))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyUpThrough",
  ["self", "anElement"],
  sl.annotateFunction(function (_self, _anElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anElement";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _indexOf_2(_self, _anElement);
    /* Statements */
    return _if_3(
      _equalsSign_2(_index, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _first_2(_self, _index);
      }, []),
    );
  }, ["self", "anElement"]),
  "{ :self :anElement | let index = indexOf(self,anElement); if((=(index, 0)), { copy(self) }, { first(self,index) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyUpTo",
  ["self", "anElement"],
  sl.annotateFunction(function (_self, _anElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anElement";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _indexOf_2(_self, _anElement);
    /* Statements */
    return _if_3(
      _equalsSign_2(_index, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _first_2(_self, _hyphenMinus_2(_index, 1));
      }, []),
    );
  }, ["self", "anElement"]),
  "{ :self :anElement | let index = indexOf(self,anElement); if((=(index, 0)), { copy(self) }, { first(self,-(index, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyUpToLast",
  ["self", "anElement"],
  sl.annotateFunction(function (_self, _anElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anElement";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _lastIndexOf_2(_self, _anElement);
    /* Statements */
    return _if_3(
      _equalsSign_2(_index, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _first_2(_self, _hyphenMinus_2(_index, 1));
      }, []),
    );
  }, ["self", "anElement"]),
  "{ :self :anElement | let index = lastIndexOf(self,anElement); if((=(index, 0)), { copy(self) }, { first(self,-(index, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "copyWithFirst",
  ["self", "newElement"],
  sl.annotateFunction(function (_self, _newElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _newElement";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _copy_1(_self);
    /* Statements */
    _addFirst_2(_answer, _newElement);
    return _answer;
  }, ["self", "newElement"]),
  "{ :self :newElement | let answer = copy(self); addFirst(answer,newElement); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "correlation",
  ["v", "w"],
  sl.annotateFunction(function (_v, _w) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _v, _w";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isMatrix_1(_v),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _matrixCorrelation_2(_v, _w);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _solidus_2(
          _covariance_2(_v, _w),
          _asterisk_2(_standardDeviation_1(_v), _standardDeviation_1(_w)),
        );
      }, []),
    );
  }, ["v", "w"]),
  "{ :v :w | if(isMatrix(v), { matrixCorrelation(v,w) }, { /(covariance(v,w), (*(standardDeviation(v), standardDeviation(w)))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "correlationDistance",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _uu = _hyphenMinus_2(_u, _mean_1(_u));
    let _vv = _hyphenMinus_2(_v, _mean_1(_v));
    /* Statements */
    return _hyphenMinus_2(
      1,
      _solidus_2(
        _dot_2(
          _hyphenMinus_2(_u, _mean_1(_u)),
          _hyphenMinus_2(_v, _mean_1(_v)),
        ),
        _asterisk_2(_norm_1(_uu), _norm_1(_vv)),
      ),
    );
  }, ["u", "v"]),
  "{ :u :v | let uu = (-(u, mean(u))); let vv = (-(v, mean(v))); -(1, (/(dot((-(u, mean(u))),-(v, mean(v))), (*(norm(uu), norm(vv)))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "cosineDistance",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Statements */
    return _hyphenMinus_2(
      1,
      _solidus_2(_dot_2(_u, _v), _asterisk_2(_norm_1(_u), _norm_1(_v))),
    );
  }, ["u", "v"]),
  "{ :u :v | -(1, (/(dot(u,v), (*(norm(u), norm(v)))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "covariance",
  ["v", "w"],
  sl.annotateFunction(function (_v, _w) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _v, _w";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isMatrix_1(_v),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _matrixCovariance_2(_v, _w);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _n = _size_1(_v);
        let _m = _size_1(_w);
        /* Statements */
        return _if_3(
          _equalsSign_2(_n, _m),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _asterisk_2(
              _solidus_2(1, _hyphenMinus_2(_n, 1)),
              _dot_2(
                _hyphenMinus_2(_v, _mean_1(_v)),
                _conjugated_1(_hyphenMinus_2(_w, _mean_1(_w))),
              ),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _error_2(
              [_v, _w],
              "@Sequenceable>>covariance: vectors must be equal",
            );
          }, []),
        );
      }, []),
    );
  }, ["v", "w"]),
  "{ :v :w | if(isMatrix(v), { matrixCovariance(v,w) }, { let n = size(v); let m = size(w); if((=(n, m)), { *((/(1, (-(n, 1)))), dot((-(v, mean(v))),conjugated((-(w, mean(w)))))) }, { error([v, w],'@Sequenceable>>covariance: vectors must be equal') }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "cross",
  ["u"],
  sl.annotateFunction(function (_u) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _u";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL1 = _assertIsOfSize_2(_u, 2);
    let _x = _at_2(__SPL1, 1);
    let _y = _at_2(__SPL1, 2);
    /* Statements */
    return [_hyphenMinus_1(_y), _x];
  }, ["u"]),
  "{ :u | let __SPL1 = assertIsOfSize(u, 2); let x = at(__SPL1, 1); let y = at(__SPL1, 2); [-(y), x] }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "cross",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL2 = _assertIsOfSize_2(_u, 3);
    let _ux = _at_2(__SPL2, 1);
    let _uy = _at_2(__SPL2, 2);
    let _uz = _at_2(__SPL2, 3);
    let __SPL3 = _assertIsOfSize_2(_v, 3);
    let _vx = _at_2(__SPL3, 1);
    let _vy = _at_2(__SPL3, 2);
    let _vz = _at_2(__SPL3, 3);
    /* Statements */
    return [
      _hyphenMinus_2(_asterisk_2(_uy, _vz), _asterisk_2(_uz, _vy)),
      _hyphenMinus_2(_asterisk_2(_uz, _vx), _asterisk_2(_ux, _vz)),
      _hyphenMinus_2(_asterisk_2(_ux, _vy), _asterisk_2(_uy, _vx)),
    ];
  }, ["u", "v"]),
  "{ :u :v | let __SPL2 = assertIsOfSize(u, 3); let ux = at(__SPL2, 1); let uy = at(__SPL2, 2); let uz = at(__SPL2, 3); let __SPL3 = assertIsOfSize(v, 3); let vx = at(__SPL3, 1); let vy = at(__SPL3, 2); let vz = at(__SPL3, 3); [-((*(uy, vz)), (*(uz, vy))), -((*(uz, vx)), (*(ux, vz))), -((*(ux, vy)), (*(uy, vx)))] }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "crossedMultiply",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _withCollectCrossed_3(_self, _aList, _asterisk_2);
  }, ["self", "aList"]),
  "{ :self :aList | withCollectCrossed(self,aList, *) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "deBruijnSequence",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _catenate_1(
      _select_2(
        _lyndonWords_2(_self, _anInteger),
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _k = _size_1(_each);
          /* Statements */
          return _verticalLine_2(
            _equalsSign_2(_k, 1),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _divisible_2(_k, _anInteger);
            }, []),
          );
        }, ["each"]),
      ),
    );
  }, ["self", "anInteger"]),
  "{ :self :anInteger | catenate(select(lyndonWords(self,anInteger), { :each | let k = size(each); |(=(k, 1), { divisible(k,anInteger) }) })) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "deleteAdjacentDuplicates",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = [_first_1(_self)];
        /* Statements */
        _adjacentPairsDo_2(
          _self,
          sl.annotateFunction(function (_i, _j) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _i, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _ifFalse_2(
              _aBlock_2(_i, _j),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _add_2(_answer, _j);
              }, []),
            );
          }, ["i", "j"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | if(isEmpty(self), { [] }, { let answer = [first(self)]; adjacentPairsDo(self, { :i :j | ifFalse(aBlock(i, j), { add(answer,j) }) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "deleteAdjacentDuplicates",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _deleteAdjacentDuplicates_2(_self, _equalsSign_2);
  }, ["self"]),
  "{ :self | deleteAdjacentDuplicates(self,=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "detectIndex",
  ["self", "predicate:/1"],
  sl.annotateFunction(function (_self, _predicate_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _predicate_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _detectIndexIfFoundIfNone_4(
      _self,
      _predicate_1,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _each;
      }, ["each"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return null;
      }, []),
    );
  }, ["self", "predicate:/1"]),
  "{ :self :predicate:/1 | detectIndexIfFoundIfNone(self, predicate:/1, { :each | each }, { nil }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "detectIndexIfFound",
  ["self", "predicate:/1", "ifFound:/1"],
  sl.annotateFunction(function (_self, _predicate_1, _ifFound_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _predicate_1, _ifFound_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _detectIndexIfFoundIfNone_4(
      _self,
      _predicate_1,
      _ifFound_1,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        }
      }, []),
    );
  }, ["self", "predicate:/1", "ifFound:/1"]),
  "{ :self :predicate:/1 :ifFound:/1 | detectIndexIfFoundIfNone(self,predicate:/1, ifFound:/1, {  }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "detectIndexIfFoundIfNone",
  ["self", "predicate:/1", "ifFound:/1", "ifNone:/0"],
  sl.annotateFunction(function (_self, _predicate_1, _ifFound_1, _ifNone_0) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _predicate_1, _ifFound_1, _ifNone_0";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _toDo_3(
        1,
        _size_1(_self),
        sl.annotateFunction(function (_index) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _index";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifTrue_2(
            _predicate_1(_at_2(_self, _index)),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_ifFound_1(_index));
            }, []),
          );
        }, ["index"]),
      );
      return _ifNone_0();
    }, ["return:/1"]));
  }, ["self", "predicate:/1", "ifFound:/1", "ifNone:/0"]),
  "{ :self :predicate:/1 :ifFound:/1 :ifNone:/0 | valueWithReturn({ :return:/1 | toDo(1, size(self), { :index | ifTrue(predicate(at(self, index)), { return(ifFound(index)) }) }); ifNone() }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "detectStartingAt",
  ["self", "predicate:/1", "startIndex"],
  sl.annotateFunction(function (_self, _predicate_1, _startIndex) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _predicate_1, _startIndex";
      throw new Error(errorMessage);
    } /* Statements */
    return _detectStartingAtIfFoundIfNone_5(
      _self,
      _predicate_1,
      _startIndex,
      sl.annotateFunction(function (_item) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _item";
          throw new Error(errorMessage);
        } /* Statements */
        return _item;
      }, ["item"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>detectStartingAt: no such item");
      }, []),
    );
  }, ["self", "predicate:/1", "startIndex"]),
  "{ :self :predicate:/1 :startIndex | detectStartingAtIfFoundIfNone(self, predicate:/1, startIndex, { :item | item }, { error(self,'@Sequenceable>>detectStartingAt: no such item') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "detectStartingAtIfFoundIfNone",
  ["self", "predicate:/1", "startIndex", "ifFound:/1", "ifNone:/0"],
  sl.annotateFunction(
    function (_self, _predicate_1, _startIndex, _ifFound_1, _ifNone_0) {
      /* ArityCheck */
      if (arguments.length !== 5) {
        const errorMessage =
          "Arity: expected 5, _self, _predicate_1, _startIndex, _ifFound_1, _ifNone_0";
        throw new Error(errorMessage);
      } /* Statements */
      return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _return_1";
          throw new Error(errorMessage);
        } /* Statements */
        _toDo_3(
          _startIndex,
          _size_1(_self),
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _item = _at_2(_self, _index);
            /* Statements */
            return _ifTrue_2(
              _predicate_1(_item),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _return_1(_ifFound_1(_item));
              }, []),
            );
          }, ["index"]),
        );
        return _ifNone_0();
      }, ["return:/1"]));
    },
    ["self", "predicate:/1", "startIndex", "ifFound:/1", "ifNone:/0"],
  ),
  "{ :self :predicate:/1 :startIndex :ifFound:/1 :ifNone:/0 | valueWithReturn({ :return:/1 | toDo(startIndex, size(self), { :index | let item = at(self, index); ifTrue(predicate(item), { return(ifFound(item)) }) }); ifNone() }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "differences",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _partition_3(_self, 2, 1),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _hyphenMinus_2(_at_2(_each, 2), _at_2(_each, 1));
      }, ["each"]),
    );
  }, ["self"]),
  "{ :self | collect(partition(self,2, 1), { :each | -(at(each, 2), at(each, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "do",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
    return _self;
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | indicesDo(self, { :index | aBlock(at(self, index)) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "doSeparatedBy",
  ["self", "elementBlock:/1", "separatorBlock:/0"],
  sl.annotateFunction(function (_self, _elementBlock_1, _separatorBlock_0) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _elementBlock_1, _separatorBlock_0";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        _ifFalse_2(
          _equalsSign_2(_index, 1),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _separatorBlock_0();
          }, []),
        );
        return _elementBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "elementBlock:/1", "separatorBlock:/0"]),
  "{ :self :elementBlock:/1 :separatorBlock:/0 | indicesDo(self, { :index | ifFalse((=(index, 1)), { separatorBlock() }); elementBlock(at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "doWithout",
  ["self", "aBlock:/1", "anItem"],
  sl.annotateFunction(function (_self, _aBlock_1, _anItem) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aBlock_1, _anItem";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifFalse_2(
          _equalsSign_2(_anItem, _at_2(_self, _index)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_1(_at_2(_self, _index));
          }, []),
        );
      }, ["index"]),
    );
  }, ["self", "aBlock:/1", "anItem"]),
  "{ :self :aBlock:/1 :anItem | indicesDo(self, { :index | ifFalse((=(anItem, at(self, index))), { aBlock(at(self, index)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "drop",
  ["self", "count"],
  sl.annotateFunction(function (_self, _count) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _count";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSignEqualsSign_2(_abs_1(_count), _size_1(_self)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _new_1(_species_1(_self));
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _lessThanSign_2(_count, 0),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _dropLast_2(_self, _negated_1(_count));
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _dropFirst_2(_self, _count);
          }, []),
        );
      }, []),
    );
  }, ["self", "count"]),
  "{ :self :count | if((>=(abs(count), size(self))), { new(species(self)) }, { if((<(count, 0)), { dropLast(self,negated(count)) }, { dropFirst(self,count) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "dropFirst",
  ["self", "count"],
  sl.annotateFunction(function (_self, _count) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _count";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, _plusSign_2(_count, 1), _size_1(_self));
  }, ["self", "count"]),
  "{ :self :count | copyFromTo(self,+(count, 1), size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "dropLast",
  ["self", "count"],
  sl.annotateFunction(function (_self, _count) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _count";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, 1, _hyphenMinus_2(_size_1(_self), _count));
  }, ["self", "count"]),
  "{ :self :count | copyFromTo(self,1, -(size(self), count)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "dropWhile",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _detectIndexIfFoundIfNone_4(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _not_1(_aBlock_1(_each));
      }, ["each"]),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, _i, _size_1(_self));
      }, ["i"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _new_1(_species_1(_self));
      }, []),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | detectIndexIfFoundIfNone(self, { :each | not(aBlock(each)) }, { :i | copyFromTo(self,i, size(self)) }, { new(species(self)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "duplicateEach",
  ["self", "counts"],
  sl.annotateFunction(function (_self, _counts) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _counts";
      throw new Error(errorMessage);
    } /* Statements */
    _ifTrue_2(
      _isInteger_1(_counts),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _counts = _numberSign_2(_counts, _size_1(_self));
      }, []),
    );
    return _replicateEachApplying_3(_self, _counts, _value_1);
  }, ["self", "counts"]),
  "{ :self :counts | ifTrue(isInteger(counts), { counts := #(counts, size(self)) }); replicateEachApplying(self,counts, value:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "editDistance",
  ["self", "other"],
  sl.annotateFunction(function (_self, _other) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _other";
      throw new Error(errorMessage);
    } /* Statements */
    return _levenshteinDistance_2(_self, _other);
  }, ["self", "other"]),
  "{ :self :other | levenshteinDistance(self,other) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "endsWith",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isSequenceable_1(_aList),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _sequenceSize = _size_1(_aList);
        let _offset = _hyphenMinus_2(_size_1(_self), _sequenceSize);
        /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _ifTrue_2(
            _lessThanSign_2(_offset, 0),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(false);
            }, []),
          );
          _toDo_3(
            1,
            _sequenceSize,
            sl.annotateFunction(function (_index) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _index";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifFalse_2(
                _equalsSign_2(
                  _at_2(_aList, _index),
                  _at_2(_self, _plusSign_2(_index, _offset)),
                ),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
            }, ["index"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>endsWith: not a sequence");
      }, []),
    );
  }, ["self", "aList"]),
  "{ :self :aList | if(isSequenceable(aList), { let sequenceSize = size(aList); let offset = -(size(self), sequenceSize); valueWithReturn({ :return:/1 | ifTrue((<(offset, 0)), { return(false) }); toDo(1, sequenceSize, { :index | ifFalse((=(at(aList, index), at(self, +(index, offset)))), { return(false) }) }); true }) }, { error(self,'@Sequenceable>>endsWith: not a sequence') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "endsWithAnyOf",
  ["self", "aCollection"],
  sl.annotateFunction(function (_self, _aCollection) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aCollection";
      throw new Error(errorMessage);
    } /* Statements */
    return _anySatisfy_2(
      _aCollection,
      sl.annotateFunction(function (_suffix) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _suffix";
          throw new Error(errorMessage);
        } /* Statements */
        return _endsWith_2(_self, _suffix);
      }, ["suffix"]),
    );
  }, ["self", "aCollection"]),
  "{ :self :aCollection | anySatisfy(aCollection, { :suffix | endsWith(self,suffix) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "equalBy",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _verticalLine_2(
      _equalsSignEqualsSign_2(_self, _anObject),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersand_2(
          _equalsSign_2(_typeOf_1(_self), _typeOf_1(_anObject)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _hasEqualElementsBy_3(_self, _anObject, _aBlock_2);
          }, []),
        );
      }, []),
    );
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | |(==(self, anObject), { &(=(typeOf(self), typeOf(anObject)), { hasEqualElementsBy(self,anObject, aBlock:/2) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "euclideanDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _norm_1(_hyphenMinus_2(_self, _aList));
  }, ["self", "aList"]),
  "{ :self :aList | norm((-(self, aList))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "eulerMatrix",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let __SPL4 = _assertIsOfSize_2(_cos_1(_self), 3);
    let _ca = _at_2(__SPL4, 1);
    let _cb = _at_2(__SPL4, 2);
    let _cc = _at_2(__SPL4, 3);
    let __SPL5 = _assertIsOfSize_2(_sin_1(_self), 3);
    let _sa = _at_2(__SPL5, 1);
    let _sb = _at_2(__SPL5, 2);
    let _sc = _at_2(__SPL5, 3);
    /* Statements */
    return [[
      _hyphenMinus_2(
        _asterisk_2(_asterisk_2(_ca, _cb), _cc),
        _asterisk_2(_sa, _sc),
      ),
      _hyphenMinus_2(
        _hyphenMinus_2(0, _asterisk_2(_cc, _sa)),
        _asterisk_2(_asterisk_2(_ca, _cb), _sc),
      ),
      _asterisk_2(_ca, _sb),
    ], [
      _plusSign_2(
        _asterisk_2(_asterisk_2(_cb, _cc), _sa),
        _asterisk_2(_ca, _sc),
      ),
      _hyphenMinus_2(
        _asterisk_2(_ca, _cc),
        _asterisk_2(_asterisk_2(_cb, _sa), _sc),
      ),
      _asterisk_2(_sa, _sb),
    ], [_hyphenMinus_2(0, _asterisk_2(_cc, _sb)), _asterisk_2(_sb, _sc), _cb]];
  }, ["self"]),
  "{ :self | let __SPL4 = assertIsOfSize(cos(self), 3); let ca = at(__SPL4, 1); let cb = at(__SPL4, 2); let cc = at(__SPL4, 3); let __SPL5 = assertIsOfSize(sin(self), 3); let sa = at(__SPL5, 1); let sb = at(__SPL5, 2); let sc = at(__SPL5, 3); [[-((*(*(ca, cb), cc)), (*(sa, sc))), -(-(0, (*(cc, sa))), (*(*(ca, cb), sc))), *(ca, sb)], [+((*(*(cb, cc), sa)), (*(ca, sc))), -((*(ca, cc)), (*(*(cb, sa), sc))), *(sa, sb)], [-(0, (*(cc, sb))), *(sb, sc), cb]] }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "exponentialMovingAverage",
  ["self", "alpha"],
  sl.annotateFunction(function (_self, _alpha) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _alpha";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _List_1(_size_1(_self));
    /* Statements */
    _atPut_3(_answer, 1, _at_2(_self, 1));
    _toDo_3(
      2,
      _size_1(_self),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _y = _at_2(_answer, _hyphenMinus_2(_i, 1));
        /* Statements */
        return _atPut_3(
          _answer,
          _i,
          _plusSign_2(
            _y,
            _asterisk_2(_alpha, _hyphenMinus_2(_at_2(_self, _i), _y)),
          ),
        );
      }, ["i"]),
    );
    return _answer;
  }, ["self", "alpha"]),
  "{ :self :alpha | let answer = List(size(self)); atPut(answer, 1, at(self, 1)); toDo(2, size(self), { :i | let y = at(answer, -(i, 1)); atPut(answer, i, +(y, (*(alpha, (-(at(self, i), y)))))) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fastWalshHadamardTransform",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _h = 1;
    let _k = _size_1(_self);
    /* Statements */
    _ifFalse_2(
      _isPowerOfTwo_1(_k),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _addAll_2(
          _self,
          _List_2(_hyphenMinus_2(_asLargerPowerOfTwo_1(_k), _k), 0),
        );
        return _k = _size_1(_self);
      }, []),
    );
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSign_2(_h, _k);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _toByDo_4(
          1,
          _k,
          _asterisk_2(_h, 2),
          sl.annotateFunction(function (_i) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _i";
              throw new Error(errorMessage);
            } /* Statements */
            return _toDo_3(
              _i,
              _hyphenMinus_2(_plusSign_2(_i, _h), 1),
              sl.annotateFunction(function (_j) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _j";
                  throw new Error(errorMessage);
                } /* Temporaries */
                let _x = _at_2(_self, _j);
                let _y = _at_2(_self, _plusSign_2(_j, _h));
                /* Statements */
                _atPut_3(_self, _j, _plusSign_2(_x, _y));
                return _atPut_3(
                  _self,
                  _plusSign_2(_j, _h),
                  _hyphenMinus_2(_x, _y),
                );
              }, ["j"]),
            );
          }, ["i"]),
        );
        return _h = _asterisk_2(_h, 2);
      }, []),
    );
    return _self;
  }, ["self"]),
  "{ :self | let h = 1; let k = size(self); ifFalse(isPowerOfTwo(k), { addAll(self,List(-(asLargerPowerOfTwo(k), k), 0)); k := size(self) }); whileTrue({ <(h, k) }, { toByDo(1, k, *(h, 2), { :i | toDo(i, -(+(i, h), 1), { :j | let x = at(self, j); let y = at(self, +(j, h)); atPut(self, j, +(x, y)); atPut(self, +(j, h), -(x, y)) }) }); h := *(h, 2) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fillFromWith",
  ["self", "aCollection", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aCollection, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aCollection, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = 1;
    /* Statements */
    _do_2(
      _aCollection,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        _atPut_3(_self, _index, _aBlock_1(_each));
        return _index = _plusSign_2(_index, 1);
      }, ["each"]),
    );
    return _self;
  }, ["self", "aCollection", "aBlock:/1"]),
  "{ :self :aCollection :aBlock:/1 | let index = 1; do(aCollection, { :each | atPut(self, index, aBlock(each)); index := +(index, 1) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinary",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _findBinaryDoIfNone_4(
      _self,
      _aBlock_1,
      sl.annotateFunction(function (_found) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _found";
          throw new Error(errorMessage);
        } /* Statements */
        return _found;
      }, ["found"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>findBinary: not found");
      }, []),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | findBinaryDoIfNone(self, aBlock:/1, { :found | found }, { error(self,'@Sequenceable>>findBinary: not found') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinaryDoIfNone",
  ["self", "aBlock:/1", "actionBlock:/1", "exceptionBlock"],
  sl.annotateFunction(
    function (_self, _aBlock_1, _actionBlock_1, _exceptionBlock) {
      /* ArityCheck */
      if (arguments.length !== 4) {
        const errorMessage =
          "Arity: expected 4, _self, _aBlock_1, _actionBlock_1, _exceptionBlock";
        throw new Error(errorMessage);
      } /* Statements */
      return _findBinaryIndexDoIfNone_4(
        _self,
        _aBlock_1,
        sl.annotateFunction(function (_foundIndex) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _foundIndex";
            throw new Error(errorMessage);
          } /* Statements */
          return _actionBlock_1(_at_2(_self, _foundIndex));
        }, ["foundIndex"]),
        sl.annotateFunction(function (_previousIndex, _nextIndex) {
          /* ArityCheck */
          if (arguments.length !== 2) {
            const errorMessage =
              "Arity: expected 2, _previousIndex, _nextIndex";
            throw new Error(errorMessage);
          } /* Statements */
          return _cull_3(
            _exceptionBlock,
            _ifTrue_2(
              _greaterThanSign_2(_previousIndex, 0),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _at_2(_self, _previousIndex);
              }, []),
            ),
            _ifTrue_2(
              _lessThanSignEqualsSign_2(_nextIndex, _size_1(_self)),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _at_2(_self, _nextIndex);
              }, []),
            ),
          );
        }, ["previousIndex", "nextIndex"]),
      );
    },
    ["self", "aBlock:/1", "actionBlock:/1", "exceptionBlock"],
  ),
  "{ :self :aBlock:/1 :actionBlock:/1 :exceptionBlock | findBinaryIndexDoIfNone(self, aBlock:/1, { :foundIndex | actionBlock(at(self, foundIndex)) }, { :previousIndex :nextIndex | cull(exceptionBlock,ifTrue((>(previousIndex, 0)), { at(self, previousIndex) }), ifTrue((<=(nextIndex, size(self))), { at(self, nextIndex) })) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinaryIfNone",
  ["self", "aBlock:/1", "exceptionBlock"],
  sl.annotateFunction(function (_self, _aBlock_1, _exceptionBlock) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _aBlock_1, _exceptionBlock";
      throw new Error(errorMessage);
    } /* Statements */
    return _findBinaryDoIfNone_4(
      _self,
      _aBlock_1,
      sl.annotateFunction(function (_found) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _found";
          throw new Error(errorMessage);
        } /* Statements */
        return _found;
      }, ["found"]),
      _exceptionBlock,
    );
  }, ["self", "aBlock:/1", "exceptionBlock"]),
  "{ :self :aBlock:/1 :exceptionBlock | findBinaryDoIfNone(self,aBlock:/1, { :found | found }, exceptionBlock) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinaryIndex",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _findBinaryIndexDoIfNone_4(
      _self,
      _aBlock_1,
      sl.annotateFunction(function (_found) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _found";
          throw new Error(errorMessage);
        } /* Statements */
        return _found;
      }, ["found"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>findBinaryIndex: not found");
      }, []),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | findBinaryIndexDoIfNone(self, aBlock:/1, { :found | found }, { error(self,'@Sequenceable>>findBinaryIndex: not found') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinaryIndexDoIfNone",
  ["self", "aBlock:/1", "actionBlock:/1", "exceptionBlock"],
  sl.annotateFunction(
    function (_self, _aBlock_1, _actionBlock_1, _exceptionBlock) {
      /* ArityCheck */
      if (arguments.length !== 4) {
        const errorMessage =
          "Arity: expected 4, _self, _aBlock_1, _actionBlock_1, _exceptionBlock";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _low = 1;
      let _high = _size_1(_self);
      /* Statements */
      return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _return_1";
          throw new Error(errorMessage);
        } /* Statements */
        _whileFalse_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _lessThanSign_2(_high, _low);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _index = _solidusSolidus_2(_plusSign_2(_high, _low), 2);
            let _test = _aBlock_1(_at_2(_self, _index));
            /* Statements */
            return _if_3(
              _lessThanSign_2(_test, 0),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _high = _hyphenMinus_2(_index, 1);
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _if_3(
                  _lessThanSign_2(0, _test),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _low = _plusSign_2(_index, 1);
                  }, []),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _return_1(_actionBlock_1(_index));
                  }, []),
                );
              }, []),
            );
          }, []),
        );
        return _cull_3(_exceptionBlock, _high, _low);
      }, ["return:/1"]));
    },
    ["self", "aBlock:/1", "actionBlock:/1", "exceptionBlock"],
  ),
  "{ :self :aBlock:/1 :actionBlock:/1 :exceptionBlock | let low = 1; let high = size(self); valueWithReturn({ :return:/1 | whileFalse({ <(high, low) }, { let index = //(+(high, low), 2); let test = aBlock(at(self, index)); if((<(test, 0)), { high := -(index, 1) }, { if((<(0, test)), { low := +(index, 1) }, { return(actionBlock(index)) }) }) }); cull(exceptionBlock,high, low) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findBinaryIndexIfNone",
  ["self", "aBlock:/1", "exceptionBlock"],
  sl.annotateFunction(function (_self, _aBlock_1, _exceptionBlock) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _aBlock_1, _exceptionBlock";
      throw new Error(errorMessage);
    } /* Statements */
    return _findBinaryIndexDoIfNone_4(
      _self,
      _aBlock_1,
      sl.annotateFunction(function (_found) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _found";
          throw new Error(errorMessage);
        } /* Statements */
        return _found;
      }, ["found"]),
      _exceptionBlock,
    );
  }, ["self", "aBlock:/1", "exceptionBlock"]),
  "{ :self :aBlock:/1 :exceptionBlock | findBinaryIndexDoIfNone(self,aBlock:/1, { :found | found }, exceptionBlock) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "findLast",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _plusSign_2(_size_1(_self), 1);
    /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _whileTrue_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _greaterThanSignEqualsSign_2(
            _index = _hyphenMinus_2(_index, 1),
            1,
          );
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifTrue_2(
            _aBlock_1(_at_2(_self, _index)),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_index);
            }, []),
          );
        }, []),
      );
      return 0;
    }, ["return:/1"]));
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let index = +(size(self), 1); valueWithReturn({ :return:/1 | whileTrue({ >=((index := -(index, 1)), 1) }, { ifTrue(aBlock(at(self, index)), { return(index) }) }); 0 }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "first",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, 1);
  }, ["self"]),
  "{ :self | at(self, 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "first",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, 1, _n);
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,1, n) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "first",
  ["self", "n", "zero"],
  sl.annotateFunction(function (_self, _n, _zero) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _n, _zero";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_self);
    /* Statements */
    return _if_3(
      _greaterThanSignEqualsSign_2(_k, _n),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, 1, _n);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSignPlusSign_2(
          _self,
          _numberSign_2(_zero, _hyphenMinus_2(_n, _k)),
        );
      }, []),
    );
  }, ["self", "n", "zero"]),
  "{ :self :n :zero | let k = size(self); if((>=(k, n)), { copyFromTo(self,1, n) }, { ++(self, (#(zero, (-(n, k))))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fisherYatesShuffle",
  ["self", "rng"],
  sl.annotateFunction(function (_self, _rng) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _rng";
      throw new Error(errorMessage);
    } /* Statements */
    _toByDo_4(
      _size_1(_self),
      2,
      -1,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _swapWith_3(_self, _each, _nextRandomInteger_3(_rng, 1, _each));
      }, ["each"]),
    );
    return _self;
  }, ["self", "rng"]),
  "{ :self :rng | toByDo(size(self), 2, -1, { :each | swapWith(self,each, nextRandomInteger(rng,1, each)) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fisherYatesShuffle",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _fisherYatesShuffle_2(_self, _system);
  }, ["self"]),
  "{ :self | fisherYatesShuffle(self,system) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "flattenTo",
  ["self", "depth"],
  sl.annotateFunction(function (_self, _depth) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _depth";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_depth, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = [];
        /* Statements */
        _do_2(
          _self,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _isCollection_1(_each),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _addAll_2(
                  _answer,
                  _flattenTo_2(_each, _hyphenMinus_2(_depth, 1)),
                );
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _add_2(_answer, _each);
              }, []),
            );
          }, ["each"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "depth"]),
  "{ :self :depth | if((<=(depth, 0)), { self }, { let answer = []; do(self, { :each | if(isCollection(each), { addAll(answer,flattenTo(each,-(depth, 1))) }, { add(answer,each) }) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "flatten",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _flattenTo_2(_self, Infinity);
  }, ["self"]),
  "{ :self | flattenTo(self,Infinity) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "flatten",
  ["self", "depth"],
  sl.annotateFunction(function (_self, _depth) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _depth";
      throw new Error(errorMessage);
    } /* Statements */
    return _flattenTo_2(_self, _depth);
  }, ["self", "depth"]),
  "{ :self :depth | flattenTo(self,depth) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldedIndex",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Statements */
    return _foldBetweenAnd_3(_index, 1, _size_1(_self));
  }, ["self", "index"]),
  "{ :self :index | foldBetweenAnd(index,1, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldLeftPrefix",
  ["self", "count", "aBlock:/2"],
  sl.annotateFunction(function (_self, _count, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _count, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _errorEmptyCollection_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _at_2(_self, 1);
        /* Statements */
        _toDo_3(
          2,
          _size_1(_self),
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            return _answer = _aBlock_2(_answer, _at_2(_self, _index));
          }, ["index"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "count", "aBlock:/2"]),
  "{ :self :count :aBlock:/2 | ifEmpty(self, { errorEmptyCollection(self) }, { let answer = at(self, 1); toDo(2, size(self), { :index | answer := aBlock(answer, at(self, index)) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldLeft",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _foldLeftPrefix_3(_self, _size_1(_self), _aBlock_2);
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | foldLeftPrefix(self,size(self), aBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldList",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [_anObject];
    let _accumulator = _anObject;
    /* Statements */
    _do_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        _accumulator = _aBlock_2(_accumulator, _each);
        return _add_2(_answer, _accumulator);
      }, ["each"]),
    );
    return _answer;
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | let answer = [anObject]; let accumulator = anObject; do(self, { :each | accumulator := aBlock(accumulator, each); add(answer,accumulator) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldRightPrefix",
  ["self", "count", "aBlock:/2"],
  sl.annotateFunction(function (_self, _count, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _count, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _errorEmptyCollection_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _at_2(_self, _count);
        /* Statements */
        _toByDo_4(
          _hyphenMinus_2(_count, 1),
          1,
          -1,
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            return _answer = _aBlock_2(_at_2(_self, _index), _answer);
          }, ["index"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "count", "aBlock:/2"]),
  "{ :self :count :aBlock:/2 | ifEmpty(self, { errorEmptyCollection(self) }, { let answer = at(self, count); toByDo((-(count, 1)), 1, -1, { :index | answer := aBlock(at(self, index), answer) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "foldRight",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _foldRightPrefix_3(_self, _size_1(_self), _aBlock_2);
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | foldRightPrefix(self,size(self), aBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "forceToPaddingWith",
  ["self", "length", "anObject"],
  sl.annotateFunction(function (_self, _length, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _length, _anObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _new_2(_species_1(_self), _length);
    /* Statements */
    _atAllPut_2(_answer, _anObject);
    _replaceFromToWithStartingAt_5(
      _answer,
      1,
      _min_2(_size_1(_self), _length),
      _self,
      1,
    );
    return _answer;
  }, ["self", "length", "anObject"]),
  "{ :self :length :anObject | let answer = new(species(self),length); atAllPut(answer,anObject); replaceFromToWithStartingAt(answer,1, min(size(self),length), self, 1); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fourth",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _plusSign_2(1, 3));
  }, ["self"]),
  "{ :self | at(self, +(1, 3)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fourth",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(
      _self,
      _plusSign_2(_asterisk_2(_n, 3), 1),
      _asterisk_2(_n, 4),
    );
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,+(*(n, 3), 1), *(n, 4)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromContinuedFraction",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _tilde_2(_self, [0]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 0;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _Fraction_2(0n, 1n);
        /* Statements */
        _reverseDo_2(
          _self,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _next = _plusSign_2(_each, _answer);
            /* Statements */
            return _ifTrue_2(
              _greaterThanSign_2(_next, 0),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _answer = _solidus_2(1, _next);
              }, []),
            );
          }, ["each"]),
        );
        return _solidus_2(1, _answer);
      }, []),
    );
  }, ["self"]),
  "{ :self | if((~(self, [0])), { 0 }, { let answer = Fraction(0L, 1L); reverseDo(self, { :each | let next = (+(each, answer)); ifTrue((>(next, 0)), { answer := /(1, next) }) }); /(1, answer) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromDigits",
  ["self", "radix"],
  sl.annotateFunction(function (_self, _radix) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _radix";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = 0;
    let _m = 1;
    /* Statements */
    _reverseDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        _answer = _plusSign_2(_answer, _asterisk_2(_each, _m));
        return _m = _asterisk_2(_m, _radix);
      }, ["each"]),
    );
    return _answer;
  }, ["self", "radix"]),
  "{ :self :radix | let answer = 0; let m = 1; reverseDo(self, { :each | answer := +(answer, (*(each, m))); m := *(m, radix) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromDms",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _numberCompose_2(_self, [1, _solidus_2(1, 60), _solidus_2(1, 3600)]);
  }, ["self"]),
  "{ :self | numberCompose(self,[1, /(1, 60), /(1, 3600)]) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromToDo",
  ["self", "start", "stop", "aBlock:/1"],
  sl.annotateFunction(function (_self, _start, _stop, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _self, _start, _stop, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      _start,
      _stop,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "start", "stop", "aBlock:/1"]),
  "{ :self :start :stop :aBlock:/1 | toDo(start, stop, { :index | aBlock(at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromToKeysAndValuesDo",
  ["self", "start", "stop", "aBlock:/2"],
  sl.annotateFunction(function (_self, _start, _stop, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _self, _start, _stop, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      _start,
      _stop,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_index, _at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "start", "stop", "aBlock:/2"]),
  "{ :self :start :stop :aBlock:/2 | toDo(start, stop, { :index | aBlock(index, at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "fromToPut",
  ["self", "startIndex", "endIndex", "anObject"],
  sl.annotateFunction(function (_self, _startIndex, _endIndex, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _startIndex, _endIndex, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    _if_3(
      _greaterThanSign_2(_startIndex, _endIndex),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _written = 1;
        let _toWrite = _plusSign_2(_hyphenMinus_2(_endIndex, _startIndex), 1);
        let _thisWrite = null;
        /* Statements */
        _atPut_3(_self, _startIndex, _anObject);
        return _whileTrue_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _lessThanSign_2(_written, _toWrite);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _thisWrite = _min_2(_written, _hyphenMinus_2(_toWrite, _written));
            _replaceFromToWithStartingAt_5(
              _self,
              _plusSign_2(_startIndex, _written),
              _hyphenMinus_2(
                _plusSign_2(_plusSign_2(_startIndex, _written), _thisWrite),
                1,
              ),
              _self,
              _startIndex,
            );
            return _written = _plusSign_2(_written, _thisWrite);
          }, []),
        );
      }, []),
    );
    return _anObject;
  }, ["self", "startIndex", "endIndex", "anObject"]),
  "{ :self :startIndex :endIndex :anObject | if((>(startIndex, endIndex)), { self }, { let written = 1; let toWrite = +(-(endIndex, startIndex), 1); let thisWrite = nil; atPut(self, startIndex, anObject); whileTrue({ <(written, toWrite) }, { thisWrite := min(written,-(toWrite, written)); replaceFromToWithStartingAt(self,+(startIndex, written), -(+(+(startIndex, written), thisWrite), 1), self, startIndex); written := +(written, thisWrite) }) }); anObject }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "groupsDo",
  ["self", "aBlock"],
  sl.annotateFunction(function (_self, _aBlock) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _numArgs = _numArgs_1(_aBlock);
    /* Statements */
    return _caseOfOtherwise_3(
      _numArgs,
      [
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 0;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _error_2(
              _self,
              "groupsDo: At least one block argument expected",
            );
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 1;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _do_2(_self, _aBlock);
          }, []),
        ),
        _hyphenMinusGreaterThanSign_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return 2;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _pairsDo_2(_self, _aBlock);
          }, []),
        ),
      ],
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _argumentList = _List_1(_numArgs);
        let _index = 1;
        let _endIndex = _plusSign_2(
          _hyphenMinus_2(_size_1(_self), _numArgs),
          1,
        );
        /* Statements */
        return _whileTrue_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _lessThanSignEqualsSign_2(_index, _endIndex);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _replaceFromToWithStartingAt_5(
              _argumentList,
              1,
              _numArgs,
              _self,
              _index,
            );
            _valueWithArguments_2(_aBlock, _argumentList);
            return _index = _plusSign_2(_index, _numArgs);
          }, []),
        );
      }, []),
    );
  }, ["self", "aBlock"]),
  "{ :self :aBlock | let numArgs = numArgs(aBlock); caseOfOtherwise(numArgs, [->({ 0 }, { error(self,'groupsDo: At least one block argument expected') }), ->({ 1 }, { do(self,aBlock) }), ->({ 2 }, { pairsDo(self,aBlock) })], { let argumentList = List(numArgs); let index = 1; let endIndex = +(-(size(self), numArgs), 1); whileTrue({ <=(index, endIndex) }, { replaceFromToWithStartingAt(argumentList,1, numArgs, self, index); valueWithArguments(aBlock,argumentList); index := +(index, numArgs) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "grownBy",
  ["self", "length"],
  sl.annotateFunction(function (_self, _length) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _length";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _ofSize_2(
      _species_1(_self),
      _plusSign_2(_size_1(_self), _length),
    );
    /* Statements */
    return _replaceFromToWithStartingAt_5(_answer, 1, _size_1(_self), _self, 1);
  }, ["self", "length"]),
  "{ :self :length | let answer = ofSize(species(self),+(size(self), length)); replaceFromToWithStartingAt(answer,1, size(self), self, 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "hammingDistance",
  ["self", "other"],
  sl.annotateFunction(function (_self, _other) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _other";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _size = _min_2(_size_1(_self), _size_1(_other));
    let _count = _abs_1(_hyphenMinus_2(_size_1(_self), _size_1(_other)));
    /* Statements */
    _toDo_3(
      1,
      _size,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _tildeEqualsSign_2(_at_2(_self, _index), _at_2(_other, _index)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _count = _plusSign_2(_count, 1);
          }, []),
        );
      }, ["index"]),
    );
    return _count;
  }, ["self", "other"]),
  "{ :self :other | let size = min(size(self),size(other)); let count = abs((-(size(self), size(other)))); toDo(1, size, { :index | ifTrue((~=(at(self, index), at(other, index))), { count := +(count, 1) }) }); count }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "hasEqualElementsBy",
  ["self", "otherCollection", "aBlock:/2"],
  sl.annotateFunction(function (_self, _otherCollection, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _otherCollection, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _ampersand_2(
        _isSequenceable_1(_otherCollection),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_size_1(_self), _size_1(_otherCollection));
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _indicesDo_2(
            _self,
            sl.annotateFunction(function (_index) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _index";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifFalse_2(
                _aBlock_2(
                  _at_2(_self, _index),
                  _at_2(_otherCollection, _index),
                ),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
            }, ["index"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return false;
      }, []),
    );
  }, ["self", "otherCollection", "aBlock:/2"]),
  "{ :self :otherCollection :aBlock:/2 | if((&(isSequenceable(otherCollection), { =(size(self), size(otherCollection)) })), { valueWithReturn({ :return:/1 | indicesDo(self, { :index | ifFalse(aBlock(at(self, index), at(otherCollection, index)), { return(false) }) }); true }) }, { false }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "hasEqualElements",
  ["self", "otherCollection"],
  sl.annotateFunction(function (_self, _otherCollection) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _otherCollection";
      throw new Error(errorMessage);
    } /* Statements */
    return _hasEqualElementsBy_3(_self, _otherCollection, _equalsSign_2);
  }, ["self", "otherCollection"]),
  "{ :self :otherCollection | hasEqualElementsBy(self,otherCollection, =) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "includes",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _tildeEqualsSign_2(_indexOf_2(_self, _anObject), 0);
  }, ["self", "anObject"]),
  "{ :self :anObject | ~=(indexOf(self,anObject), 0) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "includesIndex",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Statements */
    return _ampersand_2(
      _isInteger_1(_index),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersand_2(
          _greaterThanSign_2(_index, 0),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _lessThanSignEqualsSign_2(_index, _size_1(_self));
          }, []),
        );
      }, []),
    );
  }, ["self", "index"]),
  "{ :self :index | &(isInteger(index), { &(>(index, 0), { <=(index, size(self)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "includesScatteredSubsequence",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _powerSetDo_2(
        _to_2(1, _size_1(_self)),
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifFalse_2(
            _isArithmeticSeriesBy_3(_each, 1, _equalsSign_2),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifTrue_2(
                _equalsSign_2(_commercialAtAsterisk_2(_self, _each), _aList),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(true);
                }, []),
              );
            }, []),
          );
        }, ["each"]),
      );
      return false;
    }, ["return:/1"]));
  }, ["self", "aList"]),
  "{ :self :aList | valueWithReturn({ :return:/1 | powerSetDo(to(1,size(self)), { :each | ifFalse(isArithmeticSeriesBy(each,1, =), { ifTrue((=(@*(self, each), aList)), { return(true) }) }) }); false }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "includesSubsequence",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i = 1;
      /* Statements */
      _do_2(
        _aList,
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          _i = _indexOfStartingAtBy_4(_self, _each, _i, _equalsSign_2);
          return _ifTrue_2(
            _equalsSign_2(_i, 0),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(false);
            }, []),
          );
        }, ["each"]),
      );
      return true;
    }, ["return:/1"]));
  }, ["self", "aList"]),
  "{ :self :aList | valueWithReturn({ :return:/1 | let i = 1; do(aList, { :each | i := indexOfStartingAtBy(self,each, i, =); ifTrue((=(i, 0)), { return(false) }) }); true }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "includesSubstring",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_aList);
    let _c = _first_1(_aList);
    /* Statements */
    return _anySatisfy_2(
      _indicesOf_2(_self, _c),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _allSatisfy_2(
          _upOrDownTo_2(1, _k),
          sl.annotateFunction(function (_j) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _equalsSign_2(
              _at_2(_self, _hyphenMinus_2(_plusSign_2(_i, _j), 1)),
              _at_2(_aList, _j),
            );
          }, ["j"]),
        );
      }, ["i"]),
    );
  }, ["self", "aList"]),
  "{ :self :aList | let k = size(aList); let c = first(aList); anySatisfy(indicesOf(self,c), { :i | allSatisfy(upOrDownTo(1, k), { :j | =(at(self, -(+(i, j), 1)), at(aList, j)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "increasingSubsequenceList",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_size_1(_self), 2),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [_self];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _increasing_2 = sl.annotateFunction(function (_done, _remaining) {
          /* ArityCheck */
          if (arguments.length !== 2) {
            const errorMessage = "Arity: expected 2, _done, _remaining";
            throw new Error(errorMessage);
          } /* Statements */
          return _if_3(
            _isEmpty_1(_remaining),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return [_done];
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _plusSignPlusSign_2(
                _if_3(
                  _aBlock_2(_last_1(_done), _first_1(_remaining)),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _increasing_2(
                      _plusSignPlusSign_2(_done, [_first_1(_remaining)]),
                      _allButFirst_1(_remaining),
                    );
                  }, []),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return [];
                  }, []),
                ),
                _increasing_2(_done, _allButFirst_1(_remaining)),
              );
            }, []),
          );
        }, ["done", "remaining"]);
        /* Statements */
        return _gather_2(
          _upOrDownTo_2(1, _size_1(_self)),
          sl.annotateFunction(function (_i) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _i";
              throw new Error(errorMessage);
            } /* Statements */
            return _increasing_2(
              _last_2(_first_2(_self, _i), 1),
              _drop_2(_self, _i),
            );
          }, ["i"]),
        );
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | if((<(size(self), 2)), { [self] }, { let increasing = { :done :remaining | if(isEmpty(remaining), { [done] }, { ++(if(aBlock(last(done), first(remaining)), { increasing(++(done, [first(remaining)]), allButFirst(remaining)) }, { [] }), increasing(done, allButFirst(remaining))) }) }; gather(upOrDownTo(1, size(self)), { :i | increasing(last(first(self,i),1), drop(self,i)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "increasingSubsequenceList",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _increasingSubsequenceList_2(_self, _lessThanSign_2);
  }, ["self"]),
  "{ :self | increasingSubsequenceList(self,<) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOf",
  ["self", "anElement"],
  sl.annotateFunction(function (_self, _anElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anElement";
      throw new Error(errorMessage);
    } /* Statements */
    return _indexOfStartingAtBy_4(_self, _anElement, 1, _equalsSign_2);
  }, ["self", "anElement"]),
  "{ :self :anElement | indexOfStartingAtBy(self,anElement, 1, =) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOfBy",
  ["self", "anElement", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anElement, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anElement, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _indexOfStartingAtBy_4(_self, _anElement, 1, _aBlock_2);
  }, ["self", "anElement", "aBlock:/2"]),
  "{ :self :anElement :aBlock:/2 | indexOfStartingAtBy(self,anElement, 1, aBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOfIfAbsent",
  ["self", "anElement", "exceptionBlock:/0"],
  sl.annotateFunction(function (_self, _anElement, _exceptionBlock_0) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _anElement, _exceptionBlock_0";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _indexOfStartingAtBy_4(_self, _anElement, 1, _equalsSign_2);
    /* Statements */
    return _if_3(
      _equalsSign_2(_index, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _exceptionBlock_0();
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _index;
      }, []),
    );
  }, ["self", "anElement", "exceptionBlock:/0"]),
  "{ :self :anElement :exceptionBlock:/0 | let index = indexOfStartingAtBy(self,anElement, 1, =); if((=(index, 0)), { exceptionBlock() }, { index }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOfStartingAtBy",
  ["self", "anElement", "start", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anElement, _start, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _anElement, _start, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _toDo_3(
        _start,
        _size_1(_self),
        sl.annotateFunction(function (_index) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _index";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifTrue_2(
            _aBlock_2(_at_2(_self, _index), _anElement),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_index);
            }, []),
          );
        }, ["index"]),
      );
      return 0;
    }, ["return:/1"]));
  }, ["self", "anElement", "start", "aBlock:/2"]),
  "{ :self :anElement :start :aBlock:/2 | valueWithReturn({ :return:/1 | toDo(start, size(self), { :index | ifTrue(aBlock(at(self, index), anElement), { return(index) }) }); 0 }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOfSubstring",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _indexOfSubstringStartingAt_3(_self, _aList, 1);
  }, ["self", "aList"]),
  "{ :self :aList | indexOfSubstringStartingAt(self,aList, 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexOfSubstringStartingAt",
  ["self", "aList", "start"],
  sl.annotateFunction(function (_self, _aList, _start) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _start";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _k = _size_1(_aList);
    /* Statements */
    return _if_3(
      _equalsSign_2(_k, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 0;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _first = _at_2(_aList, 1);
        /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _toDo_3(
            _max_2(_start, 1),
            _plusSign_2(_hyphenMinus_2(_size_1(_self), _k), 1),
            sl.annotateFunction(function (_startIndex) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _startIndex";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifTrue_2(
                _equalsSign_2(_at_2(_self, _startIndex), _first),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Temporaries */
                  let _index = 2;
                  /* Statements */
                  _whileTrue_2(
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _ampersand_2(
                        _lessThanSignEqualsSign_2(_index, _k),
                        sl.annotateFunction(function () {
                          /* ArityCheck */
                          if (arguments.length !== 0) {
                            const errorMessage = "Arity: expected 0, ";
                            throw new Error(errorMessage);
                          } /* Statements */
                          return _equalsSign_2(
                            _at_2(
                              _self,
                              _hyphenMinus_2(
                                _plusSign_2(_startIndex, _index),
                                1,
                              ),
                            ),
                            _at_2(_aList, _index),
                          );
                        }, []),
                      );
                    }, []),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _index = _plusSign_2(_index, 1);
                    }, []),
                  );
                  return _ifFalse_2(
                    _lessThanSignEqualsSign_2(_index, _k),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _return_1(_startIndex);
                    }, []),
                  );
                }, []),
              );
            }, ["startIndex"]),
          );
          return 0;
        }, ["return:/1"]));
      }, []),
    );
  }, ["self", "aList", "start"]),
  "{ :self :aList :start | let k = size(aList); if((=(k, 0)), { 0 }, { let first = at(aList, 1); valueWithReturn({ :return:/1 | toDo(max(start,1), +(-(size(self), k), 1), { :startIndex | ifTrue((=(at(self, startIndex), first)), { let index = 2; whileTrue({ &(<=(index, k), { =(at(self, -(+(startIndex, index), 1)), at(aList, index)) }) }, { index := +(index, 1) }); ifFalse((<=(index, k)), { return(startIndex) }) }) }); 0 }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indexValueAssociations",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _List_1(_size_1(_self));
    /* Statements */
    _withIndexDo_2(
      _self,
      sl.annotateFunction(function (_each, _index) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _each, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _answer,
          _index,
          _hyphenMinusGreaterThanSign_2(_index, _each),
        );
      }, ["each", "index"]),
    );
    return _answer;
  }, ["self"]),
  "{ :self | let answer = List(size(self)); withIndexDo(self, { :each :index | atPut(answer, index, (->(index, each))) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indices",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _upOrDownTo_2(1, _size_1(_self));
  }, ["self"]),
  "{ :self | upOrDownTo(1, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indicesSorted",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _indices_1(_self);
  }, ["self"]),
  "{ :self | indices(self) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indicesDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(1, _size_1(_self), _aBlock_1);
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toDo(1,size(self), aBlock:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indicesOfSubsequence",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _powerSetDo_2(
      _to_2(1, _size_1(_self)),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _equalsSign_2(_commercialAtAsterisk_2(_self, _each), _aList),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _add_2(_answer, _each);
          }, []),
        );
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aList"]),
  "{ :self :aList | let answer = []; powerSetDo(to(1,size(self)), { :each | ifTrue((=(@*(self, each), aList)), { add(answer,each) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indicesOfSubstring",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesOfSubstringStartingAt_3(_self, _aList, 1);
  }, ["self", "aList"]),
  "{ :self :aList | indicesOfSubstringStartingAt(self,aList, 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "indicesOfSubstringStartingAt",
  ["self", "aList", "initialIndex"],
  sl.annotateFunction(function (_self, _aList, _initialIndex) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _initialIndex";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    let _index = _hyphenMinus_2(_initialIndex, 1);
    /* Statements */
    _whileFalse_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _index = _indexOfSubstringStartingAt_3(
          _self,
          _aList,
          _plusSign_2(_index, 1),
        );
        return _equalsSign_2(_index, 0);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _index);
      }, []),
    );
    return _answer;
  }, ["self", "aList", "initialIndex"]),
  "{ :self :aList :initialIndex | let answer = []; let index = -(initialIndex, 1); whileFalse({ index := indexOfSubstringStartingAt(self,aList, +(index, 1)); =(index, 0) }, { add(answer,index) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "injectInto",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _anObject;
    /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _answer = _aBlock_2(_answer, _at_2(_self, _index));
      }, ["index"]),
    );
    return _answer;
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | let answer = anObject; indicesDo(self, { :index | answer := aBlock(answer, at(self, index)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "intercalate",
  ["self", "aCollection"],
  sl.annotateFunction(function (_self, _aCollection) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aCollection";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _doSeparatedBy_3(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _addAll_2(_answer, _aCollection);
      }, []),
    );
    return _answer;
  }, ["self", "aCollection"]),
  "{ :self :aCollection | let answer = []; doSeparatedBy(self, { :each | add(answer,each) }, { addAll(answer,aCollection) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "interleave",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    let _k = _max_2(_size_1(_self), _size_1(_aList));
    /* Statements */
    _toDo_3(
      1,
      _k,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        _add_2(_answer, _atWrap_2(_self, _i));
        return _add_2(_answer, _atWrap_2(_aList, _i));
      }, ["i"]),
    );
    return _answer;
  }, ["self", "aList"]),
  "{ :self :aList | let answer = []; let k = max(size(self),size(aList)); toDo(1, k, { :i | add(answer,atWrap(self,i)); add(answer,atWrap(aList,i)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "intersperse",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _doSeparatedBy_3(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _anObject);
      }, []),
    );
    return _answer;
  }, ["self", "anObject"]),
  "{ :self :anObject | let answer = []; doSeparatedBy(self, { :each | add(answer,each) }, { add(answer,anObject) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isArithmeticSeries",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_size_1(_self), 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return true;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _isArithmeticSeriesBy_3(
          _self,
          _hyphenMinus_2(_second_1(_self), _first_1(_self)),
          _equalsSign_2,
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | if((<=(size(self), 1)), { true }, { isArithmeticSeriesBy(self,-(second(self), first(self)), =) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isArithmeticSeriesBy",
  ["self", "aNumber", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aNumber, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aNumber, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_size_1(_self), 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return true;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _adjacentPairsDo_2(
            _self,
            sl.annotateFunction(function (_a, _b) {
              /* ArityCheck */
              if (arguments.length !== 2) {
                const errorMessage = "Arity: expected 2, _a, _b";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifFalse_2(
                _aBlock_2(_hyphenMinus_2(_b, _a), _aNumber),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
            }, ["a", "b"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
    );
  }, ["self", "aNumber", "aBlock:/2"]),
  "{ :self :aNumber :aBlock:/2 | if((<=(size(self), 1)), { true }, { valueWithReturn({ :return:/1 | adjacentPairsDo(self, { :a :b | ifFalse(aBlock(-(b, a), aNumber), { return(false) }) }); true }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isCloseTo",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _equalBy_3(_self, _anObject, _isCloseTo_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | equalBy(self,anObject, isCloseTo:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isGeometricSeries",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_size_1(_self), 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return true;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _isGeometricSeriesBy_3(
          _self,
          _solidus_2(_second_1(_self), _first_1(_self)),
          _equalsSign_2,
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | if((<=(size(self), 1)), { true }, { isGeometricSeriesBy(self,/(second(self), first(self)), =) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isGeometricSeriesBy",
  ["self", "aNumber", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aNumber, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aNumber, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_size_1(_self), 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return true;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _adjacentPairsDo_2(
            _self,
            sl.annotateFunction(function (_a, _b) {
              /* ArityCheck */
              if (arguments.length !== 2) {
                const errorMessage = "Arity: expected 2, _a, _b";
                throw new Error(errorMessage);
              } /* Statements */
              return _ifFalse_2(
                _aBlock_2(_solidus_2(_b, _a), _aNumber),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
            }, ["a", "b"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
    );
  }, ["self", "aNumber", "aBlock:/2"]),
  "{ :self :aNumber :aBlock:/2 | if((<=(size(self), 1)), { true }, { valueWithReturn({ :return:/1 | adjacentPairsDo(self, { :a :b | ifFalse(aBlock(/(b, a), aNumber), { return(false) }) }); true }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isMonotonicallyIncreasing",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _ampersand_2(
      _isFinite_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _isSortedBy_2(_self, _lessThanSignEqualsSign_2);
      }, []),
    );
  }, ["self"]),
  "{ :self | &(isFinite(self), { isSortedBy(self,<=) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isOctetSequence",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _allSatisfy_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersand_2(
          _isInteger_1(_each),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _betweenAnd_3(_each, 0, 255);
          }, []),
        );
      }, ["each"]),
    );
  }, ["self"]),
  "{ :self | allSatisfy(self, { :each | &(isInteger(each), { betweenAnd(each,0, 255) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isSequenceable",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return true;
  }, ["self"]),
  "{ :self | true }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isSorted",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _isSortedBetweenAnd_3(_self, 1, _size_1(_self));
  }, ["self"]),
  "{ :self | isSortedBetweenAnd(self,1, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isSortedBetweenAnd",
  ["self", "startIndex", "endIndex"],
  sl.annotateFunction(function (_self, _startIndex, _endIndex) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _startIndex, _endIndex";
      throw new Error(errorMessage);
    } /* Statements */
    return _isSortedByBetweenAnd_4(
      _self,
      _lessThanSignEqualsSign_2,
      _startIndex,
      _endIndex,
    );
  }, ["self", "startIndex", "endIndex"]),
  "{ :self :startIndex :endIndex | isSortedByBetweenAnd(self,<=, startIndex, endIndex) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isSortedBy",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _isSortedByBetweenAnd_4(_self, _aBlock_2, 1, _size_1(_self));
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | isSortedByBetweenAnd(self,aBlock:/2, 1, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isSortedByBetweenAnd",
  ["self", "aBlock:/2", "startIndex", "endIndex"],
  sl.annotateFunction(function (_self, _aBlock_2, _startIndex, _endIndex) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _aBlock_2, _startIndex, _endIndex";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_endIndex, _startIndex),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return true;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _previousElement = _at_2(_self, _startIndex);
        /* Statements */
        return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _return_1";
            throw new Error(errorMessage);
          } /* Statements */
          _toDo_3(
            _plusSign_2(_startIndex, 1),
            _endIndex,
            sl.annotateFunction(function (_index) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _index";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _element = _at_2(_self, _index);
              /* Statements */
              _ifFalse_2(
                _aBlock_2(_previousElement, _element),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(false);
                }, []),
              );
              return _previousElement = _element;
            }, ["index"]),
          );
          return true;
        }, ["return:/1"]));
      }, []),
    );
  }, ["self", "aBlock:/2", "startIndex", "endIndex"]),
  "{ :self :aBlock:/2 :startIndex :endIndex | if((<=(endIndex, startIndex)), { true }, { let previousElement = at(self, startIndex); valueWithReturn({ :return:/1 | toDo((+(startIndex, 1)), endIndex, { :index | let element = at(self, index); ifFalse(aBlock(previousElement, element), { return(false) }); previousElement := element }); true }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isStrictlyIncreasing",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _ampersand_2(
      _isFinite_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _isSortedBy_2(_self, _lessThanSign_2);
      }, []),
    );
  }, ["self"]),
  "{ :self | &(isFinite(self), { isSortedBy(self,<) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isVectorOf",
  ["self", "elementType"],
  sl.annotateFunction(function (_self, _elementType) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _elementType";
      throw new Error(errorMessage);
    } /* Statements */
    return _ampersand_2(
      _isVector_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _equalsSign_2(_elementType_1(_self), _elementType);
      }, []),
    );
  }, ["self", "elementType"]),
  "{ :self :elementType | &(isVector(self), { =(elementType(self), elementType) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "keysAndValuesDo",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _withIndexDo_2(
      _self,
      sl.annotateFunction(function (_each, _index) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _each, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_index, _each);
      }, ["each", "index"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | withIndexDo(self, { :each :index | aBlock(index, each) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "last",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _size_1(_self));
  }, ["self"]),
  "{ :self | at(self, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "last",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _i = _size_1(_self);
    /* Statements */
    return _copyFromTo_3(_self, _plusSign_2(_hyphenMinus_2(_i, _n), 1), _i);
  }, ["self", "n"]),
  "{ :self :n | let i = size(self); copyFromTo(self,+(-(i, n), 1), i) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "last",
  ["self", "n", "zero"],
  sl.annotateFunction(function (_self, _n, _zero) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _n, _zero";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _i = _size_1(_self);
    /* Statements */
    return _if_3(
      _greaterThanSignEqualsSign_2(_i, _n),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, _plusSign_2(_hyphenMinus_2(_i, _n), 1), _i);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSignPlusSign_2(
          _numberSign_2(_zero, _hyphenMinus_2(_n, _i)),
          _self,
        );
      }, []),
    );
  }, ["self", "n", "zero"]),
  "{ :self :n :zero | let i = size(self); if((>=(i, n)), { copyFromTo(self,+(-(i, n), 1), i) }, { ++((#(zero, (-(n, i)))), self) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lastIndexOf",
  ["self", "anElement"],
  sl.annotateFunction(function (_self, _anElement) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anElement";
      throw new Error(errorMessage);
    } /* Statements */
    return _lastIndexOfStartingAt_3(_self, _anElement, _size_1(_self));
  }, ["self", "anElement"]),
  "{ :self :anElement | lastIndexOfStartingAt(self,anElement, size(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lastIndexOfIfAbsent",
  ["self", "anElement", "exceptionBlock:/0"],
  sl.annotateFunction(function (_self, _anElement, _exceptionBlock_0) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _anElement, _exceptionBlock_0";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = _lastIndexOfStartingAt_3(_self, _anElement, _size_1(_self));
    /* Statements */
    return _if_3(
      _equalsSign_2(_index, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _exceptionBlock_0();
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _index;
      }, []),
    );
  }, ["self", "anElement", "exceptionBlock:/0"]),
  "{ :self :anElement :exceptionBlock:/0 | let index = lastIndexOfStartingAt(self,anElement, size(self)); if((=(index, 0)), { exceptionBlock() }, { index }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lastIndexOfStartingAt",
  ["self", "anElement", "lastIndex"],
  sl.annotateFunction(function (_self, _anElement, _lastIndex) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anElement, _lastIndex";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _toByDo_4(
        _lastIndex,
        1,
        -1,
        sl.annotateFunction(function (_index) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _index";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifTrue_2(
            _equalsSign_2(_at_2(_self, _index), _anElement),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_index);
            }, []),
          );
        }, ["index"]),
      );
      return 0;
    }, ["return:/1"]));
  }, ["self", "anElement", "lastIndex"]),
  "{ :self :anElement :lastIndex | valueWithReturn({ :return:/1 | toByDo(lastIndex, 1, -1, { :index | ifTrue((=(at(self, index), anElement)), { return(index) }) }); 0 }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lastIndexOfStartingAtIfAbsent",
  ["self", "anElement", "lastIndex", "exceptionBlock:/0"],
  sl.annotateFunction(
    function (_self, _anElement, _lastIndex, _exceptionBlock_0) {
      /* ArityCheck */
      if (arguments.length !== 4) {
        const errorMessage =
          "Arity: expected 4, _self, _anElement, _lastIndex, _exceptionBlock_0";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _index = _lastIndexOfStartingAt_3(_self, _anElement, _lastIndex);
      /* Statements */
      return _if_3(
        _equalsSign_2(_index, 0),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _exceptionBlock_0();
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _index;
        }, []),
      );
    },
    ["self", "anElement", "lastIndex", "exceptionBlock:/0"],
  ),
  "{ :self :anElement :lastIndex :exceptionBlock:/0 | let index = lastIndexOfStartingAt(self,anElement, lastIndex); if((=(index, 0)), { exceptionBlock() }, { index }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lexicographicallyLeastRotationStartIndex",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _f = _List_2(_asterisk_2(2, _n), -1);
    let _k = 0;
    let _b_1 = sl.annotateFunction(function (_i) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _i";
        throw new Error(errorMessage);
      } /* Statements */
      return _at_2(
        _self,
        _plusSign_2(_percentSign_2(_hyphenMinus_2(_i, 1), _n), 1),
      );
    }, ["i"]);
    /* Statements */
    _toDo_3(
      1,
      _hyphenMinus_2(_asterisk_2(2, _n), 1),
      sl.annotateFunction(function (_j) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _j";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _i = _at_2(_f, _hyphenMinus_2(_j, _k));
        /* Statements */
        _whileTrue_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _ampersand_2(
              _tildeEqualsSign_2(_i, -1),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _tildeEqualsSign_2(
                  _b_1(_j),
                  _b_1(_plusSign_2(_plusSign_2(_k, _i), 1)),
                );
              }, []),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _ifTrue_2(
              _lessThanSign_2(
                _b_1(_j),
                _b_1(_plusSign_2(_plusSign_2(_k, _i), 1)),
              ),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _k = _hyphenMinus_2(_hyphenMinus_2(_j, _i), 1);
              }, []),
            );
            return _i = _at_2(_f, _plusSign_2(_i, 1));
          }, []),
        );
        return _if_3(
          _ampersand_2(
            _equalsSign_2(_i, -1),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _tildeEqualsSign_2(
                _b_1(_j),
                _b_1(_plusSign_2(_plusSign_2(_k, _i), 1)),
              );
            }, []),
          ),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _ifTrue_2(
              _lessThanSign_2(
                _b_1(_j),
                _b_1(_plusSign_2(_plusSign_2(_k, _i), 1)),
              ),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _k = _j;
              }, []),
            );
            return _atPut_3(_f, _plusSign_2(_hyphenMinus_2(_j, _k), 1), -1);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _atPut_3(
              _f,
              _plusSign_2(_hyphenMinus_2(_j, _k), 1),
              _plusSign_2(_i, 1),
            );
          }, []),
        );
      }, ["j"]),
    );
    return _plusSign_2(_percentSign_2(_hyphenMinus_2(_k, 1), _n), 1);
  }, ["self"]),
  "{ :self | let n = size(self); let f = List(*(2, n), -1); let k = 0; let b = { :i | at(self, +(%(-(i, 1), n), 1)) }; toDo(1, -(*(2, n), 1), { :j | let i = at(f, -(j, k)); whileTrue({ &((~=(i, -1)), { ~=(b(j), b(+(+(k, i), 1))) }) }, { ifTrue((<(b(j), b(+(+(k, i), 1)))), { k := -(-(j, i), 1) }); i := at(f, +(i, 1)) }); if((&((=(i, -1)), { ~=(b(j), b(+(+(k, i), 1))) })), { ifTrue((<(b(j), b(+(+(k, i), 1)))), { k := j }); atPut(f, +(-(j, k), 1), -1) }, { atPut(f, +(-(j, k), 1), +(i, 1)) }) }); +(%(-(k, 1), n), 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lexicographicallyLeastRotation",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _rotatedLeft_2(
      _self,
      _hyphenMinus_2(_lexicographicallyLeastRotationStartIndex_1(_self), 1),
    );
  }, ["self"]),
  "{ :self | rotatedLeft(self,-(lexicographicallyLeastRotationStartIndex(self), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "leastSquares",
  ["m", "b"],
  sl.annotateFunction(function (_m, _b) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _m, _b";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _transposed_1(_m);
    let _y = [_b];
    let _cx = _m;
    let _cy = _transposed_1(_y);
    /* Statements */
    return _first_1(
      _transposed_1(_dot_2(_dot_2(_inverse_1(_dot_2(_x, _cx)), _x), _cy)),
    );
  }, ["m", "b"]),
  "{ :m :b | let x = transposed(m); let y = [b]; let cx = m; let cy = transposed(y); first(transposed(dot(dot(inverse(dot(x,cx)),x),cy))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "levenshteinDistance",
  ["self", "other"],
  sl.annotateFunction(function (_self, _other) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _other";
      throw new Error(errorMessage);
    } /* Statements */
    return _levenshteinDistance_3(_self, _other, _equalsSign_2);
  }, ["self", "other"]),
  "{ :self :other | levenshteinDistance(self,other, =) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "levenshteinDistance",
  ["self", "other", "equalityBlock:/2"],
  sl.annotateFunction(function (_self, _other, _equalityBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _other, _equalityBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _verticalLine_2(
        _isEmpty_1(_self),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _isEmpty_1(_other);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _size_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _matrix = _asList_1(_upOrDownTo_2(0, _size_1(_other)));
        /* Statements */
        _toDo_3(
          1,
          _size_1(_self),
          sl.annotateFunction(function (_xIndex) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _xIndex";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _corner = _hyphenMinus_2(_xIndex, 1);
            /* Statements */
            _atPut_3(_matrix, 1, _hyphenMinus_2(_xIndex, 1));
            return _toDo_3(
              1,
              _size_1(_other),
              sl.annotateFunction(function (_yIndex) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _yIndex";
                  throw new Error(errorMessage);
                } /* Temporaries */
                let _upper = _at_2(_matrix, _plusSign_2(_yIndex, 1));
                /* Statements */
                _atPut_3(
                  _matrix,
                  _plusSign_2(_yIndex, 1),
                  _if_3(
                    _equalityBlock_2(
                      _at_2(_self, _xIndex),
                      _at_2(_other, _yIndex),
                    ),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _corner;
                    }, []),
                    sl.annotateFunction(function () {
                      /* ArityCheck */
                      if (arguments.length !== 0) {
                        const errorMessage = "Arity: expected 0, ";
                        throw new Error(errorMessage);
                      } /* Statements */
                      return _plusSign_2(
                        _min_1([_upper, _corner, _at_2(_matrix, _yIndex)]),
                        1,
                      );
                    }, []),
                  ),
                );
                return _corner = _upper;
              }, ["yIndex"]),
            );
          }, ["xIndex"]),
        );
        return _at_2(_matrix, _plusSign_2(_size_1(_other), 1));
      }, []),
    );
  }, ["self", "other", "equalityBlock:/2"]),
  "{ :self :other :equalityBlock:/2 | if((|(isEmpty(self), { isEmpty(other) })), { size(self) }, { let matrix = asList(upOrDownTo(0, size(other))); toDo(1, size(self), { :xIndex | let corner = -(xIndex, 1); atPut(matrix, 1, -(xIndex, 1)); toDo(1, size(other), { :yIndex | let upper = at(matrix, +(yIndex, 1)); atPut(matrix, +(yIndex, 1), if(equalityBlock(at(self, xIndex), at(other, yIndex)), { corner }, { +(min([upper, corner, at(matrix, yIndex)]), 1) })); corner := upper }) }); at(matrix, +(size(other), 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "linearIndex",
  ["shape", "aList"],
  sl.annotateFunction(function (_shape, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _shape, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _plusSign_2(
      _mixedRadixDecode_2(_hyphenMinus_2(_aList, 1), _shape),
      1,
    );
  }, ["shape", "aList"]),
  "{ :shape :aList | +(mixedRadixDecode((-(aList, 1)),shape), 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "linearRecurrence",
  ["kernel", "init", "n"],
  sl.annotateFunction(function (_kernel, _init, _n) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _kernel, _init, _n";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _copy_1(_init);
    let _m = _size_1(_kernel);
    let _d = _size_1(_init);
    let _j_2 = _if_3(
      _verticalLineVerticalLine_2(_isVector_1(_kernel), _isVector_1(_init)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _asterisk_2;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _dot_2;
      }, []),
    );
    /* Statements */
    _toDo_3(
      _d,
      _hyphenMinus_2(_n, 1),
      sl.annotateFunction(function (_k) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _k";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _z = 0;
        /* Statements */
        _toDo_3(
          1,
          _m,
          sl.annotateFunction(function (_i) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _i";
              throw new Error(errorMessage);
            } /* Statements */
            return _z = _plusSign_2(
              _z,
              _j_2(
                _at_2(_kernel, _i),
                _at_2(_answer, _hyphenMinus_2(_plusSign_2(_k, 1), _i)),
              ),
            );
          }, ["i"]),
        );
        return _add_2(_answer, _z);
      }, ["k"]),
    );
    return _answer;
  }, ["kernel", "init", "n"]),
  "{ :kernel :init :n | let answer = copy(init); let m = size(kernel); let d = size(init); let j:/2 = if((||(isVector(kernel), isVector(init))), { * }, { dot:/2 }); toDo(d, -(n, 1), { :k | let z = 0; toDo(1, m, { :i | z := +(z, j(at(kernel, i), at(answer, -(+(k, 1), i)))) }); add(answer,z) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "longestCommonSubsequence",
  ["a", "b"],
  sl.annotateFunction(function (_a, _b) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _a, _b";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _m = _plusSign_2(_size_1(_a), 1);
    let _n = _plusSign_2(_size_1(_b), 1);
    let _lengths = _zeroMatrix_2(_m, _n);
    let _answer = [];
    /* Statements */
    _withIndexCollect_2(
      _a,
      sl.annotateFunction(function (_x, _i) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _x, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _withIndexCollect_2(
          _b,
          sl.annotateFunction(function (_y, _j) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _y, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _equalsSign_2(_x, _y),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _atPut_4(
                  _lengths,
                  _plusSign_2(_i, 1),
                  _plusSign_2(_j, 1),
                  _plusSign_2(_at_3(_lengths, _i, _j), 1),
                );
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _atPut_4(
                  _lengths,
                  _plusSign_2(_i, 1),
                  _plusSign_2(_j, 1),
                  _max_2(
                    _at_3(_lengths, _plusSign_2(_i, 1), _j),
                    _at_3(_lengths, _i, _plusSign_2(_j, 1)),
                  ),
                );
              }, []),
            );
          }, ["y", "j"]),
        );
      }, ["x", "i"]),
    );
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersandAmpersand_2(
          _greaterThanSign_2(_m, 1),
          _greaterThanSign_2(_n, 1),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _equalsSign_2(
            _at_3(_lengths, _m, _n),
            _at_3(_lengths, _hyphenMinus_2(_m, 1), _n),
          ),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _m = _hyphenMinus_2(_m, 1);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _equalsSign_2(
                _at_3(_lengths, _m, _n),
                _at_3(_lengths, _m, _hyphenMinus_2(_n, 1)),
              ),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _n = _hyphenMinus_2(_n, 1);
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _ifFalse_2(
                  _equalsSign_2(
                    _at_2(_a, _hyphenMinus_2(_m, 1)),
                    _at_2(_b, _hyphenMinus_2(_n, 1)),
                  ),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _error_1(
                      "@Sequenceable>>longestCommonSubsequence: error?",
                    );
                  }, []),
                );
                _addFirst_2(_answer, _at_2(_a, _hyphenMinus_2(_m, 1)));
                _m = _hyphenMinus_2(_m, 1);
                return _n = _hyphenMinus_2(_n, 1);
              }, []),
            );
          }, []),
        );
      }, []),
    );
    return _answer;
  }, ["a", "b"]),
  "{ :a :b | let m = +(size(a), 1); let n = +(size(b), 1); let lengths = zeroMatrix(m,n); let answer = []; withIndexCollect(a, { :x :i | withIndexCollect(b, { :y :j | if((=(x, y)), { atPut(lengths, +(i, 1), +(j, 1), +(at(lengths, i, j), 1)) }, { atPut(lengths, +(i, 1), +(j, 1), max(at(lengths, +(i, 1), j),at(lengths, i, +(j, 1)))) }) }) }); whileTrue({ &&((>(m, 1)), (>(n, 1))) }, { if((=(at(lengths, m, n), at(lengths, -(m, 1), n))), { m := -(m, 1) }, { if((=(at(lengths, m, n), at(lengths, m, -(n, 1)))), { n := -(n, 1) }, { ifFalse((=(at(a, -(m, 1)), at(b, -(n, 1)))), { error('@Sequenceable>>longestCommonSubsequence: error?') }); addFirst(answer,at(a, -(m, 1))); m := -(m, 1); n := -(n, 1) }) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "longestCommonSubstringList",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _find_1 = sl.annotateFunction(function (_k) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _k";
        throw new Error(errorMessage);
      } /* Statements */
      return _intersection_2(
        _partition_3(_self, _k, 1),
        _partition_3(_aList, _k, 1),
      );
    }, ["k"]);
    let _n = _min_2(_size_1(_self), _size_1(_aList));
    /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _toByDo_4(
        _n,
        1,
        -1,
        sl.annotateFunction(function (_k) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _k";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _common = _find_1(_k);
          /* Statements */
          return _ifFalse_2(
            _isEmpty_1(_common),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_common);
            }, []),
          );
        }, ["k"]),
      );
      return [];
    }, ["return:/1"]));
  }, ["self", "aList"]),
  "{ :self :aList | let find = { :k | intersection(partition(self,k, 1),partition(aList,k, 1)) }; let n = min(size(self),size(aList)); valueWithReturn({ :return:/1 | toByDo(n, 1, -1, { :k | let common = find(k); ifFalse(isEmpty(common), { return(common) }) }); [] }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "longestCommonSubstring",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _common = _longestCommonSubstringList_2(_self, _aList);
    /* Statements */
    return _if_3(
      _isEmpty_1(_common),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _first_1(_common);
      }, []),
    );
  }, ["self", "aList"]),
  "{ :self :aList | let common = longestCommonSubstringList(self,aList); if(isEmpty(common), { [] }, { first(common) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "longestIncreasingSubsequence",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _self;
    let _n = _size_1(_x);
    /* Statements */
    return _if_3(
      _lessThanSign_2(_n, 2),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _x;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _p = _List_2(_n, 0);
        let _m = _List_2(_plusSign_2(_n, 1), 0);
        let _l = 0;
        let _answer = [];
        let _k = null;
        /* Statements */
        _toDo_3(
          0,
          _hyphenMinus_2(_n, 1),
          sl.annotateFunction(function (_i) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _i";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _lo = 1;
            let _hi = _l;
            let _z = null;
            /* Statements */
            _whileTrue_2(
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _lessThanSignEqualsSign_2(_lo, _hi);
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Temporaries */
                let _mid = _ceiling_1(_solidus_2(_plusSign_2(_lo, _hi), 2));
                /* Statements */
                return _if_3(
                  _lessThanSign_2(
                    _at_2(_x, _plusSign_2(_at_2(_m, _plusSign_2(_mid, 1)), 1)),
                    _at_2(_x, _plusSign_2(_i, 1)),
                  ),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _lo = _plusSign_2(_mid, 1);
                  }, []),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _hi = _hyphenMinus_2(_mid, 1);
                  }, []),
                );
              }, []),
            );
            _z = _lo;
            _atPut_3(_p, _plusSign_2(_i, 1), _at_2(_m, _z));
            _atPut_3(_m, _plusSign_2(_z, 1), _i);
            return _ifTrue_2(
              _greaterThanSign_2(_z, _l),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _l = _z;
              }, []),
            );
          }, ["i"]),
        );
        _k = _at_2(_m, _plusSign_2(_l, 1));
        _timesRepeat_2(
          _l,
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _addFirst_2(_answer, _at_2(_x, _plusSign_2(_k, 1)));
            return _k = _at_2(_p, _plusSign_2(_k, 1));
          }, []),
        );
        return _answer;
      }, []),
    );
  }, ["self"]),
  "{ :self | let x = self; let n = size(x); if((<(n, 2)), { x }, { let p = List(n, 0); let m = List(+(n, 1), 0); let l = 0; let answer = []; let k = nil; toDo(0, -(n, 1), { :i | let lo = 1; let hi = l; let z = nil; whileTrue({ <=(lo, hi) }, { let mid = ceiling((/((+(lo, hi)), 2))); if((<(at(x, +(at(m, +(mid, 1)), 1)), at(x, +(i, 1)))), { lo := +(mid, 1) }, { hi := -(mid, 1) }) }); z := lo; atPut(p, +(i, 1), at(m, z)); atPut(m, +(z, 1), i); ifTrue((>(z, l)), { l := z }) }); k := at(m, +(l, 1)); timesRepeat(l, { addFirst(answer,at(x, +(k, 1))); k := at(p, +(k, 1)) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "longestIncreasingSubsequenceList",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a = _sort_2(
      _increasingSubsequenceList_1(_self),
      sl.annotateFunction(function (_p, _q) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _p, _q";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSign_2(_size_1(_q), _size_1(_p));
      }, ["p", "q"]),
    );
    let _k = _size_1(_first_1(_a));
    /* Statements */
    return _reverse_1(_takeWhile_2(
      _a,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _equalsSign_2(_size_1(_each), _k);
      }, ["each"]),
    ));
  }, ["self"]),
  "{ :self | let a = sort(increasingSubsequenceList(self), { :p :q | <(size(q), size(p)) }); let k = size(first(a)); reverse(takeWhile(a, { :each | =(size(each), k) })) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lyndonWordsDo",
  ["alphabet", "n", "aBlock:/1"],
  sl.annotateFunction(function (_alphabet, _n, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _alphabet, _n, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _nextWord_1 = sl.annotateFunction(function (_w) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _w";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _k = _plusSign_2(_solidusSolidus_2(_n, _size_1(_w)), 1);
      let _x = _first_2(
        _catenate_1(_exclamationMark_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _w;
          }, []),
          _k,
        )),
        _n,
      );
      /* Statements */
      _whileTrue_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _ampersand_2(
            _greaterThanSign_2(_size_1(_x), 0),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _equalsSign_2(_last_1(_x), _last_1(_alphabet));
            }, []),
          );
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _removeLast_1(_x);
        }, []),
      );
      _ifFalse_2(
        _isEmpty_1(_x),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _i = _plusSign_2(_indexOf_2(_alphabet, _last_1(_x)), 1);
          /* Statements */
          return _atPut_3(_x, _size_1(_x), _at_2(_alphabet, _i));
        }, []),
      );
      return _x;
    }, ["w"]);
    let _w = [_first_1(_alphabet)];
    /* Statements */
    return _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersand_2(
          _greaterThanSign_2(_size_1(_w), 0),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _lessThanSignEqualsSign_2(_size_1(_w), _n);
          }, []),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _aBlock_1(_w);
        return _w = _nextWord_1(_w);
      }, []),
    );
  }, ["alphabet", "n", "aBlock:/1"]),
  "{ :alphabet :n :aBlock:/1 | let nextWord = { :w | let k = +((//(n, size(w))), 1); let x = first(catenate((!({ w }, k))),n); whileTrue({ &(>(size(x), 0), { =(last(x), last(alphabet)) }) }, { removeLast(x) }); ifFalse(isEmpty(x), { let i = +(indexOf(alphabet,last(x)), 1); atPut(x, size(x), at(alphabet, i)) }); x }; let w = [first(alphabet)]; whileTrue({ &(>(size(w), 0), { <=(size(w), n) }) }, { aBlock(w); w := nextWord(w) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "lyndonWords",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _lyndonWordsDo_3(
      _self,
      _anInteger,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
    );
    return _answer;
  }, ["self", "anInteger"]),
  "{ :self :anInteger | let answer = []; lyndonWordsDo(self, anInteger, { :each | add(answer,each) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "manhattanDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _sum_1(_abs_1(_hyphenMinus_2(_self, _aList)));
  }, ["self", "aList"]),
  "{ :self :aList | sum(abs((-(self, aList)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "median",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _median_1(_asSortedList_1(_self));
  }, ["self"]),
  "{ :self | median(asSortedList(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeInPlace",
  ["self", "select:/1", "insert:/2"],
  sl.annotateFunction(function (_self, _select_1, _insert_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _select_1, _insert_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _whileFalse_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _isEmpty_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _x = _collect_2(_self, _first_1);
        let _i = _indexOf_2(_x, _select_1(_x));
        /* Statements */
        _insert_2(_answer, _removeFirst_1(_at_2(_self, _i)));
        return _ifTrue_2(
          _isEmpty_1(_at_2(_self, _i)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _removeAt_2(_self, _i);
          }, []),
        );
      }, []),
    );
    return _answer;
  }, ["self", "select:/1", "insert:/2"]),
  "{ :self :select:/1 :insert:/2 | let answer = []; whileFalse({ isEmpty(self) }, { let x = collect(self,first:/1); let i = indexOf(x,select(x)); insert(answer,removeFirst(at(self, i))); ifTrue(isEmpty(at(self, i)), { removeAt(self,i) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeFirstMiddleLastIntoBy",
  ["self", "first", "middle", "last", "destination", "aBlock:/2"],
  sl.annotateFunction(
    function (_self, _first, _middle, _last, _destination, _aBlock_2) {
      /* ArityCheck */
      if (arguments.length !== 6) {
        const errorMessage =
          "Arity: expected 6, _self, _first, _middle, _last, _destination, _aBlock_2";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i1 = _first;
      let _i2 = _plusSign_2(_middle, 1);
      let _val1 = _at_2(_self, _i1);
      let _val2 = _at_2(_self, _i2);
      let _out = _hyphenMinus_2(_first, 1);
      /* Statements */
      _whileTrue_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _ampersand_2(
            _lessThanSignEqualsSign_2(_i1, _middle),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _lessThanSignEqualsSign_2(_i2, _last);
            }, []),
          );
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _if_3(
            _aBlock_2(_val1, _val2),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              _out = _plusSign_2(_out, 1);
              _i1 = _plusSign_2(_i1, 1);
              _atPut_3(_destination, _out, _val1);
              return _val1 = _at_2(_self, _i1);
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              _out = _plusSign_2(_out, 1);
              _i2 = _plusSign_2(_i2, 1);
              _atPut_3(_destination, _out, _val2);
              return _val2 = _atWrap_2(_self, _i2);
            }, []),
          );
        }, []),
      );
      return _if_3(
        _lessThanSignEqualsSign_2(_i1, _middle),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _replaceFromToWithStartingAt_5(
            _destination,
            _plusSign_2(_out, 1),
            _last,
            _self,
            _i1,
          );
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _replaceFromToWithStartingAt_5(
            _destination,
            _plusSign_2(_out, 1),
            _last,
            _self,
            _i2,
          );
        }, []),
      );
    },
    ["self", "first", "middle", "last", "destination", "aBlock:/2"],
  ),
  "{ :self :first :middle :last :destination :aBlock:/2 | let i1 = first; let i2 = +(middle, 1); let val1 = at(self, i1); let val2 = at(self, i2); let out = -(first, 1); whileTrue({ &(<=(i1, middle), { <=(i2, last) }) }, { if(aBlock(val1, val2), { out := +(out, 1); i1 := +(i1, 1); atPut(destination, out, val1); val1 := at(self, i1) }, { out := +(out, 1); i2 := +(i2, 1); atPut(destination, out, val2); val2 := atWrap(self,i2) }) }); if((<=(i1, middle)), { replaceFromToWithStartingAt(destination,+(out, 1), last, self, i1) }, { replaceFromToWithStartingAt(destination,+(out, 1), last, self, i2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeSortFromToBy",
  ["self", "startIndex", "stopIndex", "aBlock:/2"],
  sl.annotateFunction(function (_self, _startIndex, _stopIndex, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _startIndex, _stopIndex, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _size = _size_1(_self);
    /* Statements */
    return _if_3(
      _verticalLine_2(
        _lessThanSignEqualsSign_2(_size, 1),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _equalsSign_2(_startIndex, _stopIndex);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _ifFalse_2(
          _lessThanSignEqualsSign_2(1, _startIndex),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _errorSubscriptBounds_2(_self, _startIndex);
          }, []),
        );
        _ifFalse_2(
          _lessThanSignEqualsSign_2(_stopIndex, _size),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _errorSubscriptBounds_2(_self, _stopIndex);
          }, []),
        );
        _ifFalse_2(
          _lessThanSign_2(_startIndex, _stopIndex),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _errorSubscriptBounds_2(_self, _startIndex);
          }, []),
        );
        _mergeSortFromToIntoBy_5(
          _shallowCopy_1(_self),
          _startIndex,
          _stopIndex,
          _self,
          _aBlock_2,
        );
        return _self;
      }, []),
    );
  }, ["self", "startIndex", "stopIndex", "aBlock:/2"]),
  "{ :self :startIndex :stopIndex :aBlock:/2 | let size = size(self); if((|(<=(size, 1), { =(startIndex, stopIndex) })), { self }, { ifFalse((<=(1, startIndex)), { errorSubscriptBounds(self,startIndex) }); ifFalse((<=(stopIndex, size)), { errorSubscriptBounds(self,stopIndex) }); ifFalse((<(startIndex, stopIndex)), { errorSubscriptBounds(self,startIndex) }); mergeSortFromToIntoBy(shallowCopy(self),startIndex, stopIndex, self, aBlock:/2); self }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeSortFromToIntoBy",
  ["self", "firstIndex", "lastIndex", "destination", "aBlock:/2"],
  sl.annotateFunction(
    function (_self, _firstIndex, _lastIndex, _destination, _aBlock_2) {
      /* ArityCheck */
      if (arguments.length !== 5) {
        const errorMessage =
          "Arity: expected 5, _self, _firstIndex, _lastIndex, _destination, _aBlock_2";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _n = _hyphenMinus_2(_lastIndex, _firstIndex);
      /* Statements */
      return _if_3(
        _lessThanSignEqualsSign_2(_n, 1),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _if_3(
            _equalsSign_2(_n, 0),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _self;
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _firstObject = _at_2(_self, _firstIndex);
              let _lastObject = _at_2(_self, _lastIndex);
              /* Statements */
              _if_3(
                _aBlock_2(_firstObject, _lastObject),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  _atPut_3(_destination, _lastIndex, _lastObject);
                  return _atPut_3(_destination, _firstIndex, _firstObject);
                }, []),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  _atPut_3(_destination, _lastIndex, _firstObject);
                  return _atPut_3(_destination, _firstIndex, _lastObject);
                }, []),
              );
              return _self;
            }, []),
          );
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          _n = _solidusSolidus_2(_plusSign_2(_firstIndex, _lastIndex), 2);
          _mergeSortFromToIntoBy_5(
            _destination,
            _firstIndex,
            _n,
            _self,
            _aBlock_2,
          );
          _mergeSortFromToIntoBy_5(
            _destination,
            _plusSign_2(_n, 1),
            _lastIndex,
            _self,
            _aBlock_2,
          );
          return _mergeFirstMiddleLastIntoBy_6(
            _self,
            _firstIndex,
            _n,
            _lastIndex,
            _destination,
            _aBlock_2,
          );
        }, []),
      );
    },
    ["self", "firstIndex", "lastIndex", "destination", "aBlock:/2"],
  ),
  "{ :self :firstIndex :lastIndex :destination :aBlock:/2 | let n = -(lastIndex, firstIndex); if((<=(n, 1)), { if((=(n, 0)), { self }, { let firstObject = at(self, firstIndex); let lastObject = at(self, lastIndex); if(aBlock(firstObject, lastObject), { atPut(destination, lastIndex, lastObject); atPut(destination, firstIndex, firstObject) }, { atPut(destination, lastIndex, firstObject); atPut(destination, firstIndex, lastObject) }); self }) }, { n := //(+(firstIndex, lastIndex), 2); mergeSortFromToIntoBy(destination,firstIndex, n, self, aBlock:/2); mergeSortFromToIntoBy(destination,+(n, 1), lastIndex, self, aBlock:/2); mergeFirstMiddleLastIntoBy(self,firstIndex, n, lastIndex, destination, aBlock:/2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeSortBy",
  ["self", "aSortBlock:/2"],
  sl.annotateFunction(function (_self, _aSortBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aSortBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _mergeSortFromToBy_4(_self, 1, _size_1(_self), _aSortBlock_2);
  }, ["self", "aSortBlock:/2"]),
  "{ :self :aSortBlock:/2 | mergeSortFromToBy(self,1, size(self), aSortBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mergeSort",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _mergeSortBy_2(_self, _lessThanSignEqualsSign_2);
  }, ["self"]),
  "{ :self | mergeSortBy(self,<=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "meshGrid",
  ["x", "y"],
  sl.annotateFunction(function (_x, _y) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _x, _y";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _m = _size_1(_y);
    let _n = _size_1(_x);
    /* Statements */
    return [
      _exclamationMark_2(_asList_1(_x), _m),
      _transposed_1(_exclamationMark_2(_asList_1(_y), _n)),
    ];
  }, ["x", "y"]),
  "{ :x :y | let m = size(y); let n = size(x); [!(asList(x), m), transposed((!(asList(y), n)))] }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "middle",
  ["self", "count"],
  sl.annotateFunction(function (_self, _count) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _count";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _i = _plusSign_2(_solidusSolidus_2(_size_1(_self), 2), 1);
    let _j = _solidusSolidus_2(_count, 2);
    let _k = _hyphenMinus_2(_i, _j);
    /* Statements */
    return _copyFromTo_3(_self, _k, _hyphenMinus_2(_plusSign_2(_k, _count), 1));
  }, ["self", "count"]),
  "{ :self :count | let i = +(//(size(self), 2), 1); let j = //(count, 2); let k = -(i, j); copyFromTo(self,k, -(+(k, count), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "middle",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _plusSign_2(_solidusSolidus_2(_size_1(_self), 2), 1));
  }, ["self"]),
  "{ :self | at(self, +(//(size(self), 2), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mirror",
  ["self", "m", "n"],
  sl.annotateFunction(function (_self, _m, _n) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _m, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _plusSignPlusSign_2(
      _self,
      _reversed_1(
        _copyFromTo_3(
          _self,
          _plusSign_2(_n, 1),
          _hyphenMinus_2(_size_1(_self), _m),
        ),
      ),
    );
  }, ["self", "m", "n"]),
  "{ :self :m :n | ++(self, reversed(copyFromTo(self,+(n, 1), -(size(self), m)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "mixedRadixDecode",
  ["self", "factors"],
  sl.annotateFunction(function (_self, _factors) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _factors";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = 0;
    let _base = 1;
    let _k = _min_2(_size_1(_factors), _size_1(_self));
    let _prefix = _hyphenMinus_2(_size_1(_self), _size_1(_factors));
    /* Statements */
    _ifTrue_2(
      _greaterThanSign_2(_prefix, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "@Sequenceable>>mixedRadixDecode: sequence too long",
        );
      }, []),
    );
    _do_2(
      _upOrDownTo_2(_k, 1),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        _answer = _plusSign_2(
          _answer,
          _asterisk_2(_at_2(_self, _plusSign_2(_index, _prefix)), _base),
        );
        return _base = _asterisk_2(_base, _at_2(_factors, _index));
      }, ["index"]),
    );
    _ifTrue_2(
      _equalsSign_2(_prefix, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _answer = _plusSign_2(
          _answer,
          _asterisk_2(_at_2(_self, 1), _base),
        );
      }, []),
    );
    return _answer;
  }, ["self", "factors"]),
  "{ :self :factors | let answer = 0; let base = 1; let k = min(size(factors),size(self)); let prefix = -(size(self), size(factors)); ifTrue((>(prefix, 1)), { error(self,'@Sequenceable>>mixedRadixDecode: sequence too long') }); do(upOrDownTo(k, 1), { :index | answer := +(answer, (*(at(self, +(index, prefix)), base))); base := *(base, at(factors, index)) }); ifTrue((=(prefix, 1)), { answer := +(answer, (*(at(self, 1), base))) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "movingAverage",
  ["self", "windowSize"],
  sl.annotateFunction(function (_self, _windowSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _windowSize";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _toDo_3(
      1,
      _plusSign_2(_hyphenMinus_2(_size_1(_self), _windowSize), 1),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _n = 0;
        /* Statements */
        _toDo_3(
          _i,
          _hyphenMinus_2(_plusSign_2(_i, _windowSize), 1),
          sl.annotateFunction(function (_j) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _n = _plusSign_2(_n, _at_2(_self, _j));
          }, ["j"]),
        );
        return _add_2(_answer, _solidus_2(_n, _windowSize));
      }, ["i"]),
    );
    return _answer;
  }, ["self", "windowSize"]),
  "{ :self :windowSize | let answer = []; toDo(1, +(-(size(self), windowSize), 1), { :i | let n = 0; toDo(i, -(+(i, windowSize), 1), { :j | n := +(n, at(self, j)) }); add(answer,/(n, windowSize)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "movingMedian",
  ["self", "windowSize"],
  sl.annotateFunction(function (_self, _windowSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _windowSize";
      throw new Error(errorMessage);
    } /* Statements */
    return _movingMap_3(_median_1, _self, _windowSize);
  }, ["self", "windowSize"]),
  "{ :self :windowSize | movingMap(median:/1,self, windowSize) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "norm",
  ["self", "p"],
  sl.annotateFunction(function (_self, _p) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _p";
      throw new Error(errorMessage);
    } /* Statements */
    return _circumflexAccent_2(
      _sum_1(_circumflexAccent_2(_abs_1(_self), _p)),
      _solidus_2(1, _p),
    );
  }, ["self", "p"]),
  "{ :self :p | ^(sum((^(abs(self), p))), (/(1, p))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "norm",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _sqrt_1(_scalarProduct_2(_self, _conjugated_1(_self)));
  }, ["self"]),
  "{ :self | sqrt((scalarProduct(self,conjugated(self)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "normalize",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _norm_1(_self);
    /* Statements */
    return _if_3(
      _isVeryCloseTo_2(_n, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _solidus_2(_self, _n);
      }, []),
    );
  }, ["self"]),
  "{ :self | let n = norm(self); if(isVeryCloseTo(n,0), { self }, { /(self, n) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "normalizedSquaredEuclideanDistance",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _uu = _hyphenMinus_2(_u, _mean_1(_u));
    let _vv = _hyphenMinus_2(_v, _mean_1(_v));
    /* Statements */
    return _solidus_2(
      _asterisk_2(0.5, _squared_1(_norm_1(_hyphenMinus_2(_uu, _vv)))),
      _plusSign_2(_squared_1(_norm_1(_uu)), _squared_1(_norm_1(_vv))),
    );
  }, ["u", "v"]),
  "{ :u :v | let uu = (-(u, mean(u))); let vv = (-(v, mean(v))); /(*(0.5, squared(norm((-(uu, vv))))), (+(squared(norm(uu)), squared(norm(vv))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "nubCumulatively",
  ["self", "compareBlock:/2"],
  sl.annotateFunction(function (_self, _compareBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _compareBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _seen = [];
    /* Statements */
    return _collect_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _select_2(
          _each,
          sl.annotateFunction(function (_item) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _item";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _includesBy_3(_seen, _item, _compareBlock_2),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return false;
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _add_2(_seen, _item);
                return true;
              }, []),
            );
          }, ["item"]),
        );
      }, ["each"]),
    );
  }, ["self", "compareBlock:/2"]),
  "{ :self :compareBlock:/2 | let seen = []; collect(self, { :each | select(each, { :item | if(includesBy(seen,item, compareBlock:/2), { false }, { add(seen,item); true }) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "nubCumulatively",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _nubCumulatively_2(_self, _equalsSign_2);
  }, ["self"]),
  "{ :self | nubCumulatively(self,=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "nubSieve",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _seen = [];
    /* Statements */
    return _collect_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _includes_2(_seen, _each),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return false;
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _add_2(_seen, _each);
            return true;
          }, []),
        );
      }, ["each"]),
    );
  }, ["self"]),
  "{ :self | let seen = []; collect(self, { :each | if(includes(seen,each), { false }, { add(seen,each); true }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "numberCompose",
  ["c", "u"],
  sl.annotateFunction(function (_c, _u) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _c, _u";
      throw new Error(errorMessage);
    } /* Statements */
    return _sum_1(_asterisk_2(_c, _last_2(_u, _size_1(_c))));
  }, ["c", "u"]),
  "{ :c :u | sum((*(c, last(u,size(c))))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "occurrencesOf",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _tally = 0;
    /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _equalsSign_2(_at_2(_self, _index), _anObject),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _tally = _plusSign_2(_tally, 1);
          }, []),
        );
      }, ["index"]),
    );
    return _tally;
  }, ["self", "anObject"]),
  "{ :self :anObject | let tally = 0; indicesDo(self, { :index | ifTrue((=(at(self, index), anObject)), { tally := +(tally, 1) }) }); tally }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "ordering",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(_sortedWithIndices_1(_self), _value_1);
  }, ["self"]),
  "{ :self | collect(sortedWithIndices(self),value:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "ordering",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(_sortedWithIndices_2(_self, _aBlock_2), _value_1);
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | collect(sortedWithIndices(self,aBlock:/2),value:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "orderedSubstrings",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = [];
        let _run = [_first_1(_self)];
        /* Statements */
        _toDo_3(
          2,
          _size_1(_self),
          sl.annotateFunction(function (_i) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _i";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _item = _at_2(_self, _i);
            /* Statements */
            return _if_3(
              _aBlock_2(_at_2(_self, _hyphenMinus_2(_i, 1)), _item),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _add_2(_run, _item);
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _add_2(_answer, _copy_1(_run));
                _removeAll_1(_run);
                return _add_2(_run, _item);
              }, []),
            );
          }, ["i"]),
        );
        _add_2(_answer, _run);
        return _answer;
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | if(isEmpty(self), { [] }, { let answer = []; let run = [first(self)]; toDo(2, size(self), { :i | let item = at(self, i); if(aBlock(at(self, -(i, 1)), item), { add(run,item) }, { add(answer,copy(run)); removeAll(run); add(run,item) }) }); add(answer,run); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "orderedSubstrings",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _orderedSubstrings_2(_self, _lessThanSign_2);
  }, ["self"]),
  "{ :self | orderedSubstrings(self,<) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "outerProduct",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return (_outer_1(_asterisk_2)(_self, _aList));
  }, ["self", "aList"]),
  "{ :self :aList | (outer(*) . (self, aList)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "padLeftAndRight",
  ["self", "anInteger", "anObject"],
  sl.annotateFunction(function (_self, _anInteger, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anInteger, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSignEqualsSign_2(_size_1(_self), _anInteger),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _i = _hyphenMinus_2(_anInteger, _size_1(_self));
        let _j = _solidusSolidus_2(_i, 2);
        let _k = _hyphenMinus_2(_i, _j);
        /* Statements */
        return _plusSignPlusSign_2(
          _plusSignPlusSign_2(_numberSign_2(_anObject, _j), _self),
          _numberSign_2(_anObject, _k),
        );
      }, []),
    );
  }, ["self", "anInteger", "anObject"]),
  "{ :self :anInteger :anObject | if((>=(size(self), anInteger)), { self }, { let i = -(anInteger, size(self)); let j = //(i, 2); let k = -(i, j); ++(++((#(anObject, j)), self), (#(anObject, k))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "pairsCollect",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _upOrDownTo_2(1, _solidusSolidus_2(_size_1(_self), 2)),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(
          _at_2(_self, _hyphenMinus_2(_asterisk_2(2, _index), 1)),
          _at_2(_self, _asterisk_2(2, _index)),
        );
      }, ["index"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | collect(upOrDownTo(1, //(size(self), 2)), { :index | aBlock(at(self, -(*(2, index), 1)), at(self, *(2, index))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "pairsDo",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      1,
      _solidusSolidus_2(_size_1(_self), 2),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(
          _at_2(_self, _hyphenMinus_2(_asterisk_2(2, _index), 1)),
          _at_2(_self, _asterisk_2(2, _index)),
        );
      }, ["index"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | toDo(1, //(size(self), 2), { :index | aBlock(at(self, -(*(2, index), 1)), at(self, *(2, index))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "pairwiseSum",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_n, 128),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _sum_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _m = _floor_1(_solidus_2(_n, 2));
        /* Statements */
        return _plusSign_2(
          _pairwiseSum_1(_sliceFromTo_3(_self, 1, _m)),
          _pairwiseSum_1(_sliceFromTo_3(_self, _plusSign_2(_m, 1), _n)),
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | let n = size(self); if((<=(n, 128)), { sum(self) }, { let m = floor((/(n, 2))); +(pairwiseSum(sliceFromTo(self,1, m)), pairwiseSum(sliceFromTo(self,+(m, 1), n))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "partitionCollect",
  ["self", "windowSize", "stepSize", "aBlock:/1"],
  sl.annotateFunction(function (_self, _windowSize, _stepSize, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _windowSize, _stepSize, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _partitionDo_4(
      _self,
      _windowSize,
      _stepSize,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _aBlock_1(_each));
      }, ["each"]),
    );
    return _answer;
  }, ["self", "windowSize", "stepSize", "aBlock:/1"]),
  "{ :self :windowSize :stepSize :aBlock:/1 | let answer = []; partitionDo(self, windowSize, stepSize, { :each | add(answer,aBlock(each)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "partitionDo",
  ["self", "windowSize", "stepSize", "aBlock:/1"],
  sl.annotateFunction(function (_self, _windowSize, _stepSize, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _windowSize, _stepSize, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifTrue_2(
      _lessThanSignEqualsSign_2(_windowSize, _size_1(_self)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _equalsSign_2(_windowSize, 0),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_1([]);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _answer = _List_1(_windowSize);
            /* Statements */
            return _toByDo_4(
              1,
              _plusSign_2(_hyphenMinus_2(_size_1(_self), _windowSize), 1),
              _stepSize,
              sl.annotateFunction(function (_index) {
                /* ArityCheck */
                if (arguments.length !== 1) {
                  const errorMessage = "Arity: expected 1, _index";
                  throw new Error(errorMessage);
                } /* Statements */
                _copyFromToInto_4(
                  _self,
                  _index,
                  _hyphenMinus_2(_plusSign_2(_index, _windowSize), 1),
                  _answer,
                );
                return _aBlock_1(_answer);
              }, ["index"]),
            );
          }, []),
        );
      }, []),
    );
  }, ["self", "windowSize", "stepSize", "aBlock:/1"]),
  "{ :self :windowSize :stepSize :aBlock:/1 | ifTrue((<=(windowSize, size(self))), { if((=(windowSize, 0)), { aBlock([]) }, { let answer = List(windowSize); toByDo(1, +(-(size(self), windowSize), 1), stepSize, { :index | copyFromToInto(self,index, -(+(index, windowSize), 1), answer); aBlock(answer) }) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "partition",
  ["self", "windowSize", "stepSize"],
  sl.annotateFunction(function (_self, _windowSize, _stepSize) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _windowSize, _stepSize";
      throw new Error(errorMessage);
    } /* Statements */
    return _partitionCollect_4(_self, _windowSize, _stepSize, _copy_1);
  }, ["self", "windowSize", "stepSize"]),
  "{ :self :windowSize :stepSize | partitionCollect(self,windowSize, stepSize, copy:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "partition",
  ["self", "windowSize"],
  sl.annotateFunction(function (_self, _windowSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _windowSize";
      throw new Error(errorMessage);
    } /* Statements */
    return _partition_3(_self, _windowSize, _windowSize);
  }, ["self", "windowSize"]),
  "{ :self :windowSize | partition(self,windowSize, windowSize) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "patienceSortPiles",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _piles = [];
    let _answer = [];
    /* Statements */
    _do_2(
      _self,
      sl.annotateFunction(function (_card) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _card";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _index = _detectIndex_2(
          _piles,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _greaterThanSignEqualsSign_2(_last_1(_each), _card);
          }, ["each"]),
        );
        /* Statements */
        return _ifNil_3(
          _index,
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _addLast_2(_piles, [_card]);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _addLast_2(_at_2(_piles, _index), _card);
          }, []),
        );
      }, ["card"]),
    );
    return _piles;
  }, ["self"]),
  "{ :self | let piles = []; let answer = []; do(self, { :card | let index = detectIndex(piles, { :each | >=(last(each), card) }); ifNil(index, { addLast(piles,[card]) }, { addLast(at(piles, index),card) }) }); piles }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "patienceSort",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _mergeInPlace_3(_patienceSortPiles_1(_self), _max_1, _addFirst_2);
  }, ["self"]),
  "{ :self | mergeInPlace(patienceSortPiles(self),max:/1, addFirst:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "pick",
  ["self", "aList", "anObject"],
  sl.annotateFunction(function (_self, _aList, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_depth_1(_self), 2),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withCollect_3(
          _self,
          _aList,
          sl.annotateFunction(function (_i, _j) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _i, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _pick_3(_i, _j, _anObject);
          }, ["i", "j"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = [];
        /* Statements */
        _withDo_3(
          _self,
          _aList,
          sl.annotateFunction(function (_i, _j) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _i, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _ifTrue_2(
              _equalsSign_2(_j, _anObject),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _add_2(_answer, _i);
              }, []),
            );
          }, ["i", "j"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "aList", "anObject"]),
  "{ :self :aList :anObject | if((>(depth(self), 2)), { withCollect(self, aList, { :i :j | pick(i,j, anObject) }) }, { let answer = []; withDo(self, aList, { :i :j | ifTrue((=(j, anObject)), { add(answer,i) }) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "pinnedIndex",
  ["self", "index"],
  sl.annotateFunction(function (_self, _index) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _index";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_index, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return 1;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _greaterThanSign_2(_index, _size_1(_self)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _size_1(_self);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _index;
          }, []),
        );
      }, []),
    );
  }, ["self", "index"]),
  "{ :self :index | if((<(index, 1)), { 1 }, { if((>(index, size(self))), { size(self) }, { index }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "precedes",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _equalsSign_2(_compare_2(_self, _aList), -1);
  }, ["self", "aList"]),
  "{ :self :aList | =(compare(self,aList), -1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "precedesOrEqualTo",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _tildeEqualsSign_2(_compare_2(_self, _aList), 1);
  }, ["self", "aList"]),
  "{ :self :aList | ~=(compare(self,aList), 1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "prefixesDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      1,
      _size_1(_self),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_copyFromTo_3(_self, 1, _each));
      }, ["each"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toDo(1, size(self), { :each | aBlock(copyFromTo(self,1, each)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "prefixes",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _prefixesDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
    );
    return _answer;
  }, ["self"]),
  "{ :self | let answer = []; prefixesDo(self, { :each | add(answer,each) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "prefixProduct",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _scan_2(_self, _asterisk_2);
  }, ["self"]),
  "{ :self | scan(self,*) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "prefixSum",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _scan_2(_self, _plusSign_2);
  }, ["self"]),
  "{ :self | scan(self,+) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "projection",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _w = _conjugated_1(_v);
    /* Statements */
    return _asterisk_2(_solidus_2(_dot_2(_u, _w), _dot_2(_v, _w)), _v);
  }, ["u", "v"]),
  "{ :u :v | let w = conjugated(v); *(/(dot(u,w), dot(v,w)), v) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "quickSortFromToBy",
  ["self", "from", "to", "sortBlock:/2"],
  sl.annotateFunction(function (_self, _from, _to, _sortBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage = "Arity: expected 4, _self, _from, _to, _sortBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _i, _j, _k, _l, _ij, _di, _dj, _dij, _n, _tmp;
      /* Statements */
      _i = _from;
      _j = _to;
      return _repeat_1(sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _di = _at_2(_self, _i);
        _dj = _at_2(_self, _j);
        _ifFalse_2(
          _sortBlock_2(_di, _dj),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _atPut_3(_self, _i, _dj);
            _atPut_3(_self, _j, _di);
            _tmp = _dj;
            _dj = _di;
            return _di = _tmp;
          }, []),
        );
        _n = _hyphenMinus_2(_plusSign_2(_j, 1), _i);
        _ifTrue_2(
          _lessThanSignEqualsSign_2(_n, 2),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _return_1(_self);
          }, []),
        );
        _ij = _solidusSolidus_2(_plusSign_2(_i, _j), 2);
        _dij = _at_2(_self, _ij);
        _if_3(
          _sortBlock_2(_di, _dij),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _ifFalse_2(
              _sortBlock_2(_dij, _dj),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _atPut_3(_self, _j, _dij);
                _atPut_3(_self, _ij, _dj);
                _dij = _dj;
                return _dj = null;
              }, []),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _atPut_3(_self, _i, _dij);
            _atPut_3(_self, _ij, _di);
            _dij = _di;
            return _di = null;
          }, []),
        );
        _ifTrue_2(
          _equalsSign_2(_n, 3),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _return_1(_self);
          }, []),
        );
        _k = _i;
        _l = _j;
        _whileTrue_2(
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _whileTrue_1(sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              _l = _hyphenMinus_2(_l, 1);
              return _ampersand_2(
                _lessThanSignEqualsSign_2(_k, _l),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _sortBlock_2(_dij, _at_2(_self, _l));
                }, []),
              );
            }, []));
            _whileTrue_1(sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              _k = _plusSign_2(_k, 1);
              return _ampersand_2(
                _lessThanSignEqualsSign_2(_k, _l),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _sortBlock_2(_at_2(_self, _k), _dij);
                }, []),
              );
            }, []));
            return _lessThanSignEqualsSign_2(_k, _l);
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _tmp = _at_2(_self, _k);
            _atPut_3(_self, _k, _at_2(_self, _l));
            return _atPut_3(_self, _l, _tmp);
          }, []),
        );
        return _if_3(
          _lessThanSign_2(_i, _l),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _lessThanSign_2(_k, _j),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _if_3(
                  _lessThanSign_2(
                    _hyphenMinus_2(_l, _i),
                    _hyphenMinus_2(_j, _k),
                  ),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    _quickSortFromToBy_4(_self, _i, _l, _sortBlock_2);
                    return _i = _k;
                  }, []),
                  sl.annotateFunction(function () {
                    /* ArityCheck */
                    if (arguments.length !== 0) {
                      const errorMessage = "Arity: expected 0, ";
                      throw new Error(errorMessage);
                    } /* Statements */
                    _quickSortFromToBy_4(_self, _k, _j, _sortBlock_2);
                    return _j = _l;
                  }, []),
                );
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _j = _l;
              }, []),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _if_3(
              _lessThanSign_2(_k, _j),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _i = _k;
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _return_1(_self);
              }, []),
            );
          }, []),
        );
      }, []));
    }, ["return:/1"]));
  }, ["self", "from", "to", "sortBlock:/2"]),
  "{ :self :from :to :sortBlock:/2 | valueWithReturn({ :return:/1 | var i, j, k, l, ij, di, dj, dij, n, tmp;i := from; j := to; repeat({ di := at(self, i); dj := at(self, j); ifFalse(sortBlock(di, dj), { atPut(self, i, dj); atPut(self, j, di); tmp := dj; dj := di; di := tmp }); n := -(+(j, 1), i); ifTrue((<=(n, 2)), { return(self) }); ij := //(+(i, j), 2); dij := at(self, ij); if(sortBlock(di, dij), { ifFalse(sortBlock(dij, dj), { atPut(self, j, dij); atPut(self, ij, dj); dij := dj; dj := nil }) }, { atPut(self, i, dij); atPut(self, ij, di); dij := di; di := nil }); ifTrue((=(n, 3)), { return(self) }); k := i; l := j; whileTrue({ whileTrue({ l := -(l, 1); &(<=(k, l), { sortBlock(dij, at(self, l)) }) }); whileTrue({ k := +(k, 1); &(<=(k, l), { sortBlock(at(self, k), dij) }) }); <=(k, l) }, { tmp := at(self, k); atPut(self, k, at(self, l)); atPut(self, l, tmp) }); if((<(i, l)), { if((<(k, j)), { if((<(-(l, i), (-(j, k)))), { quickSortFromToBy(self,i, l, sortBlock:/2); i := k }, { quickSortFromToBy(self,k, j, sortBlock:/2); j := l }) }, { j := l }) }, { if((<(k, j)), { i := k }, { return(self) }) }) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "quickSortBy",
  ["self", "sortBlock:/2"],
  sl.annotateFunction(function (_self, _sortBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _sortBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _quickSortFromToBy_4(_self, 1, _size_1(_self), _sortBlock_2);
  }, ["self", "sortBlock:/2"]),
  "{ :self :sortBlock:/2 | quickSortFromToBy(self,1, size(self), sortBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "quickSort",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _quickSortBy_2(_self, _lessThanSignEqualsSign_2);
  }, ["self"]),
  "{ :self | quickSortBy(self,<=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "ratios",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _partition_3(_self, 2, 1),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _solidus_2(_at_2(_each, 2), _at_2(_each, 1));
      }, ["each"]),
    );
  }, ["self"]),
  "{ :self | collect(partition(self,2, 1), { :each | /(at(each, 2), at(each, 1)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replace",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(_self, _index, _aBlock_1(_at_2(_self, _index)));
      }, ["index"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | indicesDo(self, { :index | atPut(self, index, aBlock(at(self, index))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replaceAllWith",
  ["self", "oldObject", "newObject"],
  sl.annotateFunction(function (_self, _oldObject, _newObject) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _oldObject, _newObject";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _index = 0;
    /* Statements */
    _whileFalse_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _index = _indexOfStartingAtBy_4(
          _self,
          _oldObject,
          _plusSign_2(_index, 1),
          _equalsSign_2,
        );
        return _equalsSign_2(_index, 0);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(_self, _index, _newObject);
      }, []),
    );
    return _self;
  }, ["self", "oldObject", "newObject"]),
  "{ :self :oldObject :newObject | let index = 0; whileFalse({ index := indexOfStartingAtBy(self,oldObject, +(index, 1), =); =(index, 0) }, { atPut(self, index, newObject) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replaceFromToWith",
  ["self", "start", "stop", "replacement"],
  sl.annotateFunction(function (_self, _start, _stop, _replacement) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _start, _stop, _replacement";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _equalsSign_2(
        _size_1(_replacement),
        _plusSign_2(_hyphenMinus_2(_stop, _start), 1),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _replaceFromToWithStartingAt_5(
          _self,
          _start,
          _stop,
          _replacement,
          1,
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "@Sequenceable>> replaceFromToWith: size of replacement does not match",
        );
      }, []),
    );
  }, ["self", "start", "stop", "replacement"]),
  "{ :self :start :stop :replacement | if((=(size(replacement), (+(-(stop, start), 1)))), { replaceFromToWithStartingAt(self,start, stop, replacement, 1) }, { error(self,'@Sequenceable>> replaceFromToWith: size of replacement does not match') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replaceFromToWithStartingAt",
  ["self", "start", "stop", "replacement", "replacementStart"],
  sl.annotateFunction(
    function (_self, _start, _stop, _replacement, _replacementStart) {
      /* ArityCheck */
      if (arguments.length !== 5) {
        const errorMessage =
          "Arity: expected 5, _self, _start, _stop, _replacement, _replacementStart";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _replacementOffset = _hyphenMinus_2(_replacementStart, _start);
      let _index = _start;
      /* Statements */
      _whileTrue_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _lessThanSignEqualsSign_2(_index, _stop);
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          _atPut_3(
            _self,
            _index,
            _at_2(_replacement, _plusSign_2(_replacementOffset, _index)),
          );
          return _index = _plusSign_2(_index, 1);
        }, []),
      );
      return _self;
    },
    ["self", "start", "stop", "replacement", "replacementStart"],
  ),
  "{ :self :start :stop :replacement :replacementStart | let replacementOffset = -(replacementStart, start); let index = start; whileTrue({ <=(index, stop) }, { atPut(self, index, at(replacement, +(replacementOffset, index))); index := +(index, 1) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replicateEachApplying",
  ["self", "counts", "aBlock:/1"],
  sl.annotateFunction(function (_self, _counts, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _counts, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _tildeEqualsSign_2(_size_1(_self), _size_1(_counts)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(
          _self,
          "@Sequenceable>>replicateEachApplying: counts not of correct size",
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answerSize = _sum_1(_counts);
        let _answer = _ofSize_2(_species_1(_self), _answerSize);
        let _answerIndex = 1;
        /* Statements */
        _do_2(
          _upOrDownTo_2(1, _size_1(_self)),
          sl.annotateFunction(function (_selfIndex) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _selfIndex";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _entry = _aBlock_1(_at_2(_self, _selfIndex));
            /* Statements */
            return _timesRepeat_2(
              _at_2(_counts, _selfIndex),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _atPut_3(_answer, _answerIndex, _entry);
                return _answerIndex = _plusSign_2(_answerIndex, 1);
              }, []),
            );
          }, ["selfIndex"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "counts", "aBlock:/1"]),
  "{ :self :counts :aBlock:/1 | if((~=(size(self), size(counts))), { error(self,'@Sequenceable>>replicateEachApplying: counts not of correct size') }, { let answerSize = sum(counts); let answer = ofSize(species(self),answerSize); let answerIndex = 1; do(upOrDownTo(1, size(self)), { :selfIndex | let entry = aBlock(at(self, selfIndex)); timesRepeat(at(counts, selfIndex), { atPut(answer, answerIndex, entry); answerIndex := +(answerIndex, 1) }) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "replicateEach",
  ["self", "counts"],
  sl.annotateFunction(function (_self, _counts) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _counts";
      throw new Error(errorMessage);
    } /* Statements */
    _ifTrue_2(
      _isInteger_1(_counts),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _counts = _numberSign_2(_counts, _size_1(_self));
      }, []),
    );
    return _replicateEachApplying_3(_self, _counts, _identity_1);
  }, ["self", "counts"]),
  "{ :self :counts | ifTrue(isInteger(counts), { counts := #(counts, size(self)) }); replicateEachApplying(self,counts, identity:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "reversed",
  ["self", "level"],
  sl.annotateFunction(function (_self, _level) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _level";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSignEqualsSign_2(_level, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _ofSize_2(_species_1(_self), _size_1(_self));
        let _fromIndex = _plusSign_2(_size_1(_self), 1);
        /* Statements */
        _indicesDo_2(
          _self,
          sl.annotateFunction(function (_toIndex) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _toIndex";
              throw new Error(errorMessage);
            } /* Statements */
            _atPut_3(
              _answer,
              _toIndex,
              _at_2(_self, _hyphenMinus_2(_fromIndex, 1)),
            );
            return _fromIndex = _hyphenMinus_2(_fromIndex, 1);
          }, ["toIndex"]),
        );
        return _answer;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _self,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _reversed_2(_each, _hyphenMinus_2(_level, 1));
          }, ["each"]),
        );
      }, []),
    );
  }, ["self", "level"]),
  "{ :self :level | if((<=(level, 1)), { let answer = ofSize(species(self),size(self)); let fromIndex = +(size(self), 1); indicesDo(self, { :toIndex | atPut(answer, toIndex, at(self, -(fromIndex, 1))); fromIndex := -(fromIndex, 1) }); answer }, { collect(self, { :each | reversed(each,-(level, 1)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "reversed",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _reversed_2(_self, 1);
  }, ["self"]),
  "{ :self | reversed(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "reverseDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toByDo_4(
      _size_1(_self),
      1,
      -1,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_at_2(_self, _index));
      }, ["index"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toByDo(size(self), 1, -1, { :index | aBlock(at(self, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "reverseWithDo",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _tildeEqualsSign_2(_size_1(_self), _size_1(_aList)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>> reverseWithDo: unequal size");
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _toByDo_4(
          _size_1(_self),
          1,
          -1,
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_2(_at_2(_self, _index), _at_2(_aList, _index));
          }, ["index"]),
        );
      }, []),
    );
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | if((~=(size(self), size(aList))), { error(self,'@Sequenceable>> reverseWithDo: unequal size') }, { toByDo(size(self), 1, -1, { :index | aBlock(at(self, index), at(aList, index)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "riffle",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _if_3(
          _isSequenceable_1(_anObject),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _k = _size_1(_self);
            /* Statements */
            return _if_3(
              _greaterThanSignEqualsSign_2(_size_1(_anObject), _k),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _error_2(_self, "riffle: too many items to insert");
              }, []),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Temporaries */
                let _answer = [];
                /* Statements */
                _toDo_3(
                  1,
                  _hyphenMinus_2(_k, 1),
                  sl.annotateFunction(function (_i) {
                    /* ArityCheck */
                    if (arguments.length !== 1) {
                      const errorMessage = "Arity: expected 1, _i";
                      throw new Error(errorMessage);
                    } /* Statements */
                    _add_2(_answer, _at_2(_self, _i));
                    return _add_2(_answer, _atWrap_2(_anObject, _i));
                  }, ["i"]),
                );
                _add_2(_answer, _last_1(_self));
                return _answer;
              }, []),
            );
          }, []),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _intersperse_2(_self, _anObject);
          }, []),
        );
      }, []),
    );
  }, ["self", "anObject"]),
  "{ :self :anObject | if(isEmpty(self), { [] }, { if(isSequenceable(anObject), { let k = size(self); if((>=(size(anObject), k)), { error(self,'riffle: too many items to insert') }, { let answer = []; toDo(1, -(k, 1), { :i | add(answer,at(self, i)); add(answer,atWrap(anObject,i)) }); add(answer,last(self)); answer }) }, { intersperse(self,anObject) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotated",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _rotatedRight_2(_self, _anInteger);
  }, ["self", "anInteger"]),
  "{ :self :anInteger | rotatedRight(self,anInteger) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotatedLeft",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _rotatedLeft_2(_self, 1);
  }, ["self"]),
  "{ :self | rotatedLeft(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotatedLeft",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      _plusSign_2(1, _anInteger),
      _plusSign_2(_size_1(_self), _anInteger),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atWrap_2(_self, _index);
      }, ["index"]),
    );
  }, ["self", "anInteger"]),
  "{ :self :anInteger | toAsCollect((+(1, anInteger)), +(size(self), anInteger), species(self), { :index | atWrap(self,index) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotatedRight",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _rotatedRight_2(_self, 1);
  }, ["self"]),
  "{ :self | rotatedRight(self,1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotatedRight",
  ["self", "anInteger"],
  sl.annotateFunction(function (_self, _anInteger) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anInteger";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      _hyphenMinus_2(1, _anInteger),
      _hyphenMinus_2(_size_1(_self), _anInteger),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atWrap_2(_self, _index);
      }, ["index"]),
    );
  }, ["self", "anInteger"]),
  "{ :self :anInteger | toAsCollect((-(1, anInteger)), -(size(self), anInteger), species(self), { :index | atWrap(self,index) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "rotateLeft",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    let _left = _first_1(_self);
    /* Statements */
    _toDo_3(
      2,
      _n,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(_self, _hyphenMinus_2(_i, 1), _at_2(_self, _i));
      }, ["i"]),
    );
    return _atPut_3(_self, _n, _z);
  }, ["self"]),
  "{ :self | let n = size(self); let left = first(self); toDo(2, n, { :i | atPut(self, -(i, 1), at(self, i)) }); atPut(self, n, z) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sattoloShuffle",
  ["self", "rng"],
  sl.annotateFunction(function (_self, _rng) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _rng";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _i = _size_1(_self);
    /* Statements */
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _greaterThanSign_2(_i, 1);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _j = _nextRandomInteger_3(_rng, 1, _hyphenMinus_2(_i, 1));
        /* Statements */
        _swapWith_3(_self, _i, _j);
        return _i = _hyphenMinus_2(_i, 1);
      }, []),
    );
    return _self;
  }, ["self", "rng"]),
  "{ :self :rng | let i = size(self); whileTrue({ >(i, 1) }, { let j = nextRandomInteger(rng,1, -(i, 1)); swapWith(self,i, j); i := -(i, 1) }); self }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sattoloShuffle",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _sattoloShuffle_2(_self, _system);
  }, ["self"]),
  "{ :self | sattoloShuffle(self,system) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "scalarProduct",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _sum_1(_e_1(_asterisk_2)(_self, _aList));
  }, ["self", "aList"]),
  "{ :self :aList | sum(((e(*) . (self, aList)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "scan",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _scanLeft_2(_self, _aBlock_2);
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | scanLeft(self,aBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "scanLeft",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _new_2(_species_1(_self), _size_1(_self));
        let _next = _at_2(_self, 1);
        /* Statements */
        _atPut_3(_answer, 1, _next);
        _toDo_3(
          2,
          _size_1(_self),
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            _next = _aBlock_2(_next, _at_2(_self, _index));
            return _atPut_3(_answer, _index, _next);
          }, ["index"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | ifEmpty(self, { copy(self) }, { let answer = new(species(self),size(self)); let next = at(self, 1); atPut(answer, 1, next); toDo(2, size(self), { :index | next := aBlock(next, at(self, index)); atPut(answer, index, next) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "scanLeftAssociatingRight",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _upOrDownTo_2(1, _size_1(_self)),
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _foldRightPrefix_3(_self, _each, _aBlock_2);
          }, ["each"]),
        );
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | ifEmpty(self, { copy(self) }, { collect(upOrDownTo(1, size(self)), { :each | foldRightPrefix(self,each, aBlock:/2) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "scanRight",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copy_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = _new_2(_species_1(_self), _size_1(_self));
        let _next = _at_2(_self, _size_1(_self));
        /* Statements */
        _atPut_3(_answer, _size_1(_self), _next);
        _toByDo_4(
          _hyphenMinus_2(_size_1(_self), 1),
          1,
          -1,
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Statements */
            _next = _aBlock_2(_at_2(_self, _index), _next);
            return _atPut_3(_answer, _index, _next);
          }, ["index"]),
        );
        return _answer;
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | ifEmpty(self, { copy(self) }, { let answer = new(species(self),size(self)); let next = at(self, size(self)); atPut(answer, size(self), next); toByDo((-(size(self), 1)), 1, -1, { :index | next := aBlock(at(self, index), next); atPut(answer, index, next) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "semiconvergents",
  ["self", "epsilon"],
  sl.annotateFunction(function (_self, _epsilon) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _epsilon";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    let _final = _fromContinuedFraction_1(_self);
    let _lastError = _final;
    /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _prefixesDo_2(
        _self,
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _z = _last_1(_each);
          let _h = _roundUpTo_2(_solidus_2(_z, 2), 1);
          let _p = _allButLast_1(_each);
          /* Statements */
          return _toDo_3(
            _h,
            _z,
            sl.annotateFunction(function (_k) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _k";
                throw new Error(errorMessage);
              } /* Temporaries */
              let _c = _plusSignPlusSign_2(_p, [_k]);
              let _r = _fromContinuedFraction_1(_c);
              let _nextError = _abs_1(_hyphenMinus_2(_final, _r));
              /* Statements */
              _ifTrue_2(
                _lessThanSign_2(_nextError, _lastError),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  _add_2(_answer, _r);
                  return _lastError = _nextError;
                }, []),
              );
              return _ifTrue_2(
                _lessThanSign_2(_abs_1(_hyphenMinus_2(_final, _r)), _epsilon),
                sl.annotateFunction(function () {
                  /* ArityCheck */
                  if (arguments.length !== 0) {
                    const errorMessage = "Arity: expected 0, ";
                    throw new Error(errorMessage);
                  } /* Statements */
                  return _return_1(_answer);
                }, []),
              );
            }, ["k"]),
          );
        }, ["each"]),
      );
      return _answer;
    }, ["return:/1"]));
  }, ["self", "epsilon"]),
  "{ :self :epsilon | let answer = []; let final = fromContinuedFraction(self); let lastError = final; valueWithReturn({ :return:/1 | prefixesDo(self, { :each | let z = last(each); let h = roundUpTo((/(z, 2)),1); let p = allButLast(each); toDo(h, z, { :k | let c = ++(p, [k]); let r = fromContinuedFraction(c); let nextError = abs((-(final, r))); ifTrue((<(nextError, lastError)), { add(answer,r); lastError := nextError }); ifTrue((<(abs((-(final, r))), epsilon)), { return(answer) }) }) }); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "shiftRegisterSequence",
  ["initialState", "taps", "count"],
  sl.annotateFunction(function (_initialState, _taps, _count) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _initialState, _taps, _count";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _register = _copy_1(_initialState);
    let _k = _size_1(_register);
    let _step_0 = sl.annotateFunction(function () {
      /* ArityCheck */
      if (arguments.length !== 0) {
        const errorMessage = "Arity: expected 0, ";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _bit = _percentSign_2(
        _reduce_2(
          _collect_2(
            _taps,
            sl.annotateFunction(function (_i) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _i";
                throw new Error(errorMessage);
              } /* Statements */
              return _at_2(_register, _hyphenMinus_2(_plusSign_2(_k, 1), _i));
            }, ["i"]),
          ),
          _plusSign_2,
        ),
        2,
      );
      /* Statements */
      _removeLast_1(_register);
      _addFirst_2(_register, _bit);
      return _bit;
    }, []);
    let _answer = [_first_1(_register)];
    /* Statements */
    _timesRepeat_2(
      _hyphenMinus_2(_count, 1),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _step_0());
      }, []),
    );
    return _answer;
  }, ["initialState", "taps", "count"]),
  "{ :initialState :taps :count | let register = copy(initialState); let k = size(register); let step = { let bit = %(reduce(collect(taps, { :i | at(register, -(+(k, 1), i)) }),+), 2); removeLast(register); addFirst(register,bit); bit }; let answer = [first(register)]; timesRepeat((-(count, 1)), { add(answer,step()) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "squaredEuclideanDistance",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Statements */
    return _squared_1(_norm_1(_hyphenMinus_2(_self, _aList)));
  }, ["self", "aList"]),
  "{ :self :aList | squared(norm((-(self, aList)))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "standardDeviation",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isMatrix_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _transposed_1(_self),
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _sqrt_1(_variance_1(_each));
          }, ["each"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _sqrt_1(_variance_1(_self));
      }, []),
    );
  }, ["self"]),
  "{ :self | if(isMatrix(self), { collect(transposed(self), { :each | sqrt(variance(each)) }) }, { sqrt(variance(self)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "standardize",
  ["self", "meanBlock:/1", "deviationBlock:/1"],
  sl.annotateFunction(function (_self, _meanBlock_1, _deviationBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _meanBlock_1, _deviationBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _deviation = _deviationBlock_1(_self);
    /* Statements */
    return _if_3(
      _equalsSign_2(_deviation, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>standardize: deviation = 0?");
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _solidus_2(
          _hyphenMinus_2(_self, _meanBlock_1(_self)),
          _deviation,
        );
      }, []),
    );
  }, ["self", "meanBlock:/1", "deviationBlock:/1"]),
  "{ :self :meanBlock:/1 :deviationBlock:/1 | let deviation = deviationBlock(self); if((=(deviation, 0)), { error(self,'@Sequenceable>>standardize: deviation = 0?') }, { /((-(self, meanBlock(self))), deviation) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "standardize",
  ["self", "meanBlock:/1"],
  sl.annotateFunction(function (_self, _meanBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _meanBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _standardize_3(_self, _meanBlock_1, _standardDeviation_1);
  }, ["self", "meanBlock:/1"]),
  "{ :self :meanBlock:/1 | standardize(self,meanBlock:/1, standardDeviation:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "standardize",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _standardize_3(_self, _mean_1, _standardDeviation_1);
  }, ["self"]),
  "{ :self | standardize(self,mean:/1, standardDeviation:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "suffixesDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _size = _size_1(_self);
    /* Statements */
    return _toDo_3(
      1,
      _size,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_1(_copyFromTo_3(_self, _each, _size));
      }, ["each"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let size = size(self); toDo(1, size, { :each | aBlock(copyFromTo(self,each, size)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "suffixes",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _suffixesDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
    );
    return _answer;
  }, ["self"]),
  "{ :self | let answer = []; suffixesDo(self, { :each | add(answer,each) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "second",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _plusSign_2(1, 1));
  }, ["self"]),
  "{ :self | at(self, +(1, 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "second",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(_self, _plusSign_2(_n, 1), _asterisk_2(_n, 2));
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,+(n, 1), *(n, 2)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "secondLast",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _hyphenMinus_2(_size_1(_self), 1));
  }, ["self"]),
  "{ :self | at(self, -(size(self), 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "select",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _aBlock_1(_at_2(_self, _index)),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _add_2(_answer, _at_2(_self, _index));
          }, []),
        );
      }, ["index"]),
    );
    return _newFrom_2(_species_1(_self), _answer);
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let answer = []; indicesDo(self, { :index | ifTrue(aBlock(at(self, index)), { add(answer,at(self, index)) }) }); newFrom(species(self),answer) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "shuffle",
  ["self", "rng"],
  sl.annotateFunction(function (_self, _rng) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _rng";
      throw new Error(errorMessage);
    } /* Statements */
    return _fisherYatesShuffle_2(_self, _rng);
  }, ["self", "rng"]),
  "{ :self :rng | fisherYatesShuffle(self,rng) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "shuffle",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _fisherYatesShuffle_1(_self);
  }, ["self"]),
  "{ :self | fisherYatesShuffle(self) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "shuffled",
  ["self", "rng"],
  sl.annotateFunction(function (_self, _rng) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _rng";
      throw new Error(errorMessage);
    } /* Statements */
    return _fisherYatesShuffle_2(_copy_1(_self), _rng);
  }, ["self", "rng"]),
  "{ :self :rng | fisherYatesShuffle(copy(self),rng) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "shuffled",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _fisherYatesShuffle_1(_copy_1(_self));
  }, ["self"]),
  "{ :self | fisherYatesShuffle(copy(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sort",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _sortBy_2(_self, _lessThanSignEqualsSign_2);
  }, ["self"]),
  "{ :self | sortBy(self,<=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sort",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifNil_3(
      _aBlock_2,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _sort_1(_self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _sortBy_2(_self, _aBlock_2);
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | ifNil(aBlock:/2, { sort(self) }, { sortBy(self,aBlock:/2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sortOn",
  ["self", "keyBlock:/1"],
  sl.annotateFunction(function (_self, _keyBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _keyBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _sortOnBy_3(_self, _keyBlock_1, _lessThanSignEqualsSign_2);
  }, ["self", "keyBlock:/1"]),
  "{ :self :keyBlock:/1 | sortOnBy(self,keyBlock:/1, <=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sorted",
  ["self", "aSortBlock:/2"],
  sl.annotateFunction(function (_self, _aSortBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aSortBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _sortBy_2(_copy_1(_self), _aSortBlock_2);
  }, ["self", "aSortBlock:/2"]),
  "{ :self :aSortBlock:/2 | sortBy(copy(self),aSortBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sorted",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _sort_1(_copy_1(_self));
  }, ["self"]),
  "{ :self | sort(copy(self)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sortedWithIndices",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _sortedWithIndices_2(_self, _lessThanSignEqualsSign_2);
  }, ["self"]),
  "{ :self | sortedWithIndices(self,<=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "sortedWithIndices",
  ["self", "sortBlock:/2"],
  sl.annotateFunction(function (_self, _sortBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _sortBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _sorted_2(
          _withIndexCollect_2(
            _self,
            sl.annotateFunction(function (_each, _index) {
              /* ArityCheck */
              if (arguments.length !== 2) {
                const errorMessage = "Arity: expected 2, _each, _index";
                throw new Error(errorMessage);
              } /* Statements */
              return _hyphenMinusGreaterThanSign_2(_each, _index);
            }, ["each", "index"]),
          ),
          sl.annotateFunction(function (_p, _q) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _p, _q";
              throw new Error(errorMessage);
            } /* Statements */
            return _sortBlock_2(_key_1(_p), _key_1(_q));
          }, ["p", "q"]),
        );
      }, []),
    );
  }, ["self", "sortBlock:/2"]),
  "{ :self :sortBlock:/2 | ifEmpty(self, { [] }, { sorted(withIndexCollect(self, { :each :index | ->(each, index) }), { :p :q | sortBlock(key(p), key(q)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "split",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _ifEmpty_3(
      _self,
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _answer = [];
        let _startIndex = 1;
        let _previous = _first_1(_self);
        /* Statements */
        _toDo_3(
          2,
          _size_1(_self),
          sl.annotateFunction(function (_index) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _index";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _next = _at_2(_self, _index);
            /* Statements */
            _ifFalse_2(
              _aBlock_2(_previous, _next),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                _add_2(
                  _answer,
                  _copyFromTo_3(_self, _startIndex, _hyphenMinus_2(_index, 1)),
                );
                return _startIndex = _index;
              }, []),
            );
            return _previous = _next;
          }, ["index"]),
        );
        _add_2(_answer, _copyFromTo_3(_self, _startIndex, _size_1(_self)));
        return _answer;
      }, []),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | ifEmpty(self, { [] }, { let answer = []; let startIndex = 1; let previous = first(self); toDo(2, size(self), { :index | let next = at(self, index); ifFalse(aBlock(previous, next), { add(answer,copyFromTo(self,startIndex, -(index, 1))); startIndex := index }); previous := next }); add(answer,copyFromTo(self,startIndex, size(self))); answer }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "splitBy",
  ["self", "aCollection"],
  sl.annotateFunction(function (_self, _aCollection) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aCollection";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _splitByDo_3(
      _self,
      _aCollection,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _each);
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aCollection"]),
  "{ :self :aCollection | let answer = []; splitByDo(self, aCollection, { :each | add(answer,each) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "splitByDo",
  ["self", "aCollection", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aCollection, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aCollection, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _lastIndex = 1;
    let _nextIndex = null;
    /* Statements */
    _whileFalse_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _nextIndex = _indexOfSubstringStartingAt_3(
          _self,
          _aCollection,
          _lastIndex,
        );
        return _equalsSign_2(_nextIndex, 0);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        _aBlock_1(
          _copyFromTo_3(_self, _lastIndex, _hyphenMinus_2(_nextIndex, 1)),
        );
        return _lastIndex = _plusSign_2(_nextIndex, _size_1(_aCollection));
      }, []),
    );
    return _aBlock_1(_copyFromTo_3(_self, _lastIndex, _size_1(_self)));
  }, ["self", "aCollection", "aBlock:/1"]),
  "{ :self :aCollection :aBlock:/1 | let lastIndex = 1; let nextIndex = nil; whileFalse({ nextIndex := indexOfSubstringStartingAt(self,aCollection, lastIndex); =(nextIndex, 0) }, { aBlock(copyFromTo(self,lastIndex, -(nextIndex, 1))); lastIndex := +(nextIndex, size(aCollection)) }); aBlock(copyFromTo(self,lastIndex, size(self))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "subsequencesDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isEmpty_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return [];
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _k = _size_1(_self);
        /* Statements */
        return _powerSetDo_2(
          _asList_1(_upOrDownTo_2(1, _k)),
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_1(_commercialAtAsterisk_2(_self, _each));
          }, ["each"]),
        );
      }, []),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | if(isEmpty(self), { [] }, { let k = size(self); powerSetDo(asList(upOrDownTo(1, k)), { :each | aBlock(@*(self, each)) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "subsequences",
  ["self", "aPredicate:/1"],
  sl.annotateFunction(function (_self, _aPredicate_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aPredicate_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _subsequencesDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _aPredicate_1(_each),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _add_2(_answer, _copy_1(_each));
          }, []),
        );
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aPredicate:/1"]),
  "{ :self :aPredicate:/1 | let answer = []; subsequencesDo(self, { :each | ifTrue(aPredicate(each), { add(answer,copy(each)) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "subsequences",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _subsequences_2(_self, _constant_1(true));
  }, ["self"]),
  "{ :self | subsequences(self,constant(true)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "substringsDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _toDo_3(
      0,
      _size_1(_self),
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _partitionDo_4(_self, _each, 1, _aBlock_1);
      }, ["each"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | toDo(0, size(self), { :each | partitionDo(self,each, 1, aBlock:/1) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "substrings",
  ["self", "aPredicate:/1"],
  sl.annotateFunction(function (_self, _aPredicate_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aPredicate_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _substringsDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _ifTrue_2(
          _aPredicate_1(_each),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _add_2(_answer, _copy_1(_each));
          }, []),
        );
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aPredicate:/1"]),
  "{ :self :aPredicate:/1 | let answer = []; substringsDo(self, { :each | ifTrue(aPredicate(each), { add(answer,copy(each)) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "substrings",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _substrings_2(_self, _constant_1(true));
  }, ["self"]),
  "{ :self | substrings(self,constant(true)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "substringsInCommon",
  ["self", "aList", "k"],
  sl.annotateFunction(function (_self, _aList, _k) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _k";
      throw new Error(errorMessage);
    } /* Statements */
    return _intersection_2(
      _partition_3(_self, _k, 1),
      _partition_3(_aList, _k, 1),
    );
  }, ["self", "aList", "k"]),
  "{ :self :aList :k | intersection(partition(self,k, 1),partition(aList,k, 1)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "swapAllWith",
  ["self", "indices"],
  sl.annotateFunction(function (_self, _indices) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _indices";
      throw new Error(errorMessage);
    } /* Statements */
    return _caseOf_2(_rank_1(_indices), [
      _hyphenMinusGreaterThanSign_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return 2;
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _do_2(
            _indices,
            sl.annotateFunction(function (_each) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _each";
                throw new Error(errorMessage);
              } /* Temporaries */
              let __SPL6 = _assertIsOfSize_2(_each, 2);
              let _i = _at_2(__SPL6, 1);
              let _j = _at_2(__SPL6, 2);
              let _x = _at_2(_self, _i);
              /* Statements */
              _atPut_3(_self, _i, _at_2(_self, _j));
              return _atPut_3(_self, _j, _x);
            }, ["each"]),
          );
        }, []),
      ),
      _hyphenMinusGreaterThanSign_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return 3;
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _do_2(
            _indices,
            sl.annotateFunction(function (_each) {
              /* ArityCheck */
              if (arguments.length !== 1) {
                const errorMessage = "Arity: expected 1, _each";
                throw new Error(errorMessage);
              } /* Temporaries */
              let __SPL7 = _assertIsOfSize_2(_each, 2);
              let _i = _at_2(__SPL7, 1);
              let _j = _at_2(__SPL7, 2);
              let _x = _atPath_2(_self, _i);
              /* Statements */
              _atPathPut_3(_self, _i, _atPath_2(_self, _j));
              return _atPathPut_3(_self, _j, _x);
            }, ["each"]),
          );
        }, []),
      ),
    ]);
  }, ["self", "indices"]),
  "{ :self :indices | caseOf(rank(indices),[->({ 2 }, { do(indices, { :each | let __SPL6 = assertIsOfSize(each, 2); let i = at(__SPL6, 1); let j = at(__SPL6, 2); let x = at(self, i); atPut(self, i, at(self, j)); atPut(self, j, x) }) }), ->({ 3 }, { do(indices, { :each | let __SPL7 = assertIsOfSize(each, 2); let i = at(__SPL7, 1); let j = at(__SPL7, 2); let x = atPath(self,i); atPathPut(self,i, atPath(self,j)); atPathPut(self,j, x) }) })]) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "swapPathWith",
  ["self", "i", "j"],
  sl.annotateFunction(function (_self, _i, _j) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _i, _j";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _atPath_2(_self, _i);
    /* Statements */
    _atPathPut_3(_self, _i, _atPath_2(_self, _j));
    return _atPathPut_3(_self, _j, _x);
  }, ["self", "i", "j"]),
  "{ :self :i :j | let x = atPath(self,i); atPathPut(self,i, atPath(self,j)); atPathPut(self,j, x) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "swapWith",
  ["self", "i", "j"],
  sl.annotateFunction(function (_self, _i, _j) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _i, _j";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _x = _at_2(_self, _i);
    /* Statements */
    _atPut_3(_self, _i, _at_2(_self, _j));
    return _atPut_3(_self, _j, _x);
  }, ["self", "i", "j"]),
  "{ :self :i :j | let x = at(self, i); atPut(self, i, at(self, j)); atPut(self, j, x) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "take",
  ["self", "count", "fill"],
  sl.annotateFunction(function (_self, _count, _fill) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _count, _fill";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_count, 0),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _takeLast_3(_self, _negated_1(_count), _fill);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _takeFirst_3(_self, _count, _fill);
      }, []),
    );
  }, ["self", "count", "fill"]),
  "{ :self :count :fill | if((<(count, 0)), { takeLast(self,negated(count), fill) }, { takeFirst(self,count, fill) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "takeFirst",
  ["self", "count", "fill"],
  sl.annotateFunction(function (_self, _count, _fill) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _count, _fill";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_count, _size_1(_self)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSignPlusSign_2(
          _self,
          _numberSign_2(_fill, _hyphenMinus_2(_count, _size_1(_self))),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, 1, _count);
      }, []),
    );
  }, ["self", "count", "fill"]),
  "{ :self :count :fill | if((>(count, size(self))), { ++(self, (#(fill, (-(count, size(self)))))) }, { copyFromTo(self,1, count) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "takeLast",
  ["self", "count", "fill"],
  sl.annotateFunction(function (_self, _count, _fill) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _count, _fill";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _greaterThanSign_2(_count, _size_1(_self)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _plusSignPlusSign_2(
          _numberSign_2(_fill, _hyphenMinus_2(_count, _size_1(_self))),
          _self,
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(
          _self,
          _plusSign_2(_hyphenMinus_2(_size_1(_self), _count), 1),
          _size_1(_self),
        );
      }, []),
    );
  }, ["self", "count", "fill"]),
  "{ :self :count :fill | if((>(count, size(self))), { ++((#(fill, (-(count, size(self))))), self) }, { copyFromTo(self,+(-(size(self), count), 1), size(self)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "takeList",
  ["self", "aList"],
  sl.annotateFunction(function (_self, _aList) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aList";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    let _startIndex = 1;
    /* Statements */
    _do_2(
      _aList,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _endIndex = _hyphenMinus_2(_plusSign_2(_startIndex, _each), 1);
        /* Statements */
        _add_2(_answer, _copyFromTo_3(_self, _startIndex, _endIndex));
        return _startIndex = _plusSign_2(_endIndex, 1);
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aList"]),
  "{ :self :aList | let answer = []; let startIndex = 1; do(aList, { :each | let endIndex = -(+(startIndex, each), 1); add(answer,copyFromTo(self,startIndex, endIndex)); startIndex := +(endIndex, 1) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "takeWhile",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _detectIndexIfFoundIfNone_4(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _not_1(_aBlock_1(_each));
      }, ["each"]),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, 1, _hyphenMinus_2(_i, 1));
      }, ["i"]),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | detectIndexIfFoundIfNone(self, { :each | not(aBlock(each)) }, { :i | copyFromTo(self,1, -(i, 1)) }, { self }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "third",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, 3);
  }, ["self"]),
  "{ :self | at(self, 3) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "third",
  ["self", "n"],
  sl.annotateFunction(function (_self, _n) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _n";
      throw new Error(errorMessage);
    } /* Statements */
    return _copyFromTo_3(
      _self,
      _plusSign_2(_asterisk_2(_n, 2), 1),
      _asterisk_2(_n, 3),
    );
  }, ["self", "n"]),
  "{ :self :n | copyFromTo(self,+(*(n, 2), 1), *(n, 3)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "thirdLast",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _at_2(_self, _hyphenMinus_2(_size_1(_self), 2));
  }, ["self"]),
  "{ :self | at(self, -(size(self), 2)) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "transposed",
  ["self", "permutation"],
  sl.annotateFunction(function (_self, _permutation) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _permutation";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isPermutationList_1(_permutation),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _fromShape = _shape_1(_self);
        let _toShape = _commercialAtAsterisk_2(_fromShape, _permutation);
        let _inverse = _list_2(
          _inversePermutation_1(_permutation),
          _size_1(_permutation),
        );
        /* Statements */
        return _fill_2(
          _toShape,
          sl.annotateFunction(function (_toIndex) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _toIndex";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _fromIndex = _commercialAtAsterisk_2(_toIndex, _inverse);
            /* Statements */
            return _commercialAtGreaterThanSign_2(_self, _fromIndex);
          }, ["toIndex"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>transposed: not permutation");
      }, []),
    );
  }, ["self", "permutation"]),
  "{ :self :permutation | if(isPermutationList(permutation), { let fromShape = shape(self); let toShape = @*(fromShape, permutation); let inverse = list(inversePermutation(permutation),size(permutation)); fill(toShape, { :toIndex | let fromIndex = @*(toIndex, inverse); @>(self, fromIndex) }) }, { error(self,'@Sequenceable>>transposed: not permutation') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "transposed",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _toAsCollect_4(
      1,
      _size_1(_first_1(_self)),
      _species_1(_first_1(_self)),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(
          _self,
          sl.annotateFunction(function (_row) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _row";
              throw new Error(errorMessage);
            } /* Statements */
            return _at_2(_row, _index);
          }, ["row"]),
        );
      }, ["index"]),
    );
  }, ["self"]),
  "{ :self | toAsCollect(1, size(first(self)), species(first(self)), { :index | collect(self, { :row | at(row, index) }) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "tuplesCollect",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = [];
    /* Statements */
    _tuplesDo_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _add_2(_answer, _aBlock_1(_each));
      }, ["each"]),
    );
    return _answer;
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let answer = []; tuplesDo(self, { :each | add(answer,aBlock(each)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "tuplesDo",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _tupleCount = _product_1(_collect_2(_self, _size_1));
    let _tuple = _new_2(_species_1(_self), _size_1(_self));
    /* Statements */
    return _toDo_3(
      1,
      _tupleCount,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _k = _hyphenMinus_2(_i, 1);
        /* Statements */
        _toByDo_4(
          _size_1(_self),
          1,
          -1,
          sl.annotateFunction(function (_j) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _j";
              throw new Error(errorMessage);
            } /* Temporaries */
            let _fromSequence = _at_2(_self, _j);
            /* Statements */
            _atPut_3(
              _tuple,
              _j,
              _at_2(
                _fromSequence,
                _plusSign_2(_percentSign_2(_k, _size_1(_fromSequence)), 1),
              ),
            );
            return _k = _solidusSolidus_2(_k, _size_1(_fromSequence));
          }, ["j"]),
        );
        return _aBlock_1(_tuple);
      }, ["i"]),
    );
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | let tupleCount = product(collect(self,size:/1)); let tuple = new(species(self),size(self)); toDo(1, tupleCount, { :i | let k = -(i, 1); toByDo(size(self), 1, -1, { :j | let fromSequence = at(self, j); atPut(tuple, j, at(fromSequence, +(%(k, size(fromSequence)), 1))); k := //(k, size(fromSequence)) }); aBlock(tuple) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "tuples",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _tuplesCollect_2(_self, _copy_1);
  }, ["self"]),
  "{ :self | tuplesCollect(self,copy:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "tuples",
  ["self", "count"],
  sl.annotateFunction(function (_self, _count) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _count";
      throw new Error(errorMessage);
    } /* Statements */
    return _tuples_1(_exclamationMark_2(_self, _count));
  }, ["self", "count"]),
  "{ :self :count | tuples((!(self, count))) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "uniqueElements",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _uniqueElements_2(_self, _equalsSign_2);
  }, ["self"]),
  "{ :self | uniqueElements(self,=) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "uniqueElements",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _withIndexCollect_2(
      _self,
      sl.annotateFunction(function (_p, _i) {
        /* ArityCheck */
        if (arguments.length !== 2) {
          const errorMessage = "Arity: expected 2, _p, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _seen = [];
        /* Statements */
        _withIndexDo_2(
          _self,
          sl.annotateFunction(function (_q, _j) {
            /* ArityCheck */
            if (arguments.length !== 2) {
              const errorMessage = "Arity: expected 2, _q, _j";
              throw new Error(errorMessage);
            } /* Statements */
            return _ifTrue_2(
              _tildeEqualsSign_2(_i, _j),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _do_2(
                  _q,
                  sl.annotateFunction(function (_each) {
                    /* ArityCheck */
                    if (arguments.length !== 1) {
                      const errorMessage = "Arity: expected 1, _each";
                      throw new Error(errorMessage);
                    } /* Statements */
                    return _ifFalse_2(
                      _includesBy_3(_seen, _each, _aBlock_2),
                      sl.annotateFunction(function () {
                        /* ArityCheck */
                        if (arguments.length !== 0) {
                          const errorMessage = "Arity: expected 0, ";
                          throw new Error(errorMessage);
                        } /* Statements */
                        return _add_2(_seen, _each);
                      }, []),
                    );
                  }, ["each"]),
                );
              }, []),
            );
          }, ["q", "j"]),
        );
        return _nubBy_2(_difference_2(_p, _seen), _aBlock_2);
      }, ["p", "i"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | withIndexCollect(self, { :p :i | let seen = []; withIndexDo(self, { :q :j | ifTrue((~=(i, j)), { do(q, { :each | ifFalse(includesBy(seen,each, aBlock:/2), { add(seen,each) }) }) }) }); nubBy(difference(p,seen),aBlock:/2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "variance",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isMatrix_1(_self),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _collect_2(_transposed_1(_self), _variance_1);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _solidus_2(
          _sum_1(_circumflexAccent_2(_hyphenMinus_2(_self, _mean_1(_self)), 2)),
          _hyphenMinus_2(_size_1(_self), 1),
        );
      }, []),
    );
  }, ["self"]),
  "{ :self | if(isMatrix(self), { collect(transposed(self),variance:/1) }, { /(sum((^((-(self, mean(self))), 2))), (-(size(self), 1))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "vectorAngle",
  ["u", "v"],
  sl.annotateFunction(function (_u, _v) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _u, _v";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _ampersand_2(
        _isVector_1(_u),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _isVector_1(_v);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _arcCos_1(
          _solidus_2(_dot_2(_u, _v), _asterisk_2(_norm_1(_u), _norm_1(_v))),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _error_2(_self, "@Sequenceable>>vectorAngle: not vectors");
      }, []),
    );
  }, ["u", "v"]),
  "{ :u :v | if((&(isVector(u), { isVector(v) })), { arcCos((/(dot(u,v), (*(norm(u), norm(v)))))) }, { error(self,'@Sequenceable>>vectorAngle: not vectors') }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "isVeryCloseTo",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _equalBy_3(_self, _anObject, _isVeryCloseTo_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | equalBy(self,anObject, isVeryCloseTo:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "which",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Statements */
      _do_2(
        _self,
        sl.annotateFunction(function (_each) {
          /* ArityCheck */
          if (arguments.length !== 1) {
            const errorMessage = "Arity: expected 1, _each";
            throw new Error(errorMessage);
          } /* Statements */
          return _ifTrue_2(
            _value_1(_key_1(_each)),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _return_1(_value_1(_value_1(_each)));
            }, []),
          );
        }, ["each"]),
      );
      return null;
    }, ["return:/1"]));
  }, ["self"]),
  "{ :self | valueWithReturn({ :return:/1 | do(self, { :each | ifTrue(value(key(each)), { return(value(value(each))) }) }); nil }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollect",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _withCollectWrapping_3(_self, _aList, _aBlock_2);
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | withCollectWrapping(self,aList, aBlock:/2) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectCrossed",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _new_2(
      _species_1(_self),
      _asterisk_2(_size_1(_self), _size_1(_aList)),
    );
    let _nextIndex = 1;
    /* Statements */
    _do_2(
      _self,
      sl.annotateFunction(function (_leftItem) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _leftItem";
          throw new Error(errorMessage);
        } /* Statements */
        return _do_2(
          _aList,
          sl.annotateFunction(function (_rightItem) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _rightItem";
              throw new Error(errorMessage);
            } /* Statements */
            _atPut_3(_answer, _nextIndex, _aBlock_2(_leftItem, _rightItem));
            return _nextIndex = _plusSign_2(_nextIndex, 1);
          }, ["rightItem"]),
        );
      }, ["leftItem"]),
    );
    return _answer;
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | let answer = new(species(self),*(size(self), size(aList))); let nextIndex = 1; do(self, { :leftItem | do(aList, { :rightItem | atPut(answer, nextIndex, aBlock(leftItem, rightItem)); nextIndex := +(nextIndex, 1) }) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectEqual",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    _isOfSameSizeCheck_2(_self, _aList);
    return _toAsCollect_4(
      1,
      _size_1(_self),
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_at_2(_self, _index), _at_2(_aList, _index));
      }, ["index"]),
    );
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | isOfSameSizeCheck(self,aList); toAsCollect(1, size(self), species(self), { :index | aBlock(at(self, index), at(aList, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectFolding",
  ["self", "aCollection", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aCollection, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aCollection, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _maximumSize = _max_2(_size_1(_self), _size_1(_aCollection));
    /* Statements */
    return _toAsCollect_4(
      1,
      _maximumSize,
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(
          _atFold_2(_self, _index),
          _atFold_2(_aCollection, _index),
        );
      }, ["index"]),
    );
  }, ["self", "aCollection", "aBlock:/2"]),
  "{ :self :aCollection :aBlock:/2 | let maximumSize = max(size(self),size(aCollection)); toAsCollect(1, maximumSize, species(self), { :index | aBlock(atFold(self,index), atFold(aCollection,index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectOrAdaptTo",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _ampersand_2(
        _isCollection_1(_anObject),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _isSequenceable_1(_anObject);
        }, []),
      ),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withCollect_3(_self, _anObject, _aBlock_2);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _adaptToCollectionAndApply_3(_anObject, _self, _aBlock_2);
      }, []),
    );
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | if((&(isCollection(anObject), { isSequenceable(anObject) })), { withCollect(self,anObject, aBlock:/2) }, { adaptToCollectionAndApply(anObject,self, aBlock:/2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectOuter",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _outer_3(_aBlock_2, _self, _aList);
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | outer(aBlock:/2,self, aList) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectTable",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _collect_2(
      _self,
      sl.annotateFunction(function (_each) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _each";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_each, _aList);
      }, ["each"]),
    );
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | collect(self, { :each | aBlock(each, aList) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectTruncating",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _lessThanSign_2(_size_1(_self), _size_1(_aList)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withCollect_3(
          _self,
          _take_2(_aList, _size_1(_self)),
          _aBlock_2,
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withCollect_3(
          _take_2(_self, _size_1(_aList)),
          _aList,
          _aBlock_2,
        );
      }, []),
    );
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | if((<(size(self), size(aList))), { withCollect(self,take(aList,size(self)), aBlock:/2) }, { withCollect(take(self,size(aList)),aList, aBlock:/2) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withCollectWrapping",
  ["self", "aList", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aList, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _aList, _aBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _maximumSize = _max_2(_size_1(_self), _size_1(_aList));
    /* Statements */
    return _toAsCollect_4(
      1,
      _maximumSize,
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_2(_atWrap_2(_self, _index), _atWrap_2(_aList, _index));
      }, ["index"]),
    );
  }, ["self", "aList", "aBlock:/2"]),
  "{ :self :aList :aBlock:/2 | let maximumSize = max(size(self),size(aList)); toAsCollect(1, maximumSize, species(self), { :index | aBlock(atWrap(self,index), atWrap(aList,index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withDo",
  ["self", "otherCollection", "twoArgBlock:/2"],
  sl.annotateFunction(function (_self, _otherCollection, _twoArgBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _otherCollection, _twoArgBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    _isOfSameSizeCheck_2(_self, _otherCollection);
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _twoArgBlock_2(
          _at_2(_self, _index),
          _at_2(_otherCollection, _index),
        );
      }, ["index"]),
    );
  }, ["self", "otherCollection", "twoArgBlock:/2"]),
  "{ :self :otherCollection :twoArgBlock:/2 | isOfSameSizeCheck(self,otherCollection); indicesDo(self, { :index | twoArgBlock(at(self, index), at(otherCollection, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withReplace",
  ["self", "otherCollection", "aBlock:/2"],
  sl.annotateFunction(function (_self, _otherCollection, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage =
        "Arity: expected 3, _self, _otherCollection, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    _isOfSameSizeCheck_2(_self, _otherCollection);
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _self,
          _index,
          _aBlock_2(_at_2(_self, _index), _at_2(_otherCollection, _index)),
        );
      }, ["index"]),
    );
  }, ["self", "otherCollection", "aBlock:/2"]),
  "{ :self :otherCollection :aBlock:/2 | isOfSameSizeCheck(self,otherCollection); indicesDo(self, { :index | atPut(self, index, aBlock(at(self, index), at(otherCollection, index))) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withIndexCollect",
  ["self", "elementAndIndexBlock:/2"],
  sl.annotateFunction(function (_self, _elementAndIndexBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _elementAndIndexBlock_2";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _ofSize_2(_species_1(_self), _size_1(_self));
    /* Statements */
    _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _answer,
          _index,
          _elementAndIndexBlock_2(_at_2(_self, _index), _index),
        );
      }, ["index"]),
    );
    return _answer;
  }, ["self", "elementAndIndexBlock:/2"]),
  "{ :self :elementAndIndexBlock:/2 | let answer = ofSize(species(self),size(self)); indicesDo(self, { :index | atPut(answer, index, elementAndIndexBlock(at(self, index), index)) }); answer }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withIndexDo",
  ["self", "elementAndIndexBlock:/2"],
  sl.annotateFunction(function (_self, _elementAndIndexBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _elementAndIndexBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _elementAndIndexBlock_2(_at_2(_self, _index), _index);
      }, ["index"]),
    );
  }, ["self", "elementAndIndexBlock:/2"]),
  "{ :self :elementAndIndexBlock:/2 | indicesDo(self, { :index | elementAndIndexBlock(at(self, index), index) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withoutTrailingZeros",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _n = _size_1(_self);
    /* Statements */
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _ampersand_2(
          _greaterThanSignEqualsSign_2(_n, 2),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            return _isZero_1(_at_2(_self, _n));
          }, []),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _n = _hyphenMinus_2(_n, 1);
      }, []),
    );
    return _if_3(
      _equalsSign_2(_n, _size_1(_self)),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _self;
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _copyFromTo_3(_self, 1, _n);
      }, []),
    );
  }, ["self"]),
  "{ :self | let n = size(self); whileTrue({ &(>=(n, 2), { isZero(at(self, n)) }) }, { n := -(n, 1) }); if((=(n, size(self))), { self }, { copyFromTo(self,1, n) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withIndexReplace",
  ["self", "aBlock:/2"],
  sl.annotateFunction(function (_self, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(_self, _index, _aBlock_2(_at_2(_self, _index), _index));
      }, ["index"]),
    );
  }, ["self", "aBlock:/2"]),
  "{ :self :aBlock:/2 | indicesDo(self, { :index | atPut(self, index, aBlock(at(self, index), index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withWithCollect",
  ["self", "aList", "anotherList", "aBlock:/3"],
  sl.annotateFunction(function (_self, _aList, _anotherList, _aBlock_3) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _aList, _anotherList, _aBlock_3";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _maximumSize = _max_1(
      _collect_2([_self, _aList, _anotherList], _size_1),
    );
    /* Statements */
    return _toAsCollect_4(
      1,
      _maximumSize,
      _species_1(_self),
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_3(
          _atWrap_2(_self, _index),
          _atWrap_2(_aList, _index),
          _atWrap_2(_anotherList, _index),
        );
      }, ["index"]),
    );
  }, ["self", "aList", "anotherList", "aBlock:/3"]),
  "{ :self :aList :anotherList :aBlock:/3 | let maximumSize = max(collect([self, aList, anotherList],size:/1)); toAsCollect(1, maximumSize, species(self), { :index | aBlock(atWrap(self,index), atWrap(aList,index), atWrap(anotherList,index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "withWithDo",
  ["self", "aList", "anotherList", "aBlock:/3"],
  sl.annotateFunction(function (_self, _aList, _anotherList, _aBlock_3) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _aList, _anotherList, _aBlock_3";
      throw new Error(errorMessage);
    } /* Statements */
    _isOfSameSizeCheck_2(_self, _aList);
    _isOfSameSizeCheck_2(_self, _anotherList);
    return _indicesDo_2(
      _self,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _aBlock_3(
          _at_2(_self, _index),
          _at_2(_aList, _index),
          _at_2(_anotherList, _index),
        );
      }, ["index"]),
    );
  }, ["self", "aList", "anotherList", "aBlock:/3"]),
  "{ :self :aList :anotherList :aBlock:/3 | isOfSameSizeCheck(self,aList); isOfSameSizeCheck(self,anotherList); indicesDo(self, { :index | aBlock(at(self, index), at(aList, index), at(anotherList, index)) }) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "zeroCrossingCount",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return _count_2(_zeroCrossingDetect_1(_self), _identity_1);
  }, ["self"]),
  "{ :self | count(zeroCrossingDetect(self),identity:/1) }",
);

sl.addMethodToExistingTrait(
  "Sequenceable",
  "Sequenceable",
  "zeroCrossingDetect",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _List_2(_size_1(_self), false);
    let _p = _sign_1(_at_2(_self, 1));
    /* Statements */
    _toDo_3(
      2,
      _size_1(_self),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _q = _sign_1(_at_2(_self, _i));
        /* Statements */
        return _ifFalse_2(
          _isZero_1(_q),
          sl.annotateFunction(function () {
            /* ArityCheck */
            if (arguments.length !== 0) {
              const errorMessage = "Arity: expected 0, ";
              throw new Error(errorMessage);
            } /* Statements */
            _ifTrue_2(
              _isZero_1(_plusSign_2(_p, _q)),
              sl.annotateFunction(function () {
                /* ArityCheck */
                if (arguments.length !== 0) {
                  const errorMessage = "Arity: expected 0, ";
                  throw new Error(errorMessage);
                } /* Statements */
                return _atPut_3(_answer, _i, true);
              }, []),
            );
            return _p = _q;
          }, []),
        );
      }, ["i"]),
    );
    return _answer;
  }, ["self"]),
  "{ :self | let answer = List(size(self), false); let p = sign(at(self, 1)); toDo(2, size(self), { :i | let q = sign(at(self, i)); ifFalse(isZero(q), { ifTrue(isZero((+(p, q))), { atPut(answer, i, true) }); p := q }) }); answer }",
);

sl.extendTypeOrTraitWithMethod(
  "@Sequenceable",
  "Sequenceable",
  "applyBinaryMathOperatorInPlace",
  ["self", "anObject", "aBlock:/2"],
  sl.annotateFunction(function (_self, _anObject, _aBlock_2) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _anObject, _aBlock_2";
      throw new Error(errorMessage);
    } /* Statements */
    return _if_3(
      _isNumber_1(_anObject),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _replace_2(
          _self,
          sl.annotateFunction(function (_each) {
            /* ArityCheck */
            if (arguments.length !== 1) {
              const errorMessage = "Arity: expected 1, _each";
              throw new Error(errorMessage);
            } /* Statements */
            return _aBlock_2(_each, _anObject);
          }, ["each"]),
        );
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _withReplace_3(_self, _anObject, _aBlock_2);
      }, []),
    );
  }, ["self", "anObject", "aBlock:/2"]),
  "{ :self :anObject :aBlock:/2 | if(isNumber(anObject), { replace(self, { :each | aBlock(each, anObject) }) }, { withReplace(self,anObject, aBlock:/2) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Sequenceable",
  "Sequenceable",
  "plusSignEqualsSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _applyBinaryMathOperatorInPlace_3(_self, _anObject, _plusSign_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | applyBinaryMathOperatorInPlace(self, anObject, +) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Sequenceable",
  "Sequenceable",
  "hyphenMinusEqualsSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _applyBinaryMathOperatorInPlace_3(_self, _anObject, _hyphenMinus_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | applyBinaryMathOperatorInPlace(self, anObject, -) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Sequenceable",
  "Sequenceable",
  "asteriskEqualsSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _applyBinaryMathOperatorInPlace_3(_self, _anObject, _asterisk_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | applyBinaryMathOperatorInPlace(self, anObject, *) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Sequenceable",
  "Sequenceable",
  "solidusEqualsSign",
  ["self", "anObject"],
  sl.annotateFunction(function (_self, _anObject) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _anObject";
      throw new Error(errorMessage);
    } /* Statements */
    return _applyBinaryMathOperatorInPlace_3(_self, _anObject, _solidus_2);
  }, ["self", "anObject"]),
  "{ :self :anObject | applyBinaryMathOperatorInPlace(self, anObject, /) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Integer",
  "Sequenceable",
  "binaryDetectIndex",
  ["self", "aBlock:/1"],
  sl.annotateFunction(function (_self, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _aBlock_1";
      throw new Error(errorMessage);
    } /* Statements */
    return _valueWithReturn_1(sl.annotateFunction(function (_return_1) {
      /* ArityCheck */
      if (arguments.length !== 1) {
        const errorMessage = "Arity: expected 1, _return_1";
        throw new Error(errorMessage);
      } /* Temporaries */
      let _low = 1;
      let _high = _self;
      /* Statements */
      _whileTrue_2(
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Statements */
          return _lessThanSignEqualsSign_2(_low, _high);
        }, []),
        sl.annotateFunction(function () {
          /* ArityCheck */
          if (arguments.length !== 0) {
            const errorMessage = "Arity: expected 0, ";
            throw new Error(errorMessage);
          } /* Temporaries */
          let _mid = _solidusSolidus_2(_plusSign_2(_low, _high), 2);
          /* Statements */
          return _if_3(
            _aBlock_1(_mid),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _high = _hyphenMinus_2(_mid, 1);
            }, []),
            sl.annotateFunction(function () {
              /* ArityCheck */
              if (arguments.length !== 0) {
                const errorMessage = "Arity: expected 0, ";
                throw new Error(errorMessage);
              } /* Statements */
              return _low = _plusSign_2(_mid, 1);
            }, []),
          );
        }, []),
      );
      return _low;
    }, ["return:/1"]));
  }, ["self", "aBlock:/1"]),
  "{ :self :aBlock:/1 | valueWithReturn({ :return:/1 | let low = 1; let high = self; whileTrue({ <=(low, high) }, { let mid = //((+(low, high)), 2); if(aBlock(mid), { high := -(mid, 1) }, { low := +(mid, 1) }) }); low }) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Integer",
  "Sequenceable",
  "calkinWilfSequence",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answer = _List_1(_self);
    /* Statements */
    _atPut_3(_answer, 1, _Fraction_2(1n, 1n));
    _toDo_3(
      2,
      _self,
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _p = _at_2(_answer, _hyphenMinus_2(_i, 1));
        let _t = _plusSign_2(
          _hyphenMinus_2(_asterisk_2(_floor_1(_p), 2), _p),
          1,
        );
        /* Statements */
        return _atPut_3(_answer, _i, _solidus_2(1, _t));
      }, ["i"]),
    );
    return _answer;
  }, ["self"]),
  "{ :self | let answer = List(self); atPut(answer, 1, Fraction(1L, 1L)); toDo(2, self, { :i | let p = at(answer, -(i, 1)); let t = +(-(*(floor(p), 2), p), 1); atPut(answer, i, /(1, t)) }); answer }",
);

sl.extendTypeOrTraitWithMethod(
  "@Integer",
  "Sequenceable",
  "fibonacciSequenceInto",
  ["self", "answer"],
  sl.annotateFunction(function (_self, _answer) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _answer";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _a = 0;
    let _b = 1;
    let _i = 0;
    /* Statements */
    _whileTrue_2(
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Statements */
        return _lessThanSign_2(_i, _self);
      }, []),
      sl.annotateFunction(function () {
        /* ArityCheck */
        if (arguments.length !== 0) {
          const errorMessage = "Arity: expected 0, ";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _tmp = _b;
        /* Statements */
        _add_2(_answer, _b);
        _b = _plusSign_2(_b, _a);
        _a = _tmp;
        return _i = _plusSign_2(_i, 1);
      }, []),
    );
    return _answer;
  }, ["self", "answer"]),
  "{ :self :answer | let a = 0; let b = 1; let i = 0; whileTrue({ <(i, self) }, { let tmp = b; add(answer,b); b := +(b, a); a := tmp; i := +(i, 1) }); answer }",
);

sl.extendTypeOrTraitWithMethod(
  "@Integer",
  "Sequenceable",
  "toAsCollect",
  ["self", "stop", "species", "aBlock:/1"],
  sl.annotateFunction(function (_self, _stop, _species, _aBlock_1) {
    /* ArityCheck */
    if (arguments.length !== 4) {
      const errorMessage =
        "Arity: expected 4, _self, _stop, _species, _aBlock_1";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _answerSize = _plusSign_2(_hyphenMinus_2(_stop, _self), 1);
    let _answer = _ofSize_2(_species, _answerSize);
    /* Statements */
    _toDo_3(
      1,
      _answerSize,
      sl.annotateFunction(function (_index) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _index";
          throw new Error(errorMessage);
        } /* Statements */
        return _atPut_3(
          _answer,
          _index,
          _aBlock_1(_hyphenMinus_2(_plusSign_2(_index, _self), 1)),
        );
      }, ["index"]),
    );
    return _answer;
  }, ["self", "stop", "species", "aBlock:/1"]),
  "{ :self :stop :species :aBlock:/1 | let answerSize = +(-(stop, self), 1); let answer = ofSize(species,answerSize); toDo(1, answerSize, { :index | atPut(answer, index, aBlock(-(+(index, self), 1))) }); answer }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Sequenceable",
  "chunksOfFrom",
  ["self", "chunkSize", "startingAt"],
  sl.annotateFunction(function (_self, _chunkSize, _startingAt) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _chunkSize, _startingAt";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _chunkCount = _ceiling_1(
      _solidus_2(
        _plusSign_2(_hyphenMinus_2(_size_1(_self), _startingAt), 1),
        _chunkSize,
      ),
    );
    /* Statements */
    return _collect_2(
      _to_2(0, _hyphenMinus_2(_chunkCount, 1)),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _startIndex = _plusSign_2(_asterisk_2(_i, _chunkSize), _startingAt);
        let _stopIndex = _min_2(
          _hyphenMinus_2(_plusSign_2(_startIndex, _chunkSize), 1),
          _size_1(_self),
        );
        /* Statements */
        return _copyFromTo_3(_self, _startIndex, _stopIndex);
      }, ["i"]),
    );
  }, ["self", "chunkSize", "startingAt"]),
  "{ :self :chunkSize :startingAt | let chunkCount = ceiling((/(+(-(size(self), startingAt), 1), chunkSize))); collect(to(0,-(chunkCount, 1)), { :i | let startIndex = +(*(i, chunkSize), startingAt); let stopIndex = min((-(+(startIndex, chunkSize), 1)),size(self)); copyFromTo(self,startIndex, stopIndex) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "String",
  "Sequenceable",
  "chunksOfFrom",
  ["self", "chunkSize", "startingAt"],
  sl.annotateFunction(function (_self, _chunkSize, _startingAt) {
    /* ArityCheck */
    if (arguments.length !== 3) {
      const errorMessage = "Arity: expected 3, _self, _chunkSize, _startingAt";
      throw new Error(errorMessage);
    } /* Temporaries */
    let _chunkCount = _ceiling_1(
      _solidus_2(
        _plusSign_2(_hyphenMinus_2(_size_1(_self), _startingAt), 1),
        _chunkSize,
      ),
    );
    /* Statements */
    return _collect_2(
      _to_2(0, _hyphenMinus_2(_chunkCount, 1)),
      sl.annotateFunction(function (_i) {
        /* ArityCheck */
        if (arguments.length !== 1) {
          const errorMessage = "Arity: expected 1, _i";
          throw new Error(errorMessage);
        } /* Temporaries */
        let _startIndex = _plusSign_2(_asterisk_2(_i, _chunkSize), _startingAt);
        let _stopIndex = _min_2(
          _hyphenMinus_2(_plusSign_2(_startIndex, _chunkSize), 1),
          _size_1(_self),
        );
        /* Statements */
        return _copyFromTo_3(_self, _startIndex, _stopIndex);
      }, ["i"]),
    );
  }, ["self", "chunkSize", "startingAt"]),
  "{ :self :chunkSize :startingAt | let chunkCount = ceiling((/(+(-(size(self), startingAt), 1), chunkSize))); collect(to(0,-(chunkCount, 1)), { :i | let startIndex = +(*(i, chunkSize), startingAt); let stopIndex = min((-(+(startIndex, chunkSize), 1)),size(self)); copyFromTo(self,startIndex, stopIndex) }) }",
);

sl.extendTypeOrTraitWithMethod(
  "List",
  "Sequenceable",
  "chunksOf",
  ["self", "chunkSize"],
  sl.annotateFunction(function (_self, _chunkSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _chunkSize";
      throw new Error(errorMessage);
    } /* Statements */
    return _chunksOfFrom_3(_self, _chunkSize, 1);
  }, ["self", "chunkSize"]),
  "{ :self :chunkSize | chunksOfFrom(self,chunkSize, 1) }",
);

sl.extendTypeOrTraitWithMethod(
  "String",
  "Sequenceable",
  "chunksOf",
  ["self", "chunkSize"],
  sl.annotateFunction(function (_self, _chunkSize) {
    /* ArityCheck */
    if (arguments.length !== 2) {
      const errorMessage = "Arity: expected 2, _self, _chunkSize";
      throw new Error(errorMessage);
    } /* Statements */
    return _chunksOfFrom_3(_self, _chunkSize, 1);
  }, ["self", "chunkSize"]),
  "{ :self :chunkSize | chunksOfFrom(self,chunkSize, 1) }",
);

sl.extendTypeOrTraitWithMethod(
  "@Object",
  "Sequenceable",
  "isSequenceable",
  ["self"],
  sl.annotateFunction(function (_self) {
    /* ArityCheck */
    if (arguments.length !== 1) {
      const errorMessage = "Arity: expected 1, _self";
      throw new Error(errorMessage);
    } /* Statements */
    return false;
  }, ["self"]),
  "{ :self | false }",
);
